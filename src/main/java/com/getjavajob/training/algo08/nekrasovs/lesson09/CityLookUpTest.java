package com.getjavajob.training.algo08.nekrasovs.lesson09;

import static com.getjavajob.training.algo08.nekrasovs.lesson09.CityLookUp.addCity;
import static com.getjavajob.training.algo08.nekrasovs.lesson09.CityLookUp.searchCity;
import static com.getjavajob.training.algo08.util.Assert.assertEquals;

public class CityLookUpTest {
    public static void main(String[] args) {
        testSearchCity();
    }

    public static void testSearchCity() {
        addCity("San Diego");
        addCity("Santa Monica");
        addCity("San Francisco");
        addCity("San Jose");
        addCity("Sacramento");
        addCity("Mountain View");
        addCity("Napa");
        addCity("ab" + String.valueOf(Character.MAX_VALUE) + "cd");
        assertEquals("CityLookUpTest.testSearchCity",
                "San Diego, San Francisco, San Jose, Santa Monica",
                searchCity("san"));
        assertEquals("CityLookUpTest.testSearchCity",
                "ab" + String.valueOf(Character.MAX_VALUE) + "cd",
                searchCity("ab" + String.valueOf(Character.MAX_VALUE) + "cd"));
    }
}