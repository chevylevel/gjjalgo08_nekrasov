package com.getjavajob.training.algo08.nekrasovs.lesson04;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;

import static com.getjavajob.training.algo08.util.StopWatch.getElapsedTime;
import static com.getjavajob.training.algo08.util.StopWatch.start;

public class DoublyLinkedListPerfomanceTest {

    public static void main(String[] args) {
        //testAddDll();
        //testAddLl();

        //testAddToBeginDll();
        //testAddToBeginLl();

        //testAddToMiddleDll();
        //testAddToMiddleLl();

        //testAddToEndDll();
        //testAddToEndLl();

        //testRemoveDll();
        //testRemoveLl();

        //testRemoveBeginDll();
        //testRemoveBeginLl();

        //testRemoveMiddleDll();
        //testRemoveMiddleLl();

        //testRemoveEndDll();
        //testRemoveEndLl();
    }

    public static void testAddDll() {
        String[] testArray = new String[9_000_000];
        Arrays.fill(testArray, "0");
        DoublyLinkedList<String> dll = new DoublyLinkedList<>();
        start();
        for (String i : testArray) {
            dll.add(i);
        }
        long time = getElapsedTime();
        System.out.println("DoublyLinkedList.add(e): " + time);
    }

    public static void testAddLl() {
        String[] testArray = new String[9_000_000];
        Arrays.fill(testArray, "0");
        LinkedList<String> ll = new LinkedList<>();
        start();
        for (String i : testArray) {
            ll.add(i);
        }
        long time = getElapsedTime();
        System.out.println("LinkedList.add(e): " + time);
    }

    public static void testAddToBeginDll() {
        int[] testArray = new int[9_000_000];
        Arrays.fill(testArray, 1);
        DoublyLinkedList<Integer> dll = new DoublyLinkedList<>();
        start();
        for (int i : testArray) {
            dll.add(0, i);
        }
        long time = getElapsedTime();
        System.out.println("DoublyLinkedList.add(0, e): " + time);
    }

    public static void testAddToBeginLl() {
        int[] testArray = new int[9_000_000];
        Arrays.fill(testArray, 1);
        LinkedList<Object> ll = new LinkedList<>();
        start();
        for (int i : testArray) {
            ll.add(0, i);
        }
        long time = getElapsedTime();
        System.out.println("LinkedList.add(0, e): " + time);
    }


    public static void testAddToMiddleDll() {
        int[] testArray = new int[100_000];
        Arrays.fill(testArray, 1);
        DoublyLinkedList<Integer> dll = new DoublyLinkedList<>();
        for (int i : testArray) {
            dll.add(i);
        }
        start();
        for (int i = 0; i < 30_000; i++) {
            dll.add(dll.size() >> 1, 1);
        }
        long time = getElapsedTime();
        System.out.println("DoublyLinkedList.add(dll.size() / 2, e): " + time);
    }

    public static void testAddToMiddleLl() {
        int[] testArray = new int[100_000];
        Arrays.fill(testArray, 1);
        LinkedList<Integer> ll = new LinkedList<>();
        for (int i : testArray) {
            ll.add(i);
        }
        start();
        for (int i = 0; i < 30_000; i++) {
            ll.add(ll.size() >> 1, 1);
        }
        long time = getElapsedTime();
        System.out.println("LinkedList.add(ll.size() / 2, e): " + time);
    }

    public static void testAddToEndDll() {
        int[] testArray = new int[5_000_000];
        Arrays.fill(testArray, 1);
        DoublyLinkedList<Object> dll = new DoublyLinkedList<>();
        for (int i : testArray) {
            dll.add(i);
        }
        start();
        for (int i = 5_000_000; i < 10_000_000; i++) {
            dll.add(i, 1);
        }
        long time = getElapsedTime();
        System.out.println("DoublyLinkedList.add(i, e): " + time);
    }

    public static void testAddToEndLl() {
        int[] testArray = new int[5_000_000];
        Arrays.fill(testArray, 1);
        LinkedList<Object> ll = new LinkedList<>();
        for (int i : testArray) {
            ll.add(i);
        }
        start();
        for (int i = 5_000_000; i < 10_000_000; i++) {
            ll.add(i, 1);
        }
        long time = getElapsedTime();
        System.out.println("LinkedList.add(i, e): " + time);
    }

    public static void testRemoveDll() {
        String[] testArray = new String[9_000_000];
        Arrays.fill(testArray, "0");
        DoublyLinkedList<String> dll = new DoublyLinkedList<>();
        Collections.addAll(dll, testArray);
        start();
        for (String i : testArray) {
            dll.remove(i);
        }
        long time = getElapsedTime();
        System.out.println("DoublyLinkedList.remove(e): " + time);
    }

    public static void testRemoveLl() {
        String[] testArray = new String[9_000_000];
        Arrays.fill(testArray, "0");
        LinkedList<String> ll = new LinkedList<>();
        Collections.addAll(ll, testArray);
        start();
        for (String i : testArray) {
            ll.remove(i);
        }
        long time = getElapsedTime();
        System.out.println("LinkedList.remove(e): " + time);
    }

    public static void testRemoveBeginDll() {
        int[] testArray = new int[9_000_000];
        Arrays.fill(testArray, 0);
        DoublyLinkedList<Object> dll = new DoublyLinkedList<>();
        for (int i : testArray) {
            dll.add(i);
        }
        start();
        for (int i = 0; i < 9_000_000; i++) {
            dll.remove(0);
        }
        long time = getElapsedTime();
        System.out.println("DoublyLinkedList.remove(0): " + time);
    }

    public static void testRemoveBeginLl() {
        int[] testArray = new int[9_000_000];
        Arrays.fill(testArray, 0);
        LinkedList<Object> ll = new LinkedList<>();
        for (int i : testArray) {
            ll.add(i);
        }
        start();
        for (int i = 0; i < 9_000_000; i++) {
            ll.remove(0);
        }
        long time = getElapsedTime();
        System.out.println("LinkedList.remove(0): " + time);
    }

    public static void testRemoveMiddleDll() {
        int[] testArray = new int[200_000];
        Arrays.fill(testArray, 1);
        DoublyLinkedList<Object> dll = new DoublyLinkedList<>();
        for (int i : testArray) {
            dll.add(i);
        }
        start();
        for (int i = 0; i < 50_000; i++) {
            dll.remove(dll.size() >> 1);
        }
        long time = getElapsedTime();
        System.out.println("DoublyLinkedList.remove(dll.size() / 2): " + time);
    }

    public static void testRemoveMiddleLl() {
        int[] testArray = new int[200_000];
        Arrays.fill(testArray, 1);
        LinkedList<Object> ll = new LinkedList<>();
        for (int i : testArray) {
            ll.add(i);
        }
        start();
        for (int i = 0; i < 50_000; i++) {
            ll.remove(ll.size() >> 1);
        }
        long time = getElapsedTime();
        System.out.println("LinkedList.remove(ll.size() / 2): " + time);
    }

    public static void testRemoveEndDll() {
        int[] testArray = new int[9_000_000];
        Arrays.fill(testArray, 1);
        DoublyLinkedList<Object> dll = new DoublyLinkedList<>();
        for (int i : testArray) {
            dll.add(i);
        }
        start();
        for (int i = 8_999_999; i >= 0; i--) {
            dll.remove(i);
        }
        long time = getElapsedTime();
        System.out.println("LinkedList.add(i): " + time);
    }

    public static void testRemoveEndLl() {
        int[] testArray = new int[9_000_000];
        Arrays.fill(testArray, 1);
        LinkedList<Object> ll = new LinkedList<>();
        for (int i : testArray) {
            ll.add(i);
        }
        start();
        for (int i = 8_999_999; i >= 0; i--) {
            ll.remove(i);
        }
        long time = getElapsedTime();
        System.out.println("LinkedList.remove(i): " + time);
    }
}