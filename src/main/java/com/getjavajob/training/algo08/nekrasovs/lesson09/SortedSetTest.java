package com.getjavajob.training.algo08.nekrasovs.lesson09;

import java.util.*;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;

public class SortedSetTest {
    private static Comparator<CustomClass> cmp = new Comparator<CustomClass>() {
        @Override
        public int compare(CustomClass o1, CustomClass o2) {
            if (o1.value > o2.value) {
                return 1;
            } else if (o1.value < o2.value) {
                return -1;
            } else {
                return 0;
            }
        }
    };

    private static SortedMap<String, String> emptyTs = new TreeMap<>();

    public static void main(String[] args) {
        testComparator();
        testSubSet();
        testHeadSet();
        testTailSet();
        testFirst();
        testLast();
    }

    public static void testComparator() {
        SortedSet<String> natOrd = new TreeSet<>();
        SortedSet<CustomClass> comOrd = new TreeSet<>(cmp);

        assertEquals("SortedSetTest.testComparator", cmp, comOrd.comparator());
        assertEquals("SortedSetTest.testComparator", null, natOrd.comparator());
        System.out.println("///////////////////////////////////////////");
    }

    public static void testSubSet() {
        SortedSet<String> tS = new TreeSet<>();
        tS.add("kn1");
        tS.add("kn2");
        tS.add("kn3");
        tS.add("kn4");
        tS.add("kn5");
        tS.add("kn6");
        tS.add("kn7");

        SortedSet<String> assumedTs = new TreeSet<>();
        assumedTs.add("kn2");
        assumedTs.add("kn3");

        SortedSet<String> actualTs = tS.subSet("kn2", "kn4");

        assertEquals("SortedSetTest.testSubSet", assumedTs, actualTs);
        actualTs.remove("kn3");
        assertEquals("SortedSetTest.testSubSet", false, tS.contains("kn3"));
        tS.remove("kn2");
        assertEquals("SortedSetTest.testSubSet", false, actualTs.contains("kn2"));

        try {
            actualTs.add("kn6");
        } catch (IllegalArgumentException e) {
            System.out.println("IAE, when insert element is out of range of subset, was thrown");
        }

        SortedSet<CustomClass> tSForCce = new TreeSet<>();
        tSForCce.add(new SubClass2("k1", 1, "1"));
        tSForCce.add(new SubClass2("k2", 2, "2"));
        tSForCce.add(new SubClass2("k3", 3, "3"));
        try {
            tSForCce.subSet(new SubClass1("k1", 1, "1"), new SubClass2("k3", 3, "3"));
        } catch (ClassCastException e) {
            System.out.println("CCE, when passed elements cannot be compared to one another, was thrown");
        }

        try {
            actualTs.subSet(null, "6");
        } catch (NullPointerException e) {
            System.out.println("NPE, when passed null element, was thrown");
        }

        try {
            actualTs.subSet("kn7", "kn6");
        } catch (IllegalArgumentException e) {
            System.out.println("IAE, when fromElement > toElement, was thrown");
        }
        System.out.println("///////////////////////////////////////////");
    }

    public static void testHeadSet() {
        SortedSet<String> tS = new TreeSet<>();
        tS.add("kn1");
        tS.add("kn2");
        tS.add("kn3");
        tS.add("kn4");
        tS.add("kn5");
        tS.add("kn6");
        tS.add("kn7");

        SortedSet<String> assumedTs = new TreeSet<>();
        assumedTs.add("kn1");
        assumedTs.add("kn2");

        SortedSet<String> actualTs = tS.headSet("kn3");

        assertEquals("SortedSetTest.testHeadSet", assumedTs, actualTs);
        actualTs.remove("kn1");
        assertEquals("SortedSetTest.testHeadSet", false, tS.contains("kn1"));
        tS.remove("kn2");
        assertEquals("SortedSetTest.testHeadSet", false, actualTs.contains("kn2"));

        try {
            actualTs.add("kn5");
        } catch (IllegalArgumentException e) {
            System.out.println("IAE, when insert element is out of range of subset, was thrown");
        }

        SortedSet<CustomClass> tSForCce = new TreeSet<>();
        tSForCce.add(new SubClass1("k1", 1, "1"));
        tSForCce.add(new SubClass1("k2", 2, "2"));
        tSForCce.add(new SubClass1("k3", 3, "3"));
        try {
            tSForCce.headSet(new CustomClass("k1", 1));
        } catch (ClassCastException e) {
            System.out.println("CCE, when passed element does not implement Comparable, was thrown");
        }

        try {
            tS.headSet(null);
        } catch (NullPointerException e) {
            System.out.println("NPE, when passed null element, was thrown");
        }

        try {
            actualTs.headSet("kn4");
        } catch (IllegalArgumentException e) {
            System.out.println("IAE, when toElement is out of range of subset, was thrown");
        }
        System.out.println("///////////////////////////////////////////");
    }

    public static void testTailSet() {
        SortedSet<String> tS = new TreeSet<>();
        tS.add("kn1");
        tS.add("kn2");
        tS.add("kn3");
        tS.add("kn4");
        tS.add("kn5");
        tS.add("kn6");
        tS.add("kn7");

        SortedSet<String> assumedTs = new TreeSet<>();
        assumedTs.add("kn6");
        assumedTs.add("kn7");

        SortedSet<String> actualTs = tS.tailSet("kn6");

        assertEquals("SortedSetTest.testTailSet", assumedTs, actualTs);
        actualTs.remove("kn6");
        assertEquals("SortedSetTest.testTailSet", null, tS.contains("kn6"));
        tS.remove("kn7");
        assertEquals("SortedSetTest.testTailSet", null, actualTs.contains("kn7"));

        try {
            actualTs.add("kn5");
        } catch (IllegalArgumentException e) {
            System.out.println("IAE, when insert element is out of range of subset, was thrown");
        }

        SortedSet<CustomClass> tSForCce = new TreeSet<>();
        tSForCce.add(new SubClass1("k1", 1, "1"));
        tSForCce.add(new SubClass1("k2", 2, "2"));
        tSForCce.add(new SubClass1("k3", 3, "3"));
        tSForCce.add(new SubClass1("k4", 4, "4"));
        tSForCce.add(new SubClass1("k5", 5, "5"));
        try {
            tSForCce.tailSet(new CustomClass("k4", 4));
        } catch (ClassCastException e) {
            System.out.println("CCE, when passed element does not implement Comparable, was thrown");
        }

        try {
            tS.tailSet(null);
        } catch (NullPointerException e) {
            System.out.println("NPE, when passed null element, was thrown");
        }

        try {
            actualTs.tailSet("kn5");
        } catch (IllegalArgumentException e) {
            System.out.println("IAE, when fromElement is out of range, was thrown");
        }
        System.out.println("///////////////////////////////////////////");
    }

    public static void testFirst() {
        SortedSet<String> tS = new TreeSet<>();
        tS.add("kn1");
        tS.add("kn2");
        tS.add("kn3");

        assertEquals("SortedMapTest.testFirstKey", "kn1", tS.first());
        try {
            emptyTs.firstKey();
        } catch (NoSuchElementException e) {
            System.out.println("NoSuchElementException was thrown");
        }
        System.out.println("///////////////////////////////////////////");
    }

    private static void testLast() {
        SortedSet<String> tS = new TreeSet<>();
        tS.add("kn1");
        tS.add("kn2");
        tS.add("kn3");

        assertEquals("SortedMapTest.testLastKey", "kn3", tS.last());
        try {
            emptyTs.lastKey();
        } catch (NoSuchElementException e) {
            System.out.println("NoSuchElementException was thrown");
        }
        System.out.println("///////////////////////////////////////////");
    }

    public static class CustomClass {
        String name;
        int value;

        CustomClass(String name, int value) {
            this.name = name;
            this.value = value;
        }

        @Override
        public String toString() {
            return String.valueOf(name);
        }
    }

    protected static class SubClass1 extends CustomClass implements Comparable<SubClass1> {
        String subValue1;

        public SubClass1(String name, int value, String subValue1) {
            super(name, value);
            this.subValue1 = subValue1;
        }

        @Override
        public int compareTo(SubClass1 o) {
            return this.subValue1.compareTo(o.subValue1);
        }
    }

    protected static class SubClass2 extends CustomClass implements Comparable<SubClass2> {
        String subValue2;

        public SubClass2(String name, int value, String subValue2) {
            super(name, value);
            this.subValue2 = subValue2;
        }

        @Override
        public int compareTo(SubClass2 o) {
            return this.subValue2.compareTo(o.subValue2);
        }
    }
}