package com.getjavajob.training.algo08.nekrasovs.lesson06;

import java.util.HashMap;
import java.util.Map;

public class HashMatrix<V> implements Matrix {
    private Map<HashMatrix.Key, V> Storage = new HashMap<>();

    @Override
    public V get(int i, int j) {
        return Storage.get(new HashMatrix.Key(i, j));
    }

    @Override
    public void set(int i, int j, Object value) {
        //noinspection unchecked
        Storage.put(new HashMatrix.Key(i, j), (V) value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof HashMatrix)) return false;

        HashMatrix<?> that = (HashMatrix<?>) o;

        return Storage.equals(that.Storage);
    }

    @Override
    public int hashCode() {
        return Storage.hashCode();
    }

    static class Key {
        private final int I;
        private final int J;

        Key(int i, int j) {
            if (i > 1_000_000 || i < 0 || j > 1_000_000 || j < 0) {
                throw new IndexOutOfBoundsException();
            }
            this.I = i;
            this.J = j;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            HashMatrix.Key key = (HashMatrix.Key) o;

            return I == key.I && J == key.J;
        }

        @Override
        public int hashCode() {
            int result = I;
            result = 31 * result + J;
            return result;
        }
    }
}