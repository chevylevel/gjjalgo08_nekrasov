package com.getjavajob.training.algo08.nekrasovs.lesson07.tree.binary;

import com.getjavajob.training.algo08.nekrasovs.lesson07.tree.Node;
import com.getjavajob.training.algo08.nekrasovs.lesson07.tree.Tree;

import java.util.*;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;

public class LinkedBinaryTreeTest {
    private static Tree<String> emptyTree = new LinkedBinaryTree<>();
    private static LinkedBinaryTree<String> testTree = new LinkedBinaryTree<>();
    private static Node<String> root = testTree.addRoot("root");
    private static Node<String> l = testTree.add(testTree.root(), "l");
    private static Node<String> r = testTree.add(testTree.root(), "r");
    private static Node<String> ll = testTree.add(l, "ll");
    private static Node<String> lr = testTree.add(l, "lr");
    private static Node<String> rl = testTree.add(r, "rl");
    private static Node<String> rr = testTree.add(r, "rr");
    private static Node<String> otherNode = new Node<String>() {
        @Override
        public String getElement() {
            return null;
        }
    };

    public static void main(String[] args) {
        testIsInternal();
        testIsExternal();
        testIsRoot();
        testIsEmpty();
        testIterator();
        testPreOrder();
        testPostOrder();
        testBreadthFirst();
        testSibling();
        testChildren();
        testChildrenNumber();
        testInOrder();
        testLeft();
        testRight();
        testAddLeft();
        testAddRight();
        testRoot();
        testParent();
        testAddRoot();
        testAdd();
        testSet();
        testRemove();
        testSize();
        testNodes();
    }

    public static void testIsInternal() {
        assertEquals("AbstractTreeTest.testIsInternal", true, testTree.isInternal(l));
        assertEquals("AbstractTreeTest.testIsInternal", false, testTree.isInternal(rr));
        int passFlag = 0;
        try {
            testTree.isInternal(otherNode);
        } catch (IllegalArgumentException e) {
            System.out.println("IllegalArgumentExceptionTest passed");
            passFlag = 1;
        }
        if (passFlag == 0) {
            System.out.println("IllegalArgumentExceptionTest failed");
        }
    }

    public static void testIsExternal() {
        assertEquals("AbstractTreeTest.testIsExternal", false, testTree.isExternal(l));
        assertEquals("AbstractTreeTest.testIsExternal", true, testTree.isExternal(rr));
        int passFlag = 0;
        try {
            testTree.isExternal(otherNode);
        } catch (IllegalArgumentException e) {
            System.out.println("IllegalArgumentExceptionTest passed");
            passFlag = 1;
        }
        if (passFlag == 0) {
            System.out.println("IllegalArgumentExceptionTest failed");
        }
    }

    public static void testIsRoot() {
        assertEquals("AbstractTreeTest.testIsRoot", true, testTree.isRoot(root));
        assertEquals("AbstractTreeTest.testIsRoot", false, testTree.isRoot(l));
        int passFlag = 0;
        try {
            testTree.isRoot(otherNode);
        } catch (IllegalArgumentException e) {
            System.out.println("IllegalArgumentExceptionTest passed");
            passFlag = 1;
        }
        if (passFlag == 0) {
            System.out.println("IllegalArgumentExceptionTest failed");
        }
    }

    public static void testIsEmpty() {
        assertEquals("AbstractTreeTest.testIsEmpty", true, emptyTree.isEmpty());
        assertEquals("AbstractTreeTest.testIsEmpty", false, testTree.isEmpty());
    }

    public static void testIterator() {
        Iterator<String> itr = testTree.iterator();
        List<String> list = new LinkedList<>();
        while (itr.hasNext()) {
            list.add(itr.next());
        }
        assertEquals("AbstractTreeTest.testIterator",
                new String[]{"ll", "l", "lr", "root", "rl", "r", "rr"},
                list.toArray());
    }

    public static void testPreOrder() {
        List<String> testList = new LinkedList<>();
        for (Node<String> node : testTree.preOrder()) {
            testList.add(node.getElement());
        }
        assertEquals("AbstractTreeTest.testPreOrder",
                new String[]{"root", "l", "ll", "lr", "r", "rl", "rr"},
                testList.toArray());
    }

    public static void testPostOrder() {
        List<String> testList = new LinkedList<>();
        for (Node<String> node : testTree.postOrder()) {
            testList.add(node.getElement());
        }
        assertEquals("AbstractTreeTest.testPostOrder",
                new String[]{"ll", "lr", "l", "rl", "rr", "r", "root"},
                testList.toArray());
    }

    public static void testBreadthFirst() {
        List<String> testList = new LinkedList<>();
        for (Node<String> node : testTree.breadthFirst()) {
            testList.add(node.getElement());
        }
        assertEquals("AbstractTreeTest.testBreadthFirst",
                new String[]{"root", "l", "r", "ll", "lr", "rl", "rr"},
                testList.toArray());
    }

    public static void testSibling() {
        assertEquals("AbstractTreeTest.testSibling", l, testTree.sibling(r));

        int passFlag = 0;
        try {
            testTree.sibling(otherNode);
        } catch (IllegalArgumentException e) {
            System.out.println("IllegalArgumentExceptionTest passed");
            passFlag = 1;
        }
        if (passFlag == 0) {
            System.out.println("IllegalArgumentExceptionTest failed");
        }
    }

    public static void testChildren() {
        Collection<Node<String>> testCol = new ArrayList<>();
        testCol.add(ll);
        testCol.add(lr);

        assertEquals("AbstractTreeTest.testChildren", testCol, testTree.children(l));

        int passFlag = 0;
        try {
            testTree.children(otherNode);
        } catch (IllegalArgumentException e) {
            System.out.println("IllegalArgumentExceptionTest passed");
            passFlag = 1;
        }
        if (passFlag == 0) {
            System.out.println("IllegalArgumentExceptionTest failed");
        }
    }

    public static void testChildrenNumber() {
        assertEquals("AbstractTreeTest.testChildrenNumber", 2, testTree.childrenNumber(l));

        int passFlag = 0;
        try {
            testTree.childrenNumber(otherNode);
        } catch (IllegalArgumentException e) {
            System.out.println("IllegalArgumentExceptionTest passed");
            passFlag = 1;
        }
        if (passFlag == 0) {
            System.out.println("IllegalArgumentExceptionTest failed");
        }
    }

    public static void testInOrder() {
        Node[] arrToCompare = {ll, l, lr, root, rl, r, rr};
        assertEquals("AbstractTreeTest.testInOrder", arrToCompare, testTree.inOrder().toArray());
    }

    public static void testLeft() {
        int nullCheck = (testTree.left(ll) == null) ? 1 : 0;

        assertEquals("AbstractTreeTest.testLeft", l, testTree.left(root));
        assertEquals("AbstractTreeTest.testLeft", 1, nullCheck);

        int passFlag = 0;
        try {
            testTree.left(otherNode);
        } catch (IllegalArgumentException e) {
            System.out.println("IllegalArgumentExceptionTest passed");
            passFlag = 1;
        }
        if (passFlag == 0) {
            System.out.println("IllegalArgumentExceptionTest failed");
        }
    }

    public static void testRight() {
        int nullCheck = (testTree.right(ll) == null) ? 1 : 0;

        assertEquals("AbstractTreeTest.testRight", r, testTree.right(root));
        assertEquals("AbstractTreeTest.testRight", 1, nullCheck);

        int passFlag = 0;
        try {
            testTree.right(otherNode);
        } catch (IllegalArgumentException e) {
            System.out.println("IllegalArgumentExceptionTest passed");
            passFlag = 1;
        }
        if (passFlag == 0) {
            System.out.println("IllegalArgumentExceptionTest failed");
        }
    }

    public static void testAddLeft() {
        Node<String> leftNode = testTree.addLeft(ll, "lll");
        assertEquals("ArrayBinaryTreeTest.testAddLeft", leftNode, testTree.left(ll));

        int passFlag = 0;
        try {
            testTree.addLeft(ll, "lll");
        } catch (IllegalArgumentException e) {
            System.out.println("IllegalArgumentExceptionTest passed");
            passFlag = 1;
        }
        if (passFlag == 0) {
            System.out.println("IllegalArgumentExceptionTest failed");
        }
    }

    public static void testAddRight() {
        Node<String> rightNode = testTree.addRight(ll, "llr");
        assertEquals("ArrayBinaryTreeTest.testAddRight", rightNode, testTree.right(ll));

        int passFlag = 0;
        try {
            testTree.addRight(ll, "llr");
        } catch (IllegalArgumentException e) {
            System.out.println("IllegalArgumentExceptionTest passed");
            passFlag = 1;
        }
        if (passFlag == 0) {
            System.out.println("IllegalArgumentExceptionTest failed");
        }
    }

    public static void testRoot() {
        assertEquals("ArrayBinaryTreeTest.testRoot", root, testTree.root());
    }

    public static void testParent() {
        int nullCheck = (testTree.parent(root) == null) ? 1 : 0;

        assertEquals("ArrayBinaryTreeTest.testParent", root, testTree.parent(l));
        assertEquals("ArrayBinaryTreeTest.testParent", 1, nullCheck);

        int passFlag = 0;
        try {
            testTree.parent(otherNode);
        } catch (IllegalArgumentException e) {
            System.out.println("IllegalArgumentExceptionTest passed");
            passFlag = 1;
        }
        if (passFlag == 0) {
            System.out.println("IllegalArgumentExceptionTest failed");
        }
    }

    public static void testAddRoot() {
        assertEquals("ArrayBinaryTreeTest.testAddRoot", root, testTree.root());

        int passFlag = 0;
        try {
            testTree.addRoot("root");
        } catch (IllegalStateException e) {
            System.out.println("IllegalStateExceptionTest passed");
            passFlag = 1;
        }
        if (passFlag == 0) {
            System.out.println("IllegalStateExceptionTest failed");
        }
    }

    public static void testAdd() {
        Node<String> lrlNode = testTree.add(lr, "lrl");
        assertEquals("ArrayBinaryTreeTest.testAdd", lrlNode, testTree.left(lr));

        int passFlag = 0;
        try {
            testTree.add(otherNode, "lrl");
        } catch (IllegalArgumentException e) {
            System.out.println("IllegalArgumentExceptionTest passed");
            passFlag = 1;
        }
        if (passFlag == 0) {
            System.out.println("IllegalArgumentExceptionTest failed");
        }
    }

    public static void testSet() {
        testTree.set(lr, "set");
        assertEquals("ArrayBinaryTreeTest.testSet", "set", lr.getElement());

        int passFlag = 0;
        try {
            testTree.set(otherNode, "set");
        } catch (IllegalArgumentException e) {
            System.out.println("IllegalArgumentExceptionTest passed");
            passFlag = 1;
        }
        if (passFlag == 0) {
            System.out.println("IllegalArgumentExceptionTest failed");
        }
    }

    /* Remove node with 0, 1 and 2 children:
     *
     *            root
     *          /      \
     *         l         r
     *       /   \     /   \
     *     ll     lr rl    [rr]
     *    /  \    /
     *  lll  llr lrl
     *
     *            root
     *          /      \
     *         l        [r]
     *       /   \     /
     *     ll     lr rl
     *    /  \    /
     *  lll  llr lrl
     *
     *            root
     *          /      \
     *        [l]       rl
     *       /   \
     *     ll     lr
     *    /  \    /
     *  lll  llr lrl
     *
     *            root
     *          /      \
     *        lrl       rl
     *       /   \
     *     ll     lr
     *    /  \
     *  lll  llr
     */

    public static void testRemove() {
        testTree.remove(rr);
        int nullCheck = (testTree.right(r) == null) ? 1 : 0;
        assertEquals("ArrayBinaryTreeTest.testRemove", 1, nullCheck);

        testTree.remove(r);
        assertEquals("ArrayBinaryTreeTest.testRemove", rl, testTree.right(root));

        testTree.remove(l);
        int nullCheck2 = (testTree.left(lr) == null) ? 1 : 0;
        assertEquals("ArrayBinaryTreeTest.testRemove", "lrl", testTree.left(root).getElement());
        assertEquals("ArrayBinaryTreeTest.testRemove", 1, nullCheck2);
    }

    public static void testSize() {
        assertEquals("ArrayBinaryTreeTest.testSize", 7, testTree.size());
    }

    public static void testNodes() {
        Node<String> lrl = testTree.left(root);
        Node<String> lll = testTree.left(ll);
        Node<String> llr = testTree.right(ll);
        Node[] nodes = {lll, ll, llr, lrl, lr, root, rl};
        assertEquals("ArrayBinaryTreeTest.testNodes", nodes, testTree.nodes().toArray());
    }
}