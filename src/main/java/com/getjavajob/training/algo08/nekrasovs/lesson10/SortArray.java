package com.getjavajob.training.algo08.nekrasovs.lesson10;

public class SortArray {
    private static <E> int compare(Comparable<E> e1, E e2) {
        return e1.compareTo(e2);
    }

    public static <E> E[] bubbleSort(E[] unsorted) {
        E current;
        for (int i = unsorted.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                current = unsorted[j];
                if (compare((Comparable) current, unsorted[j + 1]) > 0) {
                    swap(unsorted, j, j + 1);
                }
            }
        }
        return unsorted;
    }

    private static <E> void swap(E[] a, int i, int j) {
        E buffer = a[i];
        a[i] = a[j];
        a[j] = buffer;
    }

    private static <E> void qSort(E[] unsorted, int i, int j) {
        E pivot = unsorted[i];
        int oldI = i;
        int oldJ = j;

        while (i < j) {
            while (compare((Comparable) unsorted[i], pivot) < 0) {
                i++;
            }
            while (compare((Comparable) unsorted[j], pivot) > 0) {
                j--;
            }
            if (i < j && unsorted[i] != unsorted[j]) {
                swap(unsorted, i, j);
            } else {
                if (i < j) {
                    i++;
                }
            }
        }

        if (oldI != i) {
            qSort(unsorted, oldI, i - 1);
        }
        if (oldJ != j) {
            qSort(unsorted, j + 1, oldJ);
        }
    }

    public static <E> E[] quickSort(E[] unsorted) {
        qSort(unsorted, 0, unsorted.length - 1);
        return unsorted;
    }

    public static <E> E[] insSort(E[] unsorted) {
        for (int i = 0; i < unsorted.length - 1; i++) {
            if (compare((Comparable) unsorted[i], unsorted[i + 1]) > 0) {
                int j = i;
                while (compare((Comparable) unsorted[j], unsorted[j + 1]) > 0) {
                    swap(unsorted, j, j + 1);
                    if (j != 0) {
                        j--;
                    }
                }
            }
        }
        return unsorted;
    }

    private static <E> void merge(E[] unsorted, int low, int mid, int hi, E[] temp) {
        for (int i = low; i <= hi; i++) {
            temp[i] = unsorted[i];
        }

        int left = low;   // counter from low to mid
        int right = mid + 1; // counter from mid to hi
        int k = low;

        while (left <= mid && right <= hi) {
            if (compare((Comparable) temp[left], temp[right]) < 0) {
                unsorted[k] = temp[left];
                left++;
            } else {
                unsorted[k] = temp[right];
                right++;
            }
            k++;
        }
        System.out.println(left + "left");
        System.out.println(mid + "mid");
        while (left <= mid) {
            unsorted[k] = temp[left];
            left++;
            k++;
        }
    }

    private static <E> void split(E[] unsorted, int low, int hi, Object[] temp) {
        if (low < hi) {
            int mid = low + (hi - low) / 2;
            split(unsorted, low, mid, temp);
            split(unsorted, mid + 1, hi, temp);
            merge(unsorted, low, mid, hi, temp);
        }
    }

    public static <E> Object[] mergeSort(E[] unsorted) {
        int length = unsorted.length;
        Object[] temp = new Object[length];
        split(unsorted, 0, length - 1, temp);
        return unsorted;
    }
}