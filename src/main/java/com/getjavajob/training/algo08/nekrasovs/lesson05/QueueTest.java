package com.getjavajob.training.algo08.nekrasovs.lesson05;

import java.util.ArrayDeque;
import java.util.Queue;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;

public class QueueTest {
    public static void main(String[] args) {
        testAdd();
        testOffer();
        testRemove();
        testPoll();
        testElement();
        testPeek();
    }

    public static void testAdd() {
        Queue<String> llq = new ArrayDeque<>();
        llq.add("t");
        llq.add("e");
        llq.add("s");
        llq.add("t");

        String testString = "";

        testString += llq.remove();
        testString += llq.remove();
        testString += llq.remove();
        testString += llq.remove();

        assertEquals("QueueTest.testAdd", "test", testString);
    }

    public static void testOffer() {
        Queue<String> llq = new ArrayDeque<>();
        llq.offer("t");
        llq.offer("e");
        llq.offer("s");
        llq.offer("t");

        String testString = "";

        testString += llq.remove();
        testString += llq.remove();
        testString += llq.remove();
        testString += llq.remove();

        assertEquals("QueueTest.testOffer", "test", testString);
    }

    public static void testRemove() {
        Queue<String> llq = new ArrayDeque<>();
        llq.add("t");
        llq.add("e");
        llq.add("s");
        llq.add("t");

        llq.remove();

        String testString = "";
        testString += llq.remove();
        testString += llq.remove();
        testString += llq.remove();

        assertEquals("QueueTest.testRemove", "est", testString);
    }

    public static void testPoll() {
        Queue<String> llq = new ArrayDeque<>();
        llq.add("t");
        llq.add("e");
        llq.add("s");
        llq.add("t");

        llq.poll();

        String testString = "";
        testString += llq.poll();
        testString += llq.poll();
        testString += llq.poll();

        assertEquals("QueueTest.testPoll", "est", testString);
    }

    public static void testElement() {
        Queue<String> llq = new ArrayDeque<>();
        llq.add("T");
        llq.add("e");
        llq.add("s");
        llq.add("t");

        String testStringElement = llq.element();
        String testStringRemove = llq.remove();

        assertEquals("QueueTest.testElement", testStringElement, testStringRemove);
    }

    public static void testPeek() {
        Queue<String> llq = new ArrayDeque<>();
        llq.add("T");
        llq.add("e");
        llq.add("s");
        llq.add("t");

        String testStringPeek = llq.peek();
        String testStringRemove = llq.remove();

        assertEquals("QueueTest.testPeek", testStringPeek, testStringRemove);
    }
}