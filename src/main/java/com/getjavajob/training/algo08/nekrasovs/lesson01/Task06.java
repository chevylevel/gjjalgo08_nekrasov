package com.getjavajob.training.algo08.nekrasovs.lesson01;

public class Task06 {
    public static void main(String[] args) {
        System.out.println((i((byte) 12)));
        System.out.println(f(31, 2));
    }

    /**
     * given n<31, calc 2^n
     */
    public static int a(int n) {
        return 2 << n - 1;
    }

    /**
     * given n,m<31, calc 2^n+2^m
     */
    public static int b(int n, int m) {
        return (2 << n - 1) + (2 << m - 1);
    }

    /**
     * reset n lower bits(create mask with n lower bits reset)
     */
    public static int c(int a, int n) {
        int mask = -1;
        mask <<= n;
        return a & mask;
    }

    /**
     * set a's n-th bit with 1
     */
    public static int d(int a, int n) {
        int mask = 1;
        mask <<= n;
        return a | mask;
    }

    /**
     * invert n-th bit (use 2 bit ops)
     */
    public static int e(int a, int n) {
        int mask = 1;
        mask <<= n;
        return a ^ mask;
    }

    /**
     * set a's n-th bit with 0
     */
    public static int f(int a, int n) {
        int mask = 1;
        mask <<= n;
        return a & ~mask;
    }

    /**
     * return n lower bits
     */
    public static int g(int a, int n) {
        int mask = -1;
        mask <<= n;
        return a & ~mask;
    }

    /**
     * return n-th bit
     */
    public static int h(int a, int n) {
        int mask = 1;
        mask <<= n;
        return (a & mask) >>> n;
    }

    /**
     * given byte a. output bin representation using bit ops (don't use jdk api).
     */
    public static String i(byte a) {

        StringBuilder binOutput = new StringBuilder("0b");
        for (int i = 7; i >= 0; i--) {
            binOutput.append(String.valueOf(h(a, i)));
        }
        return binOutput.toString();
    }
}