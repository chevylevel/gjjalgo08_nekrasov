package com.getjavajob.training.algo08.nekrasovs.lesson06;

import java.util.*;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;

public class LinkedHashMapTest {
    public static void main(String[] args) {
        testPut();
    }

    public static void testPut() {
        Map<Integer, String> lhm = new LinkedHashMap<>();
        lhm.put(1, "obj1");
        lhm.put(2, "obj2");
        lhm.put(3, "obj3");
        List<String> l = new ArrayList<>();
        Iterator<String> itr = lhm.values().iterator();
        while (itr.hasNext()) {
            l.add(itr.next());
        }
        assertEquals("LinkedHashSetTest.testAdd", "obj1", l.get(0));
        assertEquals("LinkedHashSetTest.testAdd", "obj2", l.get(1));
        assertEquals("LinkedHashSetTest.testAdd", "obj3", l.get(2));
    }
}