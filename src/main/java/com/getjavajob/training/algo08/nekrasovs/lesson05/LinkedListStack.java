package com.getjavajob.training.algo08.nekrasovs.lesson05;

public class LinkedListStack<E> implements Stack<E> {
    private SinglyLinkedList<E> sll = new SinglyLinkedList<>();

    public static void main(String[] args) {
        LinkedListStack<String> lls = new LinkedListStack<>();
        lls.push("t");
        lls.push("e");
        lls.push("s");
        lls.push("t");

        System.out.println();

        System.out.println(lls.pop());
        System.out.println(lls.pop());
    }

    public void push(E e) {
        SinglyLinkedList.Node<E> newNode = new SinglyLinkedList.Node<>();
        newNode.val = e;
        newNode.next = sll.head;
        sll.head = newNode;
    }

    public E pop() {
        E val = sll.head.val;
        sll.head = sll.head.next;
        return val;
    }
}