package com.getjavajob.training.algo08.nekrasovs.lesson04;

import java.util.Arrays;
import java.util.List;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;

public class SinglyLinkedListTest {
    public static void main(String[] args) {
        testGet();
        testSize();
        testReverse();
        testSwap1();
        testSwap2();
        testSwap3();
        testSwap4();
        testAsList();
    }

    public static void testGet() {
        SinglyLinkedList<String> sll = new SinglyLinkedList<>();
        sll.add("s");
        sll.add("t");
        sll.add("r");
        sll.add("i");
        sll.add("n");
        sll.add("g");
        assertEquals("SinglyLinkedListTest.testGet", "g", sll.get(5));
    }

    public static void testSize() {
        SinglyLinkedList<String> sll = new SinglyLinkedList<>();
        sll.add("s");
        sll.add("t");
        sll.add("r");
        sll.add("i");
        sll.add("n");
        sll.add("g");
        assertEquals("SinglyLinkedListTest.testSize", 6, sll.size());
    }

    public static void testReverse() {
        SinglyLinkedList<String> sll = new SinglyLinkedList<>();
        sll.add("s");
        sll.add("t");
        sll.add("r");
        sll.add("i");
        sll.add("n");
        sll.add("g");
        sll.reverse();
        String[] testArray = {"g", "n", "i", "r", "t", "s"};
        assertEquals("SinglyLinkedListTest.testReverse", testArray, sll.asList().toArray());
    }

    /**
     * swap adjacent nodes in the beginning
     */
    public static void testSwap1() {
        SinglyLinkedList<String> sll = new SinglyLinkedList<>();
        sll.add("s");
        sll.add("t");
        sll.add("r");
        sll.add("i");
        sll.add("n");
        sll.add("g");
        sll.swap(sll.head, sll.head.next);
        String[] testArray = {"t", "s", "r", "i", "n", "g"};
        assertEquals("SinglyLinkedListTest.testSwap1", testArray, sll.asList().toArray());
    }

    /**
     * swap nodes, first node in the beginning
     */
    public static void testSwap2() {
        SinglyLinkedList<String> sll = new SinglyLinkedList<>();
        sll.add("s");
        sll.add("t");
        sll.add("r");
        sll.add("i");
        sll.add("n");
        sll.add("g");
        sll.swap(sll.head, sll.head.next.next);
        String[] testArray = {"r", "t", "s", "i", "n", "g"};
        assertEquals("SinglyLinkedListTest.testSwap2", testArray, sll.asList().toArray());
    }

    /**
     * swap adjacent nodes in the middle
     */
    public static void testSwap3() {
        SinglyLinkedList<String> sll = new SinglyLinkedList<>();
        sll.add("s");
        sll.add("t");
        sll.add("r");
        sll.add("i");
        sll.add("n");
        sll.add("g");
        sll.swap(sll.head.next, sll.head.next.next);
        String[] testArray = {"s", "r", "t", "i", "n", "g"};
        assertEquals("SinglyLinkedListTest.testSwap3", testArray, sll.asList().toArray());
    }

    /**
     * swap not adjacent nodes in the middle
     */
    public static void testSwap4() {
        SinglyLinkedList<String> sll = new SinglyLinkedList<>();
        sll.add("s");
        sll.add("t");
        sll.add("r");
        sll.add("i");
        sll.add("n");
        sll.add("g");
        sll.swap(sll.head.next, sll.head.next.next.next);
        String[] testArray = {"s", "i", "r", "t", "n", "g"};
        assertEquals("SinglyLinkedListTest.testSwap4", testArray, sll.asList().toArray());
    }

    public static void testAsList() {
        SinglyLinkedList<String> sll = new SinglyLinkedList<>();
        sll.add("s");
        sll.add("t");
        sll.add("r");
        sll.add("i");
        sll.add("n");
        sll.add("g");
        String[] testArray = {"s", "t", "r", "i", "n", "g"};
        List<String> testList = Arrays.asList(testArray);
        assertEquals("SinglyLinkedListTest.testAsList", testList, sll.asList());
    }
}