package com.getjavajob.training.algo08.nekrasovs.lesson10;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;

public class SortArrayTest {
    private static final Integer[] UNSORTED = {8, 4, 6, 0, 0, 7};
    private static final Integer[] SORTED = {0, 0, 4, 6, 7, 8};
    //private static Integer[] huge = new Integer[Integer.MAX_VALUE];
    //Requested array size exceeds VM limit

    public static void main(String[] args) {
        //Arrays.fill(huge, 1);
        testBubbleSort();
        testInsSort();
        testQuickSort();
        testMergeSort();
    }

    private static void testBubbleSort() {
        assertEquals("SortArrayTest.testBubbleSort", SORTED, SortArray.bubbleSort(UNSORTED));
        assertEquals("SortArrayTest.testBubbleSort", SORTED, SortArray.bubbleSort(SORTED));
    }

    private static void testInsSort() {
        assertEquals("SortArrayTest.testInsertionSort", SORTED, SortArray.insSort(UNSORTED));
    }

    private static void testQuickSort() {
        assertEquals("SortArrayTest.testQuickSort", SORTED, SortArray.quickSort(UNSORTED));
        //assertEquals("SortArrayTest.testQuickSort", huge, SortArray.quickSort(huge));
    }

    private static void testMergeSort() {
        assertEquals("SortArrayTest.testMergeSort", SORTED, SortArray.quickSort(UNSORTED));
        //assertEquals("SortArrayTest.testMergeSort", huge, SortArray.quickSort(huge));
    }
}