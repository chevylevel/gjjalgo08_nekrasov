package com.getjavajob.training.algo08.nekrasovs.lesson03;

import java.util.Arrays;
import java.util.ConcurrentModificationException;
import java.util.NoSuchElementException;

public class DynamicArray {
    private final int DEFAULT_CAPACITY = 10;
    private Object[] array;
    private int size;
    private int modCount = 0;


    public DynamicArray(int size) {
        if (size < 0) {
            throw new IndexOutOfBoundsException("IndexOutOfBounds");
        } else {
            array = new Object[size];
        }
    }

    public DynamicArray() {
        array = new Object[DEFAULT_CAPACITY];
    }

    private void checkIndexForAdd(int ind) throws IndexOutOfBoundsException {
        if (ind > size || ind < 0) {
            throw new IndexOutOfBoundsException("IndexOutOfBounds");
        }
    }

    private void checkIndex(int index) {
        if (index >= size) {
            throw new IndexOutOfBoundsException("IndexOutOfBounds");
        }
    }

    private void checkCapacity() {
        modCount++;
        if (size >= array.length) {
            expandArray();
        }
    }

    private void expandArray() {
        int length = array.length;
        int newSize = length + (length >> 1);
        if (length == 0) {
            newSize = 1;
        } else if (length == 1) {
            newSize = 2;
        }
        array = Arrays.copyOf(array, newSize);
    }

    public boolean add(Object e) {
        checkCapacity();
        array[size++] = e;
        return true;
    }

    /**
     * Add object "e" to array at position "index" and shift elements right.
     */
    private void shiftValues(int index, Object e) {
        System.arraycopy(array, index, array, index + 1, size - index);
        array[index] = e;
    }

    public void add(int i, Object e) {
        checkIndexForAdd(i);
        checkCapacity();
        if (array[array.length - 1] == null) {
            shiftValues(i, e);
        } else {
            expandArray();
            shiftValues(i, e);
        }
        size++;
    }

    public Object set(int i, Object e) {
        checkIndex(i);
        Object prevObj = array[i];
        array[i] = e;
        return prevObj;
    }

    public Object get(int i) {
        checkIndex(i);
        return array[i];
    }

    public Object remove(int i) {
        checkIndex(i);
        modCount++;
        Object removedObj = array[i];
        subRemove(i);
        return removedObj;
    }

    private void subRemove(int i) {
        modCount++;
        System.arraycopy(array, i + 1, array, i, size - i - 1);
        array[--size] = null;
    }

    public boolean remove(Object e) {
        if (e == null) {
            for (int i = 0; i < size; i++) {
                if (array[i] == null) {
                    subRemove(i);
                    return true;
                }
            }
        } else {
            for (int i = 0; i < size; i++) {
                if (e.equals(array[i])) {
                    subRemove(i);
                    return true;
                }
            }
        }
        return false;
    }


    public int size() {
        return size;
    }

    public int indexOf(Object e) {
        if (e == null) {
            for (int i = 0; i < size; i++) {
                if (array[i] == null) {
                    return i;
                }
            }
        } else {
            for (int i = 0; i < size; i++) {
                if (e.equals(array[i])) {
                    return i;
                }
            }
        }
        return -1;
    }

    public boolean contains(Object e) {
        return indexOf(e) >= 0;
    }

    public Object[] toArray() {
        return Arrays.copyOf(array, size);
    }

    public ListIterator listIterator() {
        return new ListIterator(0);
    }

    class ListIterator {
        int cursor;
        int expectedModCount = modCount;
        int lastRet = -1;

        public ListIterator(int index) {
            index = cursor;
        }

        private void checkForComodification() {
            if (modCount != expectedModCount)
                throw new ConcurrentModificationException("ConcurrentModification");
        }

        public boolean hasNext() {
            return cursor != size;
        }

        public Object next() {
            checkForComodification();
            int i = cursor;
            if (i >= size) {
                throw new NoSuchElementException();
            }
            cursor = i + 1;
            return array[lastRet = i];
        }

        public boolean hasPrevious() {
            return cursor != 0;
        }

        public Object previous() {
            checkForComodification();
            int i = cursor - 1;
            if (i < 0) {
                throw new NoSuchElementException();
            }
            cursor = i;
            return array[lastRet = i];
        }

        public int nextIndex() {
            return cursor;
        }

        public int previousIndex() {
            return cursor - 1;
        }

        public void remove() {
            if (lastRet < 0) {
                throw new IllegalStateException();
            }
            checkForComodification();
            DynamicArray.this.remove(lastRet);
            cursor = lastRet;
            lastRet = -1;
            expectedModCount = modCount;
        }

        public void set(Object e) {
            if (lastRet < 0) {
                throw new IllegalStateException();
            }
            checkForComodification();
            DynamicArray.this.set(lastRet, e);
        }

        public void add(Object e) {
            checkForComodification();
            int i = cursor;
            DynamicArray.this.add(i, e);
            cursor = i + 1;
            lastRet = -1;
            expectedModCount = modCount;
        }
    }
}