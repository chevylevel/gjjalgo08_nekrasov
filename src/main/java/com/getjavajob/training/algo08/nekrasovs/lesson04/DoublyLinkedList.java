package com.getjavajob.training.algo08.nekrasovs.lesson04;

import java.util.ConcurrentModificationException;
import java.util.ListIterator;
import java.util.NoSuchElementException;

public class DoublyLinkedList<E> extends AbstractLinkedList<E> {
    private int size;
    private Element<E> first;
    private Element<E> last;
    private int modCount = 0;

    public int size() {
        return size;
    }

    public boolean add(E val) {
        linkLast(val);
        return true;
    }

    public void add(int i, E val) {
        checkPositionForAdd(i);
        if (i == size) {
            linkLast(val);
        } else {
            linkBefore(val, element(i));
        }
    }

    public boolean remove(Object val) {
        if (val == null) {
            for (Element<E> e = first; e != null; e = e.next) {
                if (e.val == null) {
                    unlink(e);
                    return true;
                }
            }
        } else {
            for (Element<E> e = first; e != null; e = e.next) {
                if (val.equals(e.val)) {
                    unlink(e);
                    return true;
                }
            }
        }
        return false;
    }

    public E remove(int i) {
        checkPosition(i);
        return unlink(element(i));
    }

    public E get(int i) {
        checkPosition(i);
        return element(i).val;
    }

    private E unlink(Element<E> element) {
        E value = element.val;
        Element<E> previous = element.prev;
        Element<E> next = element.next;

        if (previous == null) {
            first = next;
        } else {
            previous.next = next;
        }

        if (next == null) {
            last = previous;
        } else {
            next.prev = previous;
        }

        element.val = null;
        size--;
        modCount++;
        return value;
    }

    private void checkPosition(int i) {
        if (i < 0 || i >= size) {
            throw new IndexOutOfBoundsException("index: " + i + ", size: " + size);
        }
    }

    private void checkPositionForAdd(int i) {
        if (i < 0 || i > size) {
            throw new IndexOutOfBoundsException();
        }
    }

    /**
     * attaches element with value val to the end of linked list.
     */
    private void linkLast(E val) {
        Element<E> prev = last;
        Element<E> element = new Element<>(prev, null, val);
        last = element;
        if (prev == null) {
            first = element;
        } else {
            prev.next = element;
        }
        size++;
        modCount++;
    }

    /**
     * inserts new element with value val before specified element e.
     */
    private void linkBefore(E val, Element<E> e) {
        Element<E> prev = e.prev;
        Element<E> element = new Element<>(prev, e, val);
        e.prev = element;
        if (prev == null) {
            first = element;
        } else {
            prev.next = element;
        }
        size++;
        modCount++;
    }

    /**
     * returns element of linked list with specified index.
     */
    private Element<E> element(int index) {
        if (index < (size >> 1)) {
            Element<E> element = first;
            for (int i = 0; i < index; i++) {
                element = element.next;
            }
            return element;
        } else {
            Element<E> element = last;
            for (int i = size - 1; i > index; i--) {
                element = element.prev;
            }
            return element;
        }
    }

    public ListIteratorImpl listIterator() {
        return new ListIteratorImpl(0);
    }

    private static class Element<V> {
        private Element<V> prev;
        private Element<V> next;
        private V val;

        Element(Element<V> prev, Element<V> next, V val) {
            this.prev = prev;
            this.next = next;
            this.val = val;
        }
    }

    private class ListIteratorImpl implements ListIterator<E> {
        int expectedModCount = modCount;
        private Element<E> next;
        private Element<E> lastRet = null;
        private int nextIndex;

        ListIteratorImpl(int index) {
            next = (index == size) ? null : element(index);
            nextIndex = index;
        }

        public E next() {
            checkForComodification();
            if (!hasNext()) {
                throw new NoSuchElementException();
            }
            lastRet = next;
            next = next.next;
            nextIndex++;
            return lastRet.val;
        }

        public boolean hasNext() {
            return nextIndex < size;
        }

        public E previous() {
            checkForComodification();
            if (!hasPrevious()) {
                throw new NoSuchElementException();
            }
            lastRet = next = (next == null) ? last : next.prev;
            nextIndex--;
            return lastRet.val;
        }

        public boolean hasPrevious() {
            return nextIndex > 0;
        }

        public int nextIndex() {
            return nextIndex;
        }

        public int previousIndex() {
            return nextIndex - 1;
        }

        public void set(E e) {
            if (lastRet == null) {
                throw new IllegalStateException();
            }
            checkForComodification();
            lastRet.val = e;
        }

        public void add(E e) {
            checkForComodification();

            lastRet = null;
            if (next == null) {
                linkLast(e);
            } else {
                linkBefore(e, next);
            }
            nextIndex++;
            expectedModCount++;
        }

        public void remove() {
            checkForComodification();
            if (lastRet == null) {
                throw new IllegalStateException();
            }
            Element<E> lastNext = lastRet.next;
            unlink(lastRet);
            if (next == lastRet) {
                next = lastNext;
            } else {
                nextIndex--;
            }

            lastRet = null;
            expectedModCount++;
        }

        private void checkForComodification() {
            if (expectedModCount != modCount) {
                throw new ConcurrentModificationException();
            }
        }
    }
}