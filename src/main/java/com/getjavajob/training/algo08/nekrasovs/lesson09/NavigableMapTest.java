package com.getjavajob.training.algo08.nekrasovs.lesson09;

import java.util.*;

import static com.getjavajob.training.algo08.nekrasovs.lesson09.SortedSetTest.*;
import static com.getjavajob.training.algo08.util.Assert.assertEquals;

public class NavigableMapTest {
    private static NavigableMap<String, String> emptyNm = new TreeMap<>();

    public static void main(String[] args) {
        testLowerEntry();
        testLowerKey();
        testFloorEntry();
        testFloorKey();
        testCeilingEntry();
        testCeilingKey();
        testHigherEntry();
        testHigherKey();
        testFirstEntry();
        testLastEntry();
        testPollFirstEntry();
        testPollLastEntry();
        testDescendingMap();
        testNavigableKeySet();
        testDescendingKeySet();
        testSubMap();
        testHeadMap();
        testTailMap();
    }

    public static void testLowerEntry() {
        NavigableMap<String, String> nM = new TreeMap<>();
        nM.put("k1", "1");
        nM.put("k2", "2");
        nM.put("k3", "3");
        Set<Map.Entry<String, String>> entries = nM.entrySet();
        Iterator<Map.Entry<String, String>> itr = entries.iterator();
        Map.Entry<String, String> assume = null;
        while (itr.hasNext()) {
            Map.Entry<String, String> entry = itr.next();
            if (entry.getKey().equals("k2")) {
                assume = entry;
            }
        }
        assertEquals("NavigableMapTest.testLowerEntry", assume, nM.lowerEntry("k3"));
        assertEquals("NavigableMapTest.testLowerEntry", null, nM.lowerEntry("k1"));

        NavigableMap<CustomClass, String> nMForCce = new TreeMap<>();
        nMForCce.put(new SubClass1("k1", 1, "1"), "1");
        nMForCce.put(new SubClass1("k2", 2, "2"), "2");
        nMForCce.put(new SubClass1("k3", 3, "3"), "3");
        try {
            nMForCce.lowerEntry(new SubClass2("k3", 3, "3"));
        } catch (ClassCastException e) {
            System.out.println("CCE, when passed key cannot be compared with keys in TM, was thrown");
        }

        try {
            nM.lowerEntry(null);
        } catch (NullPointerException e) {
            System.out.println("NPE, when passed null key, was thrown");
        }
        System.out.println("///////////////////////////////////////////");
    }

    public static void testLowerKey() {
        NavigableMap<String, String> nM = new TreeMap<>();
        nM.put("k1", "1");
        nM.put("k2", "2");
        nM.put("k3", "3");

        assertEquals("NavigableMapTest.testLowerKey", "k2", nM.lowerKey("k3"));
        assertEquals("NavigableMapTest.testLowerKey", null, nM.lowerKey("k1"));

        NavigableMap<CustomClass, String> nMForCce = new TreeMap<>();
        nMForCce.put(new SubClass1("k1", 1, "1"), "1");
        nMForCce.put(new SubClass1("k2", 2, "2"), "2");
        nMForCce.put(new SubClass1("k3", 3, "3"), "3");
        try {
            nMForCce.lowerKey(new SubClass2("k3", 3, "3"));
        } catch (ClassCastException e) {
            System.out.println("CCE, when passed key cannot be compared with keys in TM, was thrown");
        }

        try {
            nM.lowerKey(null);
        } catch (NullPointerException e) {
            System.out.println("NPE, when passed null key, was thrown");
        }
        System.out.println("///////////////////////////////////////////");
    }

    public static void testFloorEntry() {
        NavigableMap<String, String> nM = new TreeMap<>();
        nM.put("k1", "1");
        nM.put("k2", "2");
        nM.put("k3", "3");
        Set<Map.Entry<String, String>> entries = nM.entrySet();
        Iterator<Map.Entry<String, String>> itr = entries.iterator();
        Map.Entry<String, String> assume = null;
        while (itr.hasNext()) {
            Map.Entry<String, String> entry = itr.next();
            if (entry.getKey().equals("k2")) {
                assume = entry;
            }
        }
        assertEquals("NavigableMapTest.testFloorEntry", assume, nM.floorEntry("k2"));
        assertEquals("NavigableMapTest.testFloorEntry", null, nM.floorEntry("k0"));

        NavigableMap<CustomClass, String> nMForCce = new TreeMap<>();
        nMForCce.put(new SubClass1("k1", 1, "1"), "1");
        nMForCce.put(new SubClass1("k2", 2, "2"), "2");
        nMForCce.put(new SubClass1("k3", 3, "3"), "3");
        try {
            nMForCce.floorEntry(new SubClass2("k3", 3, "3"));
        } catch (ClassCastException e) {
            System.out.println("CCE, when passed key cannot be compared with keys in TM, was thrown");
        }

        try {
            nM.floorEntry(null);
        } catch (NullPointerException e) {
            System.out.println("NPE, when passed null key, was thrown");
        }
        System.out.println("///////////////////////////////////////////");
    }

    public static void testFloorKey() {
        NavigableMap<String, String> nM = new TreeMap<>();
        nM.put("k1", "1");
        nM.put("k2", "2");
        nM.put("k3", "3");

        assertEquals("NavigableMapTest.testFloorKey", "k3", nM.floorKey("k3"));
        assertEquals("NavigableMapTest.testFloorKey", null, nM.floorKey("k0"));

        NavigableMap<CustomClass, String> nMForCce = new TreeMap<>();
        nMForCce.put(new SubClass1("k1", 1, "1"), "1");
        nMForCce.put(new SubClass1("k2", 2, "2"), "2");
        nMForCce.put(new SubClass1("k3", 3, "3"), "3");
        try {
            nMForCce.floorKey(new SubClass2("k3", 3, "3"));
        } catch (ClassCastException e) {
            System.out.println("CCE, when passed key cannot be compared with keys in TM, was thrown");
        }

        try {
            nM.floorKey(null);
        } catch (NullPointerException e) {
            System.out.println("NPE, when passed null key, was thrown");
        }
        System.out.println("///////////////////////////////////////////");
    }

    public static void testCeilingEntry() {
        NavigableMap<String, String> nM = new TreeMap<>();
        nM.put("k1", "1");
        nM.put("k2", "2");
        nM.put("k3", "3");
        Set<Map.Entry<String, String>> entries = nM.entrySet();
        Iterator<Map.Entry<String, String>> itr = entries.iterator();
        Map.Entry<String, String> assume = null;
        while (itr.hasNext()) {
            Map.Entry<String, String> entry = itr.next();
            if (entry.getKey().equals("k2")) {
                assume = entry;
            }
        }
        assertEquals("NavigableMapTest.testCeilingEntry", assume, nM.ceilingEntry("k2"));
        assertEquals("NavigableMapTest.testCeilingEntry", null, nM.ceilingEntry("k4"));

        NavigableMap<CustomClass, String> nMForCce = new TreeMap<>();
        nMForCce.put(new SubClass1("k1", 1, "1"), "1");
        nMForCce.put(new SubClass1("k2", 2, "2"), "2");
        nMForCce.put(new SubClass1("k3", 3, "3"), "3");
        try {
            nMForCce.ceilingEntry(new SubClass2("k3", 3, "3"));
        } catch (ClassCastException e) {
            System.out.println("CCE, when passed key cannot be compared with keys in TM, was thrown");
        }

        try {
            nM.ceilingEntry(null);
        } catch (NullPointerException e) {
            System.out.println("NPE, when passed null key, was thrown");
        }
        System.out.println("///////////////////////////////////////////");
    }


    public static void testCeilingKey() {
        NavigableMap<String, String> nM = new TreeMap<>();
        nM.put("k1", "1");
        nM.put("k2", "2");
        nM.put("k3", "3");

        assertEquals("NavigableMapTest.testCeilingKey", "k3", nM.ceilingKey("k3"));
        assertEquals("NavigableMapTest.testCeilingKey", null, nM.ceilingKey("k4"));

        NavigableMap<CustomClass, String> nMForCce = new TreeMap<>();
        nMForCce.put(new SubClass1("k1", 1, "1"), "1");
        nMForCce.put(new SubClass1("k2", 2, "2"), "2");
        nMForCce.put(new SubClass1("k3", 3, "3"), "3");
        try {
            nMForCce.ceilingKey(new SubClass2("k3", 3, "3"));
        } catch (ClassCastException e) {
            System.out.println("CCE, when passed key cannot be compared with keys in TM, was thrown");
        }

        try {
            nM.ceilingKey(null);
        } catch (NullPointerException e) {
            System.out.println("NPE, when passed null key, was thrown");
        }
        System.out.println("///////////////////////////////////////////");
    }

    public static void testHigherEntry() {
        NavigableMap<String, String> nM = new TreeMap<>();
        nM.put("k1", "1");
        nM.put("k2", "2");
        nM.put("k3", "3");
        Set<Map.Entry<String, String>> entries = nM.entrySet();
        Iterator<Map.Entry<String, String>> itr = entries.iterator();
        Map.Entry<String, String> assume = null;
        while (itr.hasNext()) {
            Map.Entry<String, String> entry = itr.next();
            if (entry.getKey().equals("k3")) {
                assume = entry;
            }
        }
        assertEquals("NavigableMapTest.testHigherEntry", assume, nM.higherEntry("k2"));
        assertEquals("NavigableMapTest.testHigherEntry", null, nM.higherEntry("k3"));

        NavigableMap<CustomClass, String> nMForCce = new TreeMap<>();
        nMForCce.put(new SubClass1("k1", 1, "1"), "1");
        nMForCce.put(new SubClass1("k2", 2, "2"), "2");
        nMForCce.put(new SubClass1("k3", 3, "3"), "3");
        try {
            nMForCce.higherEntry(new SubClass2("k3", 3, "3"));
        } catch (ClassCastException e) {
            System.out.println("CCE, when passed key cannot be compared with keys in TM, was thrown");
        }

        try {
            nM.higherEntry(null);
        } catch (NullPointerException e) {
            System.out.println("NPE, when passed null key, was thrown");
        }
        System.out.println("///////////////////////////////////////////");
    }


    public static void testHigherKey() {
        NavigableMap<String, String> nM = new TreeMap<>();
        nM.put("k1", "1");
        nM.put("k2", "2");
        nM.put("k3", "3");

        assertEquals("NavigableMapTest.testHigherKey", "k3", nM.higherKey("k2"));
        assertEquals("NavigableMapTest.testHigherKey", null, nM.higherKey("k3"));

        NavigableMap<CustomClass, String> nMForCce = new TreeMap<>();
        nMForCce.put(new SubClass1("k1", 1, "1"), "1");
        nMForCce.put(new SubClass1("k2", 2, "2"), "2");
        nMForCce.put(new SubClass1("k3", 3, "3"), "3");
        try {
            nMForCce.higherKey(new SubClass2("k3", 3, "3"));
        } catch (ClassCastException e) {
            System.out.println("CCE, when passed key cannot be compared with keys in TM, was thrown");
        }

        try {
            nM.higherKey(null);
        } catch (NullPointerException e) {
            System.out.println("NPE, when passed null key, was thrown");
        }
        System.out.println("///////////////////////////////////////////");
    }

    public static void testFirstEntry() {
        NavigableMap<String, String> nM = new TreeMap<>();
        nM.put("k1", "1");
        nM.put("k2", "2");
        nM.put("k3", "3");
        Set<Map.Entry<String, String>> entries = nM.entrySet();
        Iterator<Map.Entry<String, String>> itr = entries.iterator();
        Map.Entry<String, String> assume = null;
        while (itr.hasNext()) {
            Map.Entry<String, String> entry = itr.next();
            if (entry.getKey().equals("k1")) {
                assume = entry;
            }
        }
        assertEquals("NavigableMapTest.testFirstEntry", assume, nM.firstEntry());
        assertEquals("NavigableMapTest.testFirstEntry", null, emptyNm.firstEntry());
        System.out.println("///////////////////////////////////////////");
    }

    public static void testLastEntry() {
        NavigableMap<String, String> nM = new TreeMap<>();
        nM.put("k1", "1");
        nM.put("k2", "2");
        nM.put("k3", "3");
        Set<Map.Entry<String, String>> entries = nM.entrySet();
        Iterator<Map.Entry<String, String>> itr = entries.iterator();
        Map.Entry<String, String> assume = null;
        while (itr.hasNext()) {
            Map.Entry<String, String> entry = itr.next();
            if (entry.getKey().equals("k3")) {
                assume = entry;
            }
        }
        assertEquals("NavigableMapTest.testLastEntry", assume, nM.lastEntry());
        assertEquals("NavigableMapTest.testLastEntry", null, emptyNm.lastEntry());
        System.out.println("///////////////////////////////////////////");
    }

    public static void testPollFirstEntry() {
        NavigableMap<String, String> nM = new TreeMap<>();
        nM.put("k1", "1");
        nM.put("k2", "2");
        nM.put("k3", "3");
        Set<Map.Entry<String, String>> entries = nM.entrySet();
        Iterator<Map.Entry<String, String>> itr = entries.iterator();
        Map.Entry<String, String> assume = null;
        while (itr.hasNext()) {
            Map.Entry<String, String> entry = itr.next();
            if (entry.getKey().equals("k1")) {
                assume = entry;
            }
        }
        assertEquals("NavigableMapTest.testPollFirstEntry", assume, nM.pollFirstEntry());
        assertEquals("NavigableMapTest.testPollFirstEntry", false, nM.containsKey("k1"));
        assertEquals("NavigableMapTest.testPollFirstEntry", null, emptyNm.pollFirstEntry());
        System.out.println("///////////////////////////////////////////");
    }

    public static void testPollLastEntry() {
        NavigableMap<String, String> nM = new TreeMap<>();
        nM.put("k1", "1");
        nM.put("k2", "2");
        nM.put("k3", "3");
        Set<Map.Entry<String, String>> entries = nM.entrySet();
        Iterator<Map.Entry<String, String>> itr = entries.iterator();
        Map.Entry<String, String> assume = null;
        while (itr.hasNext()) {
            Map.Entry<String, String> entry = itr.next();
            if (entry.getKey().equals("k3")) {
                assume = entry;
            }
        }
        assertEquals("NavigableMapTest.testPollLastEntry", assume, nM.pollLastEntry());
        assertEquals("NavigableMapTest.testPollLastEntry", false, nM.containsKey("k3"));
        assertEquals("NavigableMapTest.testPollLastEntry", null, emptyNm.pollLastEntry());
        System.out.println("///////////////////////////////////////////");
    }


    public static void testDescendingMap() {
        NavigableMap<String, String> nM = new TreeMap<>();
        nM.put("k1", "1");
        nM.put("k2", "2");
        nM.put("k3", "3");

        NavigableMap<String, String> assume = new TreeMap<>(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o2.compareTo(o1);
            }
        });
        assume.put("k1", "1");
        assume.put("k2", "2");
        assume.put("k3", "3");

        NavigableMap<String, String> actual = nM.descendingMap();

        assertEquals("NavigableMapTest.testDescendingMap",
                assume.entrySet().toArray(),
                actual.entrySet().toArray());
        assertEquals("NavigableMapTest.testDescendingMap",
                nM.entrySet().toArray(),
                actual.descendingMap().entrySet().toArray());

        actual.put("k4", "4");
        assertEquals("NavigableMapTest.testDescendingMap", true, nM.containsKey("k4"));

        nM.remove("k1");
        assertEquals("NavigableMapTest.testDescendingMap", false, actual.containsKey("k1"));

        System.out.println("///////////////////////////////////////////");
    }

    public static void testNavigableKeySet() {
        NavigableMap<String, String> nM = new TreeMap<>();
        nM.put("k1", "1");
        nM.put("k2", "2");
        nM.put("k3", "3");

        String[] assume = {"k1", "k2", "k3"};
        NavigableSet<String> actual = nM.navigableKeySet();

        assertEquals("NavigableMapTest.testNavigableKeySet", assume, actual.toArray());
        actual.remove("k1");
        assertEquals("NavigableMapTest.testNavigableKeySet", false, nM.navigableKeySet().contains("k1"));
        nM.remove("k3");
        assertEquals("NavigableMapTest.testNavigableKeySet", false, actual.contains("k3"));
        System.out.println("///////////////////////////////////////////");
    }

    public static void testDescendingKeySet() {
        NavigableMap<String, String> nM = new TreeMap<>();
        nM.put("k1", "1");
        nM.put("k2", "2");
        nM.put("k3", "3");

        String[] assume = {"k3", "k2", "k1"};
        NavigableSet<String> actual = nM.descendingKeySet();

        assertEquals("NavigableMapTest.testDescendingKeySet", assume, actual.toArray());
        actual.remove("k1");
        assertEquals("NavigableMapTest.testDescendingKeySet", false, nM.navigableKeySet().contains("k1"));
        nM.remove("k3");
        assertEquals("NavigableMapTest.testDescendingKeySet", false, actual.contains("k3"));
        System.out.println("///////////////////////////////////////////");
    }

    public static void testSubMap() {
        NavigableMap<String, String> nM = new TreeMap<>();
        nM.put("kn1", "1");
        nM.put("kn2", "2");
        nM.put("kn3", "3");
        nM.put("kn4", "4");
        nM.put("kn5", "5");
        nM.put("kn6", "6");
        nM.put("kn7", "7");

        String assumeTt = "{kn2=2, kn3=3, kn4=4}";
        NavigableMap<String, String> actual = nM.subMap("kn2", true, "kn4", true);
        assertEquals("NavigableMapTest.testSubMap", assumeTt, actual.toString());

        String assumeFf = "{kn3=3}";
        assertEquals("NavigableMapTest.testSubMap", assumeFf,
                nM.subMap("kn2", false, "kn4", false).toString());

        String assumeTf = "{kn2=2, kn3=3}";
        assertEquals("NavigableMapTest.testSubMap", assumeTf,
                nM.subMap("kn2", true, "kn4", false).toString());

        String assumeFt = "{kn3=3, kn4=4}";
        assertEquals("NavigableMapTest.testSubMap", assumeFt,
                nM.subMap("kn2", false, "kn4", true).toString());

        actual.remove("kn3");
        assertEquals("NavigableMapTest.testSubMap", null, nM.get("kn3"));
        nM.remove("kn2");
        assertEquals("NavigableMapTest.testSubMap", null, actual.get("kn2"));

        try {
            actual.put("kn6", "6");
        } catch (IllegalArgumentException e) {
            System.out.println("IAE, when insert key is out of range of subMap, was thrown");
        }

        NavigableMap<Number, String> tMForCce = new TreeMap<>();
        try {
            tMForCce.subMap(4, true, 5.5, true);
        } catch (ClassCastException e) {
            System.out.println("CCE, when passed keys cannot be compared to one another, was thrown");
        }

        try {
            actual.subMap(null, true, "6", true);
        } catch (NullPointerException e) {
            System.out.println("NPE, when passed null key, was thrown");
        }

        try {
            actual.subMap("kn7", true, "kn6", true);
        } catch (IllegalArgumentException e) {
            System.out.println("IAE, when fromKey > toKey, was thrown");
        }
        System.out.println("///////////////////////////////////////////");
    }

    public static void testHeadMap() {
        NavigableMap<String, String> nM = new TreeMap<>();
        nM.put("kn1", "1");
        nM.put("kn2", "2");
        nM.put("kn3", "3");
        nM.put("kn4", "4");
        nM.put("kn5", "5");
        nM.put("kn6", "6");
        nM.put("kn7", "7");

        String assumeT = "{kn1=1, kn2=2, kn3=3}";
        NavigableMap<String, String> actual = nM.headMap("kn3", true);

        assertEquals("NavigableMapTest.testHeadMap", assumeT, actual.toString());
        actual.remove("kn1");
        assertEquals("NavigableMapTest.testHeadMap", null, nM.get("kn1"));
        nM.remove("kn2");
        assertEquals("NavigableMapTest.testHeadMap", null, actual.get("kn2"));

        try {
            actual.put("kn5", "5");
        } catch (IllegalArgumentException e) {
            System.out.println("IAE, when insert key is out of range of subMap, was thrown");
        }

        SortedMap<CustomClass, String> tMForCce = new TreeMap<>();
        try {
            tMForCce.headMap(new CustomClass("k1", 1));
        } catch (ClassCastException e) {
            System.out.println("CCE, when passed key does not implement Comparable, was thrown");
        }

        try {
            nM.headMap(null);
        } catch (NullPointerException e) {
            System.out.println("NPE, when passed null key, was thrown");
        }

        try {
            actual.headMap("kn4");
        } catch (IllegalArgumentException e) {
            System.out.println("IAE, when toKey is out of range of subMap, was thrown");
        }
        System.out.println("///////////////////////////////////////////");
    }

    public static void testTailMap() {
        NavigableMap<String, String> nM = new TreeMap<>();
        nM.put("kn1", "1");
        nM.put("kn2", "2");
        nM.put("kn3", "3");
        nM.put("kn4", "4");
        nM.put("kn5", "5");
        nM.put("kn6", "6");
        nM.put("kn7", "7");

        String assumeF = "{kn6=6, kn7=7}";

        NavigableMap<String, String> actual = nM.tailMap("kn5", false);

        assertEquals("NavigableMapTest.testTailMap", assumeF, actual.toString());
        actual.remove("kn6");
        assertEquals("NavigableMapTest.testTailMap", null, nM.get("kn6"));
        nM.remove("kn7");
        assertEquals("NavigableMapTest.testTailMap", null, actual.get("kn7"));

        try {
            actual.put("kn5", "5");
        } catch (IllegalArgumentException e) {
            System.out.println("IAE, when inserted key is out of range of subMap, was thrown");
        }

        SortedMap<CustomClass, String> tMForCce = new TreeMap<>();

        try {
            tMForCce.tailMap(new CustomClass("k1", 1));
        } catch (ClassCastException e) {
            System.out.println("CCE, when passed keys does no implement Comparable, was thrown");
        }

        try {
            nM.tailMap(null);
        } catch (NullPointerException e) {
            System.out.println("NPE, when passed null key, was thrown");
        }

        try {
            actual.tailMap("kn5");
        } catch (IllegalArgumentException e) {
            System.out.println("IAE, when fromKey is out of range of subMap, was thrown");
        }
        System.out.println("///////////////////////////////////////////");
    }
}