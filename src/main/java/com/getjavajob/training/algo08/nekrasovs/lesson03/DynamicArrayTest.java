package com.getjavajob.training.algo08.nekrasovs.lesson03;

import com.getjavajob.training.algo08.util.Assert;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;

public class DynamicArrayTest {
    public static void main(String[] args) {
        testAdd();
        testVoidAddToBegin();
        testVoidAddToMiddle();
        testVoidAddToEnd();
        testSet();
        testGet();
        testRemoveBegin();
        testRemoveMiddle();
        testRemoveEnd();
        testRemove();
        testSize();
        testIndexOf();
        testContains();
        testToArray();
        testHasNext();
        testNext();
        testHasPrevious();
        testPrevious();
        testNextIndex();
        testPreviousIndex();
        testListIteratorRemove();
        testListIteratorSet();
        testListIteratorAdd();
    }

    public static void testAdd() {
        DynamicArray da = new DynamicArray(0);
        da.add(1);
        assertEquals("DynamicArrayTest.testAdd", 1, da.get(0));
    }

    public static void testVoidAddToBegin() {
        DynamicArray da = new DynamicArray(0);
        da.add(0, "Begin");
        da.add(1, "Middle");
        da.add(2, "End");
        da.add(0, "test");
        assertEquals("DynamicArrayTest.testAddToBegin", "test", da.get(0));
    }

    public static void testVoidAddToMiddle() {
        DynamicArray da = new DynamicArray(0);
        da.add(0, "Begin");
        da.add(1, "Middle");
        da.add(2, "End");
        da.add(1, "test");
        assertEquals("DynamicArrayTest.testAddToMiddle", "test", da.get(1));
    }

    public static void testVoidAddToEnd() {
        DynamicArray da = new DynamicArray(0);
        da.add(0, "Begin");
        da.add(1, "Middle");
        da.add(2, "End");
        da.add(3, "test");
        assertEquals("DynamicArrayTest.testAddToEnd", "test", da.get(3));
        try {
            da.add(5, "test");
            Assert.fail("Needed exception was not thrown");
        } catch (Exception e) {
            Assert.equals("IndexOutOfBounds", e.getMessage());
        }
    }

    public static void testSet() {
        DynamicArray da = new DynamicArray(0);
        da.add("Begin");
        da.add("Middle");
        da.add("End");
        da.set(1, "test");
        assertEquals("DynamicArrayTest.testSet", "test", da.get(1));
        try {
            da.set(3, "test");
            Assert.fail("Needed exception was not thrown");
        } catch (Exception e) {
            Assert.equals("IndexOutOfBounds", e.getMessage());
        }
    }

    public static void testGet() {
        DynamicArray da = new DynamicArray(0);
        da.add("Begin");
        da.add("Middle");
        da.add("End");
        assertEquals("DynamicArrayTest.testGet", "Begin", da.get(0));
        try {
            da.get(3);
            Assert.fail("Needed exception was not thrown");
        } catch (Exception e) {
            Assert.equals("IndexOutOfBounds", e.getMessage());
        }
    }

    public static void testRemoveBegin() {
        DynamicArray da = new DynamicArray(0);
        da.add("Begin");
        da.add("Middle");
        da.add("End");
        da.remove(0);
        assertEquals("DynamicArrayTest.testRemoveBegin", false, da.contains("Begin"));
    }

    public static void testRemoveMiddle() {
        DynamicArray da = new DynamicArray(0);
        da.add("Begin");
        da.add("Middle");
        da.add("End");
        da.remove(1);
        assertEquals("DynamicArrayTest.testRemoveMiddle", false, da.contains("Middle"));
    }

    public static void testRemoveEnd() {
        DynamicArray da = new DynamicArray(0);
        da.add("Begin");
        da.add("Middle");
        da.add("End");
        da.remove(2);
        assertEquals("DynamicArrayTest.testRemoveEnd", false, da.contains("End"));
        try {
            da.remove(2);
            Assert.fail("Needed exception was not thrown");
        } catch (Exception e) {
            Assert.equals("IndexOutOfBounds", e.getMessage());
        }
    }

    public static void testRemove() {
        DynamicArray da = new DynamicArray(0);
        da.add("Begin");
        da.add("Middle");
        da.add("End");
        da.remove("Begin");
        assertEquals("DynamicArrayTest.testRemove", false, da.contains("Begin"));
    }

    public static void testSize() {
        DynamicArray da = new DynamicArray(0);
        da.add("Begin");
        da.add("Middle");
        da.add("End");
        assertEquals("DynamicArrayTest.testSize", 3, da.size());
    }

    public static void testIndexOf() {
        DynamicArray da = new DynamicArray(0);
        da.add("Begin");
        da.add("Middle");
        da.add("End");
        assertEquals("DynamicArrayTest.testIndexOf", 1, da.indexOf("Middle"));
    }

    public static void testContains() {
        DynamicArray da = new DynamicArray(0);
        da.add("Begin");
        da.add("Middle");
        da.add("End");
        assertEquals("DynamicArrayTest.testContains", true, da.contains("Middle"));
    }

    public static void testToArray() {
        DynamicArray da = new DynamicArray(0);
        da.add("Begin");
        da.add("Middle");
        da.add("End");
        Object[] testArray = {"Begin", "Middle", "End"};
        assertEquals("DynamicArrayTest.testToArray", testArray, da.toArray());
    }

    public static void testHasNext() {
        DynamicArray da = new DynamicArray(0);
        da.add("Begin");
        da.add("Middle");
        da.add("End");
        DynamicArray.ListIterator itr = da.listIterator();
        assertEquals("DynamicArrayTest.testHasNext", true, itr.hasNext());
    }

    public static void testNext() {
        DynamicArray da = new DynamicArray(0);
        da.add("Begin");
        da.add("Middle");
        da.add("End");
        DynamicArray.ListIterator itr = da.listIterator();
        assertEquals("DynamicArrayTest.testNext", "Begin", itr.next());
        try {
            da.add("CoModification");
            itr.next(); // testing expression
            Assert.fail("ConcurrentModification exception was not thrown");
        } catch (Exception e) {
            Assert.equals("ConcurrentModification", e.getMessage());
        }
        try {
            DynamicArray.ListIterator itr2 = da.listIterator();
            itr2.add("CoModification");
            itr.next(); // testing expression
            Assert.fail("ConcurrentModification exception was not thrown");
        } catch (Exception e) {
            Assert.equals("ConcurrentModification", e.getMessage());
        }
    }

    public static void testHasPrevious() {
        DynamicArray da = new DynamicArray(0);
        da.add("Begin");
        da.add("Middle");
        da.add("End");
        DynamicArray.ListIterator itr = da.listIterator();
        assertEquals("DynamicArrayTest.testHasPrevious", false, itr.hasPrevious());
    }

    public static void testPrevious() {
        DynamicArray da = new DynamicArray(0);
        da.add("Begin");
        da.add("Middle");
        da.add("End");
        DynamicArray.ListIterator itr = da.listIterator();
        itr.next();
        assertEquals("DynamicArrayTest.testPrevious", "Begin", itr.previous());
        try {
            da.add("CoModification");
            itr.previous(); // testing expression
            Assert.fail("ConcurrentModification exception was not thrown");
        } catch (Exception e) {
            Assert.equals("ConcurrentModification", e.getMessage());
        }
        try {
            DynamicArray.ListIterator itr2 = da.listIterator();
            itr2.add("CoModification");
            itr.previous(); // testing expression
            Assert.fail("ConcurrentModification exception was not thrown");
        } catch (Exception e) {
            Assert.equals("ConcurrentModification", e.getMessage());
        }
    }

    public static void testNextIndex() {
        DynamicArray da = new DynamicArray(0);
        da.add("Begin");
        da.add("Middle");
        da.add("End");
        DynamicArray.ListIterator itr = da.listIterator();
        assertEquals("DynamicArrayTest.testNextIndex", 0, itr.nextIndex());
    }

    public static void testPreviousIndex() {
        DynamicArray da = new DynamicArray(0);
        da.add("Begin");
        da.add("Middle");
        da.add("End");
        DynamicArray.ListIterator itr = da.listIterator();
        itr.next();
        assertEquals("DynamicArrayTest.testPreviousIndex", 0, itr.previousIndex());
    }

    public static void testListIteratorRemove() {
        DynamicArray da = new DynamicArray(0);
        da.add("Begin");
        da.add("Middle");
        da.add("End");
        DynamicArray.ListIterator itr = da.listIterator();
        itr.next();
        itr.remove();
        assertEquals("DynamicArrayTest.testListIteratorRemove", false, da.contains("Begin"));
        try {
            itr.next();
            da.add("CoModification");
            itr.remove(); // testing expression
            Assert.fail("ConcurrentModification exception was not thrown");
        } catch (Exception e) {
            Assert.equals("ConcurrentModification", e.getMessage());
        }
        try {
            DynamicArray.ListIterator itr2 = da.listIterator();
            itr.next();
            itr2.add("CoModification");
            itr.remove(); // testing expression
            Assert.fail("ConcurrentModification exception was not thrown");
        } catch (Exception e) {
            Assert.equals("ConcurrentModification", e.getMessage());
        }
    }

    public static void testListIteratorSet() {
        DynamicArray da = new DynamicArray(0);
        da.add("Begin");
        da.add("Middle");
        da.add("End");
        DynamicArray.ListIterator itr = da.listIterator();
        itr.next(); // Begin returned
        itr.set("testSetObject");
        assertEquals("DynamicArrayTest.testListIteratorSet", "testSetObject", da.get(0));
        try {
            itr.next();
            da.add("CoModification");
            itr.set(" "); // testing expression
            Assert.fail("ConcurrentModification exception was not thrown");
        } catch (Exception e) {
            Assert.equals("ConcurrentModification", e.getMessage());
        }
        try {
            DynamicArray.ListIterator itr2 = da.listIterator();
            itr.next();
            itr2.add("CoModification");
            itr.set(" "); // testing expression
            Assert.fail("ConcurrentModification exception was not thrown");
        } catch (Exception e) {
            Assert.equals("ConcurrentModification", e.getMessage());
        }

    }

    public static void testListIteratorAdd() {
        DynamicArray da = new DynamicArray(0);
        da.add("Begin");
        da.add("Middle");
        da.add("End");
        DynamicArray.ListIterator itr = da.listIterator();
        itr.add("testAddObject");
        assertEquals("DynamicArrayTest.testListIteratorAdd", "testAddObject", da.get(0));
        try {
            da.add("CoModification");
            itr.add("0"); // testing expression
            Assert.fail("ConcurrentModification exception was not thrown");
        } catch (Exception e) {
            Assert.equals("ConcurrentModification", e.getMessage());
        }
        try {
            DynamicArray.ListIterator itr2 = da.listIterator();
            itr2.add("CoModification");
            itr.add("0"); // testing expression
            Assert.fail("ConcurrentModification exception was not thrown");
        } catch (Exception e) {
            Assert.equals("ConcurrentModification", e.getMessage());
        }
    }
}