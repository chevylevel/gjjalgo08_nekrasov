package com.getjavajob.training.algo08.nekrasovs.lesson08.tree.binary.search;

import com.getjavajob.training.algo08.nekrasovs.lesson07.tree.Node;

import java.util.Comparator;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;

public class BinarySearchTreeTest {
    private static Comparator<MyClass> cmp = new Comparator<MyClass>() {
        @Override
        public int compare(MyClass o1, MyClass o2) {
            if (o1.value > o2.value) {
                return 1;
            } else if (o1.value < o2.value) {
                return -1;
            } else {
                return 0;
            }
        }
    };
    private static BinarySearchTree<MyClass> testTree = new BinarySearchTree<>(cmp);
    private static Node<MyClass> n10 = testTree.add(new MyClass("n10", 10));
    private static Node<MyClass> n8 = testTree.add(new MyClass("n8", 8));
    private static Node<MyClass> n12 = testTree.add(new MyClass("n12", 12));
    private static Node<MyClass> n6 = testTree.add(new MyClass("n6", 6));
    private static Node<MyClass> n9 = testTree.add(new MyClass("n9", 9));
    private static Node<MyClass> n11 = testTree.add(new MyClass("n11", 11));
    private static Node<MyClass> n15 = testTree.add(new MyClass("n15", 15));

    private static BinarySearchTree<String> t = new BinarySearchTree<>();
    private static Node<String> n2 = t.add("8");
    private static Node<String> n1 = t.add("10");
    private static Node<String> n3 = t.add("9");

    public static void main(String[] args) {
        System.out.println(testTree.toString());
        testTree.remove(n6);
        testTree.remove(n9);
        System.out.println(testTree.toString());
        testTreeSearch();
        testToString();
        System.out.println(t.toString());

    }

    public static void testTreeSearch() {
        assertEquals("BinarySearchTreeTest.testTreeSearch", null, testTree.treeSearch(n10, new MyClass("n6", 6)));
        assertEquals("BinarySearchTreeTest.testTreeSearch", n15, testTree.treeSearch(n10, new MyClass("n15", 15)));
    }

    public static void testToString() {
        assertEquals("AbstractTree.testToString",
                "(n10(n8, n12(n11, n15)))",
                testTree.toString());
    }

    private static class MyClass {
        private String name;
        private int value;

        public MyClass(String name, int value) {
            this.name = name;
            this.value = value;
        }

        @Override
        public String toString() {
            return name;
        }
    }
}