package com.getjavajob.training.algo08.nekrasovs.lesson08.tree.binary.search.balanced;

import com.getjavajob.training.algo08.nekrasovs.lesson07.tree.Node;
import com.getjavajob.training.algo08.nekrasovs.lesson08.tree.binary.search.BinarySearchTree;

public abstract class BalanceableTree<E> extends BinarySearchTree<E> {

    /**
     * Sets new relationship between parent and child. This method is used by
     * {@link #rotate(Node)} for node and its grandparent,
     * node and its parent, node's child and node's parent relinking.
     *
     * @param parent        new parent
     * @param child         new child
     * @param makeLeftChild whether new child must be left or right
     */
    private void relink(NodeImpl<E> parent, NodeImpl<E> child, boolean makeLeftChild) {
        if (makeLeftChild) {
            parent.setLeft(child);
        } else {
            parent.setRight(child);
        }
        child.setParent(parent);
    }

    /**
     * Rotates n with it's parent.
     *
     * @param n node to rotate above its parent
     */
    protected void rotate(Node<E> n) {
        NodeImpl<E> newParent = (NodeImpl<E>) n;
        NodeImpl<E> grandN = (NodeImpl<E>) parent(parent(n));
        NodeImpl<E> newChild = (NodeImpl<E>) parent(n);
        boolean makeLeftChild = (n == right(newChild));

        if (makeLeftChild) {
            newChild.setRight(left(n));
            if (left(newParent) != null) {
                ((NodeImpl<E>) left(newParent)).setParent(newChild);
            }
        } else {
            newChild.setLeft(right(n));
            if (right(newParent) != null) {
                ((NodeImpl<E>) right(newParent)).setParent(newChild);
            }
        }

        relink(newParent, newChild, makeLeftChild);

        if (grandN != null) {
            if (newChild == left(grandN)) {
                grandN.setLeft(newParent);
            } else {
                grandN.setRight(newParent);
            }
        } else {
            root = newParent;
        }
        newParent.setParent(grandN);
    }

    /**
     * Performs one rotation of <i>n</i>'s parent node or two rotations of <i>n</i> by the means of
     * {@link #rotate(Node)} to reduce the height of subtree rooted at <i>n1</i>
     * <p>
     * <pre>
     *     n1         n2           n1           n
     *    /          /  \         /            / \
     *   n2    ==>  n   n1  or  n2     ==>   n2   n1
     *  /                         \
     * n                           n
     * </pre>
     * <p>
     * Similarly for subtree with right side children.
     *
     * @param n grand child of subtree root node
     * @return new subtree root
     */
    protected Node<E> reduceSubtreeHeight(Node<E> n) {
        Node<E> pivot = parent(n);
        Node<E> subRoot = parent(parent(n));
//left left || right right
        if (left(subRoot) == pivot && left(pivot) == n ||
                right(subRoot) == pivot && right(pivot) == n) {
            rotate(pivot);
            return pivot;

//left right || right left
        } else {
            rotate(n);
            rotate(n);
            return n;
        }
    }
}