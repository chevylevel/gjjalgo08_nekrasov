package com.getjavajob.training.algo08.nekrasovs.lesson05;

public interface MyClosure<E> {
    E apply(E element);
}