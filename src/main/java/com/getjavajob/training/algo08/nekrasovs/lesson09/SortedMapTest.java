package com.getjavajob.training.algo08.nekrasovs.lesson09;

import java.util.*;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;

public class SortedMapTest {
    private static Comparator<KeyClass> cmp = new Comparator<KeyClass>() {
        @Override
        public int compare(KeyClass o1, KeyClass o2) {
            if (o1.value > o2.value) {
                return 1;
            } else if (o1.value < o2.value) {
                return -1;
            } else {
                return 0;
            }
        }
    };

    private static SortedMap<String, String> emptyTm = new TreeMap<>();

    public static void main(String[] args) {
        testComparator();
        testSubMap();
        testHeadMap();
        testTailMap();
        testFirstKey();
        testLastKey();
        testKeySet();
        testValues();
        testEntrySet();
    }

    public static void testComparator() {
        SortedMap<String, String> natOrd = new TreeMap<>();
        SortedMap<KeyClass, String> comOrd = new TreeMap<>(cmp);

        assertEquals("SortedMapTest.testComparator", cmp, comOrd.comparator());
        assertEquals("SortedMapTest.testComparator", null, natOrd.comparator());
        System.out.println("///////////////////////////////////////////");
    }

    public static void testSubMap() {
        SortedMap<String, String> tM = new TreeMap<>();
        tM.put("kn1", "1");
        tM.put("kn2", "2");
        tM.put("kn3", "3");
        tM.put("kn4", "4");
        tM.put("kn5", "5");
        tM.put("kn6", "6");
        tM.put("kn7", "7");

        SortedMap<String, String> assumedTm = new TreeMap<>();
        assumedTm.put("kn2", "2");
        assumedTm.put("kn3", "3");

        SortedMap<String, String> actualTm = tM.subMap("kn2", "kn4");

        assertEquals("SortedMapTest.testSubMap", assumedTm, actualTm);
        actualTm.remove("kn3");
        assertEquals("SortedMapTest.testSubMap", null, tM.get("kn3"));
        tM.remove("kn2");
        assertEquals("SortedMapTest.testSubMap", null, actualTm.get("kn2"));

        try {
            actualTm.put("kn6", "6");
        } catch (IllegalArgumentException e) {
            System.out.println("IAE, when insert key is out of range of subMap, was thrown");
        }

        SortedMap<Number, String> tMForCce = new TreeMap<>();
        try {
            tMForCce.subMap(4, 5.5);
        } catch (ClassCastException e) {
            System.out.println("CCE, when passed keys cannot be compared to one another, was thrown");
        }

        try {
            actualTm.subMap(null, "6");
        } catch (NullPointerException e) {
            System.out.println("NPE, when passed null key, was thrown");
        }

        try {
            actualTm.subMap("kn7", "kn6");
        } catch (IllegalArgumentException e) {
            System.out.println("IAE, when fromKey > toKey, was thrown");
        }
        System.out.println("///////////////////////////////////////////");
    }

    public static void testHeadMap() {
        SortedMap<String, String> tM = new TreeMap<>();
        tM.put("kn1", "1");
        tM.put("kn2", "2");
        tM.put("kn3", "3");
        tM.put("kn4", "4");
        tM.put("kn5", "5");
        tM.put("kn6", "6");
        tM.put("kn7", "7");

        SortedMap<String, String> assumedTm = new TreeMap<>();
        assumedTm.put("kn1", "1");
        assumedTm.put("kn2", "2");

        SortedMap<String, String> actualTm = tM.headMap("kn3");

        assertEquals("SortedMapTest.testHeadMap", assumedTm, actualTm);
        actualTm.remove("kn1");
        assertEquals("SortedMapTest.testHeadMap", null, tM.get("kn1"));
        tM.remove("kn2");
        assertEquals("SortedMapTest.testHeadMap", null, actualTm.get("kn2"));

        try {
            actualTm.put("kn5", "5");
        } catch (IllegalArgumentException e) {
            System.out.println("IAE, when insert key is out of range of subMap, was thrown");
        }

        SortedMap<KeyClass, String> tMForCce = new TreeMap<>();
        try {
            tMForCce.headMap(new KeyClass("k2", 2));
        } catch (ClassCastException e) {
            System.out.println("CCE, when passed key does not implement Comparable, was thrown");
        }

        try {
            tM.headMap(null);
        } catch (NullPointerException e) {
            System.out.println("NPE, when passed null key, was thrown");
        }

        try {
            actualTm.headMap("kn4");
        } catch (IllegalArgumentException e) {
            System.out.println("IAE, when toKey is out of range of subMap, was thrown");
        }
        System.out.println("///////////////////////////////////////////");
    }

    public static void testTailMap() {
        SortedMap<String, String> tM = new TreeMap<>();
        tM.put("kn1", "1");
        tM.put("kn2", "2");
        tM.put("kn3", "3");
        tM.put("kn4", "4");
        tM.put("kn5", "5");
        tM.put("kn6", "6");
        tM.put("kn7", "7");

        SortedMap<String, String> assumedTm = new TreeMap<>();
        assumedTm.put("kn6", "6");
        assumedTm.put("kn7", "7");

        SortedMap<String, String> actualTm = tM.tailMap("kn6");

        assertEquals("SortedMapTest.testTailMap", assumedTm, actualTm);
        actualTm.remove("kn6");
        assertEquals("SortedMapTest.testTailMap", null, tM.get("kn6"));
        tM.remove("kn7");
        assertEquals("SortedMapTest.testTailMap", null, actualTm.get("kn7"));

        try {
            actualTm.put("kn5", "5");
        } catch (IllegalArgumentException e) {
            System.out.println("IAE, when insert key is out of range of subMap, was thrown");
        }

        SortedMap<KeyClass, String> tMForCce = new TreeMap<>();

        try {
            tMForCce.tailMap(new KeyClass("k1", 1));
        } catch (ClassCastException e) {
            System.out.println("CCE, when passed keys does no implement Comparable, was thrown");
        }

        try {
            tM.tailMap(null);
        } catch (NullPointerException e) {
            System.out.println("NPE, when passed null key, was thrown");
        }

        try {
            actualTm.tailMap("kn5");
        } catch (IllegalArgumentException e) {
            System.out.println("IAE, when fromKey is out of range of subMap, was thrown");
        }
        System.out.println("///////////////////////////////////////////");
    }

    public static void testFirstKey() {
        SortedMap<String, String> tM = new TreeMap<>();
        tM.put("kn1", "1");
        tM.put("kn2", "2");
        tM.put("kn3", "3");

        assertEquals("SortedMapTest.testFirstKey", "kn1", tM.firstKey());

        try {
            emptyTm.firstKey();
        } catch (NoSuchElementException e) {
            System.out.println("NoSuchElementException was thrown");
        }
        System.out.println("///////////////////////////////////////////");
    }

    private static void testLastKey() {
        SortedMap<String, String> tM = new TreeMap<>();
        tM.put("kn1", "1");
        tM.put("kn2", "2");
        tM.put("kn3", "3");

        assertEquals("SortedMapTest.testLastKey", "kn3", tM.lastKey());

        try {
            emptyTm.lastKey();
        } catch (NoSuchElementException e) {
            System.out.println("NoSuchElementException was thrown");
        }
        System.out.println("///////////////////////////////////////////");
    }

    private static void testKeySet() {
        SortedMap<String, String> tM = new TreeMap<>();
        tM.put("kn1", "1");
        tM.put("kn2", "2");
        tM.put("kn3", "3");
        tM.put("kn4", "4");
        tM.put("kn5", "5");
        tM.put("kn6", "6");
        tM.put("kn7", "7");

        Set<String> assumedKs = new TreeSet<>();
        assumedKs.add("kn1");
        assumedKs.add("kn2");
        assumedKs.add("kn3");
        assumedKs.add("kn4");
        assumedKs.add("kn5");
        assumedKs.add("kn6");
        assumedKs.add("kn7");

        Set<String> actualKs = tM.keySet();
        assertEquals("SortedMapTest.testKeySet", assumedKs, actualKs);
        actualKs.remove("kn7");
        assertEquals("SortedMapTest.testKeySet", null, tM.get("kn7"));
        tM.remove("kn6");
        assertEquals("SortedMapTest.testKeySet", false, actualKs.contains("kn6"));
        System.out.println("///////////////////////////////////////////");
    }

    public static void testValues() {
        SortedMap<String, String> tM = new TreeMap<>();
        tM.put("kn1", "1");
        tM.put("kn2", "2");
        tM.put("kn3", "3");
        tM.put("kn4", "4");
        tM.put("kn5", "5");
        tM.put("kn6", "6");
        tM.put("kn7", "7");

        String[] assumed = {"1", "2", "3", "4", "5", "6", "7"};

        Collection<String> actual = tM.values();
        assertEquals("SortedMapTest.testValues", assumed, actual.toArray());
        actual.remove("7");
        assertEquals("SortedMapTest.testValues", null, tM.get("kn7"));
        tM.remove("kn6");
        assertEquals("SortedMapTest.testValues", false, actual.contains("6"));
        System.out.println("///////////////////////////////////////////");
    }

    public static void testEntrySet() {
        SortedMap<String, String> tM = new TreeMap<>();
        tM.put("kn1", "1");
        tM.put("kn2", "2");
        tM.put("kn3", "3");
        tM.put("kn4", "4");
        tM.put("kn5", "5");
        tM.put("kn6", "6");
        tM.put("kn7", "7");

        SortedMap<String, String> TreeMapFromSet = new TreeMap<>();
        Set<Map.Entry<String, String>> entries = tM.entrySet();
        Iterator<Map.Entry<String, String>> itr = entries.iterator();
        Map.Entry<String, String> entryToRemove = null;
        while (itr.hasNext()) {
            Map.Entry<String, String> entry = itr.next();
            TreeMapFromSet.put(entry.getKey(), entry.getValue());
            if (entry.getKey().equals("kn7")) {
                entryToRemove = entry;
            }
        }
        assertEquals("SortedMapTest.testEntrySet", tM, TreeMapFromSet);
        entries.remove(entryToRemove);
        assertEquals("SortedMapTest.testEntrySet", null, tM.get("kn7"));
        tM.remove("kn6");
        boolean isKn6Removed = true;
        while (itr.hasNext()) {
            if (itr.next().getKey().equals("kn6")) {
                isKn6Removed = false;
            }
        }
        assertEquals("SortedMapTest.testKeySet", true, isKn6Removed);
        System.out.println("///////////////////////////////////////////");
    }

    private static class KeyClass {
        String name;
        int value;

        public KeyClass(String name, int value) {
            this.name = name;
            this.value = value;
        }

        @Override
        public String toString() {
            return String.valueOf(name);
        }
    }
}