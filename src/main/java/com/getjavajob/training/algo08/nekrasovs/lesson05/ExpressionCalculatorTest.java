package com.getjavajob.training.algo08.nekrasovs.lesson05;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;

public class ExpressionCalculatorTest {

    public static void main(String[] args) {
        testIsExpCorrect1();
        testIsExpCorrect2();
        testIsExpCorrect3();
        testIsExpCorrect4();
        testIsExpCorrect5();
        testIsExpCorrect6();
        testIsExpCorrect7();
        testIsExpCorrect8();
        testIsExpCorrect9();
        testIsExpCorrect10();
        testIsExpCorrect11();
        testIsExpCorrect12();
        testConvertInfixToPostfix();
        testCalcPostfixExp();
    }

    public static void testIsExpCorrect1() {
        assertEquals("ExpressionCalculatorTest.testIsExpCorrect", false, ExpressionCalculator.isExpCorrect("1++2"));
    }

    public static void testIsExpCorrect2() {
        assertEquals("ExpressionCalculatorTest.testIsExpCorrect", false, ExpressionCalculator.isExpCorrect("1+2+"));
    }

    public static void testIsExpCorrect3() {
        assertEquals("ExpressionCalculatorTest.testIsExpCorrect", false, ExpressionCalculator.isExpCorrect("+1+2"));
    }

    public static void testIsExpCorrect4() {
        assertEquals("ExpressionCalculatorTest.testIsExpCorrect", false, ExpressionCalculator.isExpCorrect("(1+2"));
    }

    public static void testIsExpCorrect5() {
        assertEquals("ExpressionCalculatorTest.testIsExpCorrect", false, ExpressionCalculator.isExpCorrect("1++2)"));
    }

    public static void testIsExpCorrect6() {
        assertEquals("ExpressionCalculatorTest.testIsExpCorrect", false, ExpressionCalculator.isExpCorrect("1+(+2+1)"));
    }

    public static void testIsExpCorrect7() {
        assertEquals("ExpressionCalculatorTest.testIsExpCorrect", false, ExpressionCalculator.isExpCorrect("(1)+2"));
    }

    public static void testIsExpCorrect8() {
        assertEquals("ExpressionCalculatorTest.testIsExpCorrect", false, ExpressionCalculator.isExpCorrect("1+()+2"));
    }

    public static void testIsExpCorrect9() {
        assertEquals("ExpressionCalculatorTest.testIsExpCorrect", false, ExpressionCalculator.isExpCorrect("4*(1+2+)"));
    }

    public static void testIsExpCorrect10() {
        assertEquals("ExpressionCalculatorTest.testIsExpCorrect", false, ExpressionCalculator.isExpCorrect("1++2(1+2)"));
    }

    public static void testIsExpCorrect11() {
        assertEquals("ExpressionCalculatorTest.testIsExpCorrect", false, ExpressionCalculator.isExpCorrect("(1++2)3"));
    }

    public static void testIsExpCorrect12() {
        assertEquals("ExpressionCalculatorTest.testIsExpCorrect", false, ExpressionCalculator.isExpCorrect("1=2"));
    }

    public static void testConvertInfixToPostfix() {
        assertEquals("ExpressionCalculatorTest.testConvertInfixToPostfix", "1 2 +3 *",
                ExpressionCalculator.convertInfixToPostfix("(1+2)*3"));
    }

    public static void testCalcPostfixExp() {
        assertEquals("ExpressionCalculatorTest.testCalcPostfixExp", 9,
                ExpressionCalculator.calcPostfixExp("1 2 +3 *"));
    }
}