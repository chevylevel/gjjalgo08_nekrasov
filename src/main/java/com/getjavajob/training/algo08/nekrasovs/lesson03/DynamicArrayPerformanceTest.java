package com.getjavajob.training.algo08.nekrasovs.lesson03;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.getjavajob.training.algo08.util.StopWatch.getElapsedTime;
import static com.getjavajob.training.algo08.util.StopWatch.start;

public class DynamicArrayPerformanceTest {
    public static void main(String[] args) {
        //testAddAl();
        //testAddDa();

        //testAddToBeginAl();
        //testAddToBeginDa();

        //testAddToMiddleAl();
        //testAddToMiddleDa();

        //testAddToEndAl();
        //testAddToEndDa();

        //testRemoveAl();
        //testRemoveDa();

        //testRemoveBeginAl();
        //testRemoveBeginDa();

        //testRemoveMiddleAl();
        //testRemoveMiddleDa();

        //testRemoveEndAl();
        //testRemoveEndDa();
    }

    public static void testAddAl() {
        int[] testArray = new int[10_000_000];
        Arrays.fill(testArray, 1);
        List<Object> al = new ArrayList<>();
        start();
        for (int i : testArray) {
            al.add(i);
        }
        long time = getElapsedTime();
        System.out.println("ArrayList.add(e): " + time);
    }

    public static void testAddDa() {
        int[] testArray = new int[10_000_000];
        Arrays.fill(testArray, 1);
        DynamicArray da = new DynamicArray();
        start();
        for (int i : testArray) {
            da.add(i);
        }
        long time = getElapsedTime();
        System.out.println("DynamicArray.add(e): " + time);
    }

    public static void testAddToBeginAl() {
        int[] testArray = new int[130_000];
        Arrays.fill(testArray, 1);
        List<Object> al = new ArrayList<>();
        start();
        for (int i : testArray) {
            al.add(0, i);
        }
        long time = getElapsedTime();
        System.out.println("ArrayList.add(0, e): " + time);
    }

    public static void testAddToBeginDa() {
        int[] testArray = new int[130_000];
        Arrays.fill(testArray, 1);
        DynamicArray da = new DynamicArray();
        start();
        for (int i : testArray) {
            da.add(0, i);
        }
        long time = getElapsedTime();
        System.out.println("DynamicArray.add(0, e): " + time);
    }

    public static void testAddToMiddleAl() {
        int[] testArray = new int[130_000];
        Arrays.fill(testArray, 1);
        List<Object> al = new ArrayList<>();
        for (int i : testArray) {
            al.add(i);
        }
        start();
        for (int i : testArray) {
            al.add(al.size() >> 1, i);
        }
        long time = getElapsedTime();
        System.out.println("ArrayList.add(al.size() / 2, e): " + time);
    }

    public static void testAddToMiddleDa() {
        int[] testArray = new int[130_000];
        Arrays.fill(testArray, 1);
        DynamicArray da = new DynamicArray();
        for (int i : testArray) {
            da.add(i);
        }
        start();
        for (int i : testArray) {
            da.add(da.size() >> 1, i);
        }
        long time = getElapsedTime();
        System.out.println("DynamicArray.add(al.size() / 2, e): " + time);
    }

    public static void testAddToEndAl() {
        int[] testArray = new int[10_000_000];
        Arrays.fill(testArray, 1);
        List<Object> al = new ArrayList<>();
        for (int i : testArray) {
            al.add(i);
        }
        start();
        for (int i = 10_000_000; i < 20_000_000; i++) {
            al.add(i, 1);
        }
        long time = getElapsedTime();
        System.out.println("ArrayList.add(i, e): " + time);
    }

    public static void testAddToEndDa() {
        int[] testArray = new int[10_000_000];
        Arrays.fill(testArray, 1);
        DynamicArray da = new DynamicArray();
        for (int i : testArray) {
            da.add(i);
        }
        start();
        for (int i = 10_000_000; i < 20_000_000; i++) {
            da.add(i, 1);
        }
        long time = getElapsedTime();
        System.out.println("DynamicArray.add(i, e): " + time);
    }

    public static void testRemoveAl() {
        String[] testArray = new String[300_000];
        Arrays.fill(testArray, " ");
        List<Object> al = new ArrayList<>();
        for (String i : testArray) {
            al.add(i);
        }
        start();
        for (String i : testArray) {
            al.remove(i);
        }
        long time = getElapsedTime();
        System.out.println("ArrayList.remove(e): " + time);
    }

    public static void testRemoveDa() {
        String[] testArray = new String[300_000];
        Arrays.fill(testArray, " ");
        DynamicArray da = new DynamicArray();
        for (String i : testArray) {
            da.add(i);
        }
        start();
        for (String i : testArray) {
            da.remove(i);
        }
        long time = getElapsedTime();
        System.out.println("DynamicArray.remove(e): " + time);
    }

    public static void testRemoveBeginAl() {
        int[] testArray = new int[300_000];
        Arrays.fill(testArray, 1);
        List<Object> al = new ArrayList<>();
        for (int i : testArray) {
            al.add(i);
        }
        start();
        for (int i : testArray) {
            al.remove(0);
        }
        long time = getElapsedTime();
        System.out.println("ArrayList.remove(0): " + time);
    }

    public static void testRemoveBeginDa() {
        int[] testArray = new int[300_000];
        Arrays.fill(testArray, 1);
        DynamicArray da = new DynamicArray();
        for (int i : testArray) {
            da.add(i);
        }
        start();
        for (int i : testArray) {
            da.remove(0);
        }
        long time = getElapsedTime();
        System.out.println("DynamicArray.remove(0): " + time);
    }

    public static void testRemoveMiddleAl() {
        int[] testArray = new int[300_000];
        Arrays.fill(testArray, 1);
        List<Object> al = new ArrayList<>();
        for (int i : testArray) {
            al.add(i);
        }
        start();
        for (int i = 150_000; i > 0; i--) {
            al.remove(al.size() >> 1);
        }
        long time = getElapsedTime();
        System.out.println("ArrayList.remove(al.size() / 2): " + time);
    }

    public static void testRemoveMiddleDa() {
        int[] testArray = new int[300_000];
        Arrays.fill(testArray, 1);
        DynamicArray da = new DynamicArray();
        for (int i : testArray) {
            da.add(i);
        }
        start();
        for (int i = 150_000; i > 0; i--) {
            da.remove(da.size() >> 1);
        }
        long time = getElapsedTime();
        System.out.println("DynamicArray.remove(da.size() / 2): " + time);
    }

    public static void testRemoveEndAl() {
        int[] testArray = new int[10_000_000];
        Arrays.fill(testArray, 1);
        List<Object> al = new ArrayList<>();
        for (int i : testArray) {
            al.add(i);
        }
        start();
        for (int i = 9_999_999; i >= 0; i--) {
            al.remove(i);
        }
        long time = getElapsedTime();
        System.out.println("ArrayList.remove(i): " + time);
    }

    public static void testRemoveEndDa() {
        int[] testArray = new int[10_000_000];
        Arrays.fill(testArray, 1);
        DynamicArray da = new DynamicArray();
        for (int i : testArray) {
            da.add(i);
        }
        start();
        for (int i = 9_999_999; i >= 0; i--) {
            da.remove(i);
        }
        long time = getElapsedTime();
        System.out.println("DynamicArray.remove(i): " + time);
    }
}