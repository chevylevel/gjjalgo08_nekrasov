package com.getjavajob.training.algo08.nekrasovs.lesson05;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Iterator;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;

public class DequeueTest {
    public static void main(String[] args) {
        testAddFirst();
        testAddLast();
        testOfferFirst();
        testOfferLast();
        testRemoveFirst();
        testRemoveLast();
        testPollFirst();
        testPollLast();
        testGetFirst();
        testGetLast();
        testPeekFirst();
        testPeekLast();
        testRemoveFirstOccurrence();
        testRemoveLastOccurrence();
        testAdd();
        testOffer();
        testRemove();
        testPoll();
        testElement();
        testPeek();
        testPush();
        testPop();
        testRemoveObj();
        testContains();
        testSize();
        testIterator();
        testDescendingIterator();
    }

    public static void testAddFirst() {
        Deque<String> llq = new ArrayDeque<>();
        llq.add("e");
        llq.add("s");
        llq.add("t");

        llq.addFirst("t");

        String testString = "";
        testString += llq.remove();
        testString += llq.remove();
        testString += llq.remove();
        testString += llq.remove();

        assertEquals("QueueTest.testAddFirst", "test", testString);
    }

    public static void testAddLast() {
        Deque<String> llq = new ArrayDeque<>();
        llq.add("t");
        llq.add("e");
        llq.add("s");

        llq.addLast("t");

        String testString = "";
        testString += llq.remove();
        testString += llq.remove();
        testString += llq.remove();
        testString += llq.remove();

        assertEquals("QueueTest.testAddLast", "test", testString);
    }

    public static void testOfferFirst() {
        Deque<String> llq = new ArrayDeque<>();
        llq.add("e");
        llq.add("s");
        llq.add("t");

        llq.offerFirst("t");

        String testString = "";
        testString += llq.remove();
        testString += llq.remove();
        testString += llq.remove();
        testString += llq.remove();

        assertEquals("QueueTest.offerFirst", "test", testString);
    }

    public static void testOfferLast() {
        Deque<String> llq = new ArrayDeque<>();
        llq.add("t");
        llq.add("e");
        llq.add("s");

        llq.offerLast("t");

        String testString = "";
        testString += llq.remove();
        testString += llq.remove();
        testString += llq.remove();
        testString += llq.remove();

        assertEquals("QueueTest.offerLast", "test", testString);
    }

    public static void testRemoveFirst() {
        Deque<String> llq = new ArrayDeque<>();
        llq.add("t");
        llq.add("e");
        llq.add("s");
        llq.add("t");

        llq.removeFirst();

        String testString = "";
        testString += llq.remove();
        testString += llq.remove();
        testString += llq.remove();

        assertEquals("QueueTest.testRemoveFirst", "est", testString);
    }

    public static void testRemoveLast() {
        Deque<String> llq = new ArrayDeque<>();
        llq.add("t");
        llq.add("e");
        llq.add("s");
        llq.add("t");

        llq.removeLast();

        String testString = "";
        testString += llq.remove();
        testString += llq.remove();
        testString += llq.remove();

        assertEquals("QueueTest.testRemoveLast", "tes", testString);
    }

    public static void testPollFirst() {
        Deque<String> llq = new ArrayDeque<>();
        llq.add("t");
        llq.add("e");
        llq.add("s");
        llq.add("t");

        llq.pollFirst();

        String testString = "";
        testString += llq.remove();
        testString += llq.remove();
        testString += llq.remove();

        assertEquals("QueueTest.testPollFirst", "est", testString);
    }

    public static void testPollLast() {
        Deque<String> llq = new ArrayDeque<>();
        llq.add("t");
        llq.add("e");
        llq.add("s");
        llq.add("t");

        llq.pollLast();

        String testString = "";
        testString += llq.remove();
        testString += llq.remove();
        testString += llq.remove();

        assertEquals("QueueTest.testPollLast", "tes", testString);
    }

    public static void testGetFirst() {
        Deque<String> llq = new ArrayDeque<>();
        llq.add("T");
        llq.add("e");
        llq.add("s");
        llq.add("t");

        String testString = "";
        testString += llq.getFirst();
        testString += llq.remove();

        assertEquals("QueueTest.testGetFirst", "TT", testString);
    }

    public static void testGetLast() {
        Deque<String> llq = new ArrayDeque<>();
        llq.add("T");
        llq.add("e");
        llq.add("s");
        llq.add("t");

        String testString = "";

        testString += llq.getLast();
        testString += llq.remove();
        testString += llq.remove();
        testString += llq.remove();
        testString += llq.remove();

        assertEquals("QueueTest.testGetLast", "tTest", testString);
    }

    public static void testPeekFirst() {
        Deque<String> llq = new ArrayDeque<>();
        llq.add("T");
        llq.add("e");
        llq.add("s");
        llq.add("t");

        String testString = "";
        testString += llq.peekFirst();
        testString += llq.remove();

        assertEquals("QueueTest.testPeekFirst", "TT", testString);
    }

    public static void testPeekLast() {
        Deque<String> llq = new ArrayDeque<>();
        llq.add("T");
        llq.add("e");
        llq.add("s");
        llq.add("t");

        String testString = "";

        testString += llq.peekLast();
        testString += llq.remove();
        testString += llq.remove();
        testString += llq.remove();
        testString += llq.remove();

        assertEquals("QueueTest.testPeekLast", "tTest", testString);
    }

    public static void testRemoveFirstOccurrence() {
        Deque<String> llq = new ArrayDeque<>();
        llq.add("t");
        llq.add("e");
        llq.add("s");
        llq.add("t");

        llq.removeFirstOccurrence("t");

        String testString = "";
        testString += llq.remove();
        testString += llq.remove();
        testString += llq.remove();

        assertEquals("QueueTest.testRemoveFirstOccurrence", "est", testString);
    }

    public static void testRemoveLastOccurrence() {
        Deque<String> llq = new ArrayDeque<>();
        llq.add("t");
        llq.add("e");
        llq.add("s");
        llq.add("t");

        llq.removeLastOccurrence("t");

        String testString = "";
        testString += llq.remove();
        testString += llq.remove();
        testString += llq.remove();

        assertEquals("QueueTest.testRemoveLastOccurrence", "tes", testString);
    }

    public static void testAdd() {
        Deque<String> llq = new ArrayDeque<>();
        llq.add("t");
        llq.add("e");
        llq.add("s");
        llq.add("t");

        String testString = "";
        testString += llq.remove();
        testString += llq.remove();
        testString += llq.remove();
        testString += llq.remove();

        assertEquals("QueueTest.testAdd", "test", testString);
    }

    public static void testOffer() {
        Deque<String> llq = new ArrayDeque<>();
        llq.add("t");
        llq.add("e");
        llq.add("s");
        llq.offer("t");

        String testString = "";
        testString += llq.remove();
        testString += llq.remove();
        testString += llq.remove();
        testString += llq.remove();

        assertEquals("QueueTest.TestOffer", "test", testString);
    }

    public static void testRemove() {
        Deque<String> llq = new ArrayDeque<>();
        llq.add("t");
        llq.add("e");
        llq.add("s");
        llq.add("t");

        llq.remove();

        String testString = "";
        testString += llq.remove();
        testString += llq.remove();
        testString += llq.remove();

        assertEquals("QueueTest.testRemove", "est", testString);
    }

    public static void testPoll() {
        Deque<String> llq = new ArrayDeque<>();
        llq.add("t");
        llq.add("e");
        llq.add("s");
        llq.add("t");

        llq.poll();

        String testString = "";
        testString += llq.remove();
        testString += llq.remove();
        testString += llq.remove();

        assertEquals("QueueTest.testPoll", "est", testString);
    }

    public static void testElement() {
        Deque<String> llq = new ArrayDeque<>();
        llq.add("T");
        llq.add("e");
        llq.add("s");
        llq.add("t");

        String testString = "";
        testString += llq.element();
        testString += llq.remove();

        assertEquals("QueueTest.testElement", "TT", testString);
    }

    public static void testPeek() {
        Deque<String> llq = new ArrayDeque<>();
        llq.add("T");
        llq.add("e");
        llq.add("s");
        llq.add("t");

        String testString = "";
        testString += llq.peek();
        testString += llq.remove();

        assertEquals("QueueTest.testPeek", "TT", testString);
    }

    public static void testPush() {
        Deque<String> llq = new ArrayDeque<>();
        llq.add("e");
        llq.add("s");
        llq.add("t");

        llq.push("t");

        String testString = "";
        testString += llq.remove();
        testString += llq.remove();
        testString += llq.remove();
        testString += llq.remove();

        assertEquals("QueueTest.testAddFirst", "test", testString);
    }

    public static void testPop() {
        Deque<String> llq = new ArrayDeque<>();
        llq.add("t");
        llq.add("e");
        llq.add("s");
        llq.add("t");

        llq.pop();

        String testString = "";
        testString += llq.remove();
        testString += llq.remove();
        testString += llq.remove();

        assertEquals("QueueTest.testPop", "est", testString);
    }

    public static void testRemoveObj() {
        Deque<String> llq = new ArrayDeque<>();
        llq.add("t");
        llq.add("e");
        llq.add("s");
        llq.add("t");

        llq.remove("t");

        String testString = "";
        testString += llq.remove();
        testString += llq.remove();
        testString += llq.remove();

        assertEquals("QueueTest.testRemoveObj", "est", testString);
    }

    public static void testContains() {
        Deque<String> llq = new ArrayDeque<>();
        llq.add("t");
        llq.add("e");
        llq.add("s");
        llq.add("t");
        assertEquals("QueueTest.testContains", true, llq.contains("e"));

    }

    public static void testSize() {
        Deque<String> llq = new ArrayDeque<>();
        llq.add("t");
        llq.add("e");
        llq.add("s");
        llq.add("t");
        assertEquals("QueueTest.testSize", 4, llq.size());
    }

    public static void testIterator() {
        Deque<String> llqToIterate = new ArrayDeque<>();
        llqToIterate.add("t");
        llqToIterate.add("e");
        llqToIterate.add("s");
        llqToIterate.add("t");
        Iterator itr = llqToIterate.iterator();
        StringBuilder sb = new StringBuilder();
        while (itr.hasNext()) {
            sb.append(itr.next());
        }
        assertEquals("QueueTest.testIterator", "test", sb.toString());
    }

    public static void testDescendingIterator() {
        Deque<String> llqToIterate = new ArrayDeque<>();
        llqToIterate.add("t");
        llqToIterate.add("e");
        llqToIterate.add("s");
        llqToIterate.add("t");
        Iterator itr = llqToIterate.descendingIterator();
        StringBuilder sb = new StringBuilder();
        while (itr.hasNext()) {
            sb.append(itr.next());
        }
        assertEquals("QueueTest.testDescendingIterator", "tset", sb.toString());
    }
}