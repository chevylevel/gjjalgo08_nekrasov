package com.getjavajob.training.algo08.nekrasovs.lesson06;

import static java.lang.Math.abs;

public class AssociativeArray<K, V> {
    private static final int DEFAULT_CAPACITY = 16;
    private static final int MAX_CAPACITY = 1 << 30;
    private static final float DEFAULT_LOAD_FACTOR = 0.75f;
    private MyEntry<K, V>[] table;
    private int capacity;
    private int bucketSize;

    public AssociativeArray(int capacity) {
        if (capacity < 0) {
            throw new IllegalArgumentException();
        } else if (capacity > MAX_CAPACITY) {
            this.capacity = MAX_CAPACITY;
        } else {
            this.capacity = capacity;
        }

        table = new MyEntry[capacity];
    }

    public AssociativeArray() {
        this(DEFAULT_CAPACITY);
    }

    private void increaseTable() {
        MyEntry<K, V>[] oldTable = table;
        int oldCapacity = oldTable.length;
        capacity <<= 1;
        table = new MyEntry[capacity];
        rehash(oldTable, oldCapacity);
    }

    private int getIndex(K key) {
        return (key == null) ? 0 : abs(key.hashCode() % capacity);
    }

    private void rehash(MyEntry<K, V>[] oldTable, int oldCapacity) {
        for (int i = 1; i < oldCapacity; i++) {
            if (oldTable[i] != null) {
                int newIndex = getIndex(oldTable[i].key);
                table[newIndex] = oldTable[i];
            }
        }
        table[0] = oldTable[0];
    }

    private MyEntry<K, V> getEntry(K key) {
        if (bucketSize == 0) {
            return null;
        }
        int i = getIndex(key);
        if (table[i] != null) {
            for (MyEntry<K, V> e = table[i]; e != null; e = e.next) {
                if (key != null && key.equals(e.key) ||
                        key == null && e.key == null) {
                    return e;
                }
            }
        }
        return null;
    }

    public V add(K key, V val) {
        if (bucketSize >= capacity * DEFAULT_LOAD_FACTOR) {
            increaseTable();
        }
        if (key == null) {
            return addForNullKey(val);
        }
        MyEntry<K, V> e = getEntry(key);
        if (e != null) {
            V oldValue = e.value;
            e.value = val;
            return oldValue;
        } else {
            if (table[getIndex(key)] != null) {
                e = table[getIndex(key)];
                while (e.next != null) {
                    e = e.next;
                }
                e.next = new MyEntry<>(key, val);
            } else {
                table[getIndex(key)] = new MyEntry<>(key, val);
                bucketSize++;
            }
        }
        return null;
    }

    private V addForNullKey(V val) {
        if (table[0] == null) {
            table[0] = new MyEntry<>(null, val);
            bucketSize++;
        } else {
            for (MyEntry<K, V> e = table[0]; e != null; e = e.next) {
                if (e.key == null) {
                    V oldValue = e.value;
                    e.value = val;
                    return oldValue;
                }
            }
        }
        return null;
    }

    public V get(K key) {
        MyEntry<K, V> entry = getEntry(key);
        return entry == null ? null : entry.value;
    }

    public V remove(K key) {
        if (bucketSize == 0) {
            return null;
        }
        int i = (key == null) ? 0 : getIndex(key);
        MyEntry<K, V> e = table[i];
        MyEntry<K, V> prev = e;
        while (e != null) {
            MyEntry<K, V> next = e.next;
            if (key == null && e.key == null ||
                    key != null && key.equals(e.key)) {
                if (prev == e) {
                    table[i] = next;
                } else {
                    prev.next = next;
                }
                return e.value;
            }
            prev = e;
            e = next;
        }
        return null;
    }

    private static class MyEntry<K, V> {
        private final K key;
        private V value;
        private MyEntry<K, V> next;

        MyEntry(K k, V v) {
            key = k;
            value = v;
        }
    }
}