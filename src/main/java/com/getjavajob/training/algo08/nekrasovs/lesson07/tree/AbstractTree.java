package com.getjavajob.training.algo08.nekrasovs.lesson07.tree;

import java.util.*;

/**
 * An abstract base class providing some functionality of the Tree interface
 *
 * @param <E> element
 */
public abstract class AbstractTree<E> implements Tree<E> {
    @Override
    public boolean isInternal(Node<E> n) throws IllegalArgumentException {
        if (n != null) {
            return childrenNumber(n) != 0;
        }
        throw new IllegalArgumentException();
    }

    @Override
    public boolean isExternal(Node<E> n) throws IllegalArgumentException {
        if (n != null) {
            return childrenNumber(n) == 0;
        }
        throw new IllegalArgumentException();
    }

    @Override
    public boolean isRoot(Node<E> n) throws IllegalArgumentException {
        if (n != null) {
            return parent(n) == null;
        }
        throw new IllegalArgumentException();
    }

    @Override
    public boolean isEmpty() {
        return root() == null;
    }

    @Override
    public Iterator<E> iterator() {
        return new ElementIterator();
    }

    /**
     * @return an iterable collection of nodes of the tree in preorder
     */

    // Auxiliary methods for traversal
    private Collection<Node<E>> pre(Node<E> n) throws IllegalArgumentException {
        List<Node<E>> nodes = new ArrayList<>();
        Queue<Node<E>> buf = new ArrayDeque<>();
        if (n == null) {
            throw new IllegalArgumentException();
        }
        nodes.add(n);
        if (childrenNumber(n) != 0) {
            buf.addAll(children(n));
        } else {
            return nodes;
        }
        while (!buf.isEmpty()) {
            Node<E> node = buf.poll();
            nodes.addAll(pre(node));
        }
        return nodes;
    }

    private Collection<Node<E>> post(Node<E> n) throws IllegalArgumentException {
        List<Node<E>> nodes = new ArrayList<>();
        Queue<Node<E>> buf = new ArrayDeque<>();
        if (n == null) {
            throw new IllegalArgumentException();
        }
        if (childrenNumber(n) != 0) {
            buf.addAll(children(n));
        }
        while (!buf.isEmpty()) {
            Node<E> node = buf.poll();
            nodes.addAll(post(node));
        }
        nodes.add(n);
        return nodes;
    }

    private Collection<Node<E>> breadth(Node<E> n) throws IllegalArgumentException {
        Queue<Node<E>> level = new ArrayDeque<>();
        Queue<Node<E>> des = new ArrayDeque<>();
        List<Node<E>> nodes = new ArrayList<>();
        if (n == null) {
            throw new IllegalArgumentException();
        }
        nodes.add(n);
        if (childrenNumber(n) != 0) {
            level.addAll(children(n));
        } else {
            return nodes;
        }
        while (!level.isEmpty()) {
            Node<E> node = level.poll();
            nodes.add(node);
            des.addAll(children(node));
        }
        if (des.isEmpty()) {
            return nodes;
        } else {
            while (!des.isEmpty()) {
                nodes.addAll(breadth(des.poll()));
            }
        }
        return nodes;
    }

    public Collection<Node<E>> preOrder() throws IllegalArgumentException {
        return pre(root());
    }

    /**
     * @return an iterable collection of nodes of the tree in postorder
     */
    public Collection<Node<E>> postOrder() throws IllegalArgumentException {
        return post(root());
    }

    /**
     * @return an iterable collection of nodes of the tree in breadth-first order
     */
    public Collection<Node<E>> breadthFirst() throws IllegalArgumentException {
        return breadth(root());
    }

    /**
     * Adapts the iteration produced by {@link #nodes()}
     */
    private class ElementIterator implements Iterator<E> {
        private Node<E> lastReturned;
        private Iterator<Node<E>> it = nodes().iterator();

        @Override
        public boolean hasNext() {
            return it.hasNext();
        }

        @Override
        public E next() {
            lastReturned = it.next();
            return lastReturned.getElement();
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
}