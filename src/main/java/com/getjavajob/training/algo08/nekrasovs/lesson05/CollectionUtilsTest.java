package com.getjavajob.training.algo08.nekrasovs.lesson05;

import java.util.*;

import static com.getjavajob.training.algo08.nekrasovs.lesson05.CollectionUtils.*;
import static com.getjavajob.training.algo08.util.Assert.assertEquals;

public class CollectionUtilsTest {

    private static final Employee[] utilArray = {new Employee("Hank", 1000),
            new Employee("Bill", 600),
            new Employee("Boomhauer", 700),
            new Employee("Dale", 300)};

    private static class Employee {
        String name;
        int salary;

        Employee(String name, int salary) {
            this.name = name;
            this.salary = salary;
        }
    }

    public static void main(String[] args) {
        testFilter();
        testTransformer();
        testTransformer2();
        testForAllDo();
        testUnmodifiableCollection();
    }

    private static MyPredicate<Employee> salaryMoreThan() {
        return new MyPredicate<Employee>() {
            @Override
            public boolean test(Employee e) {
                return e.salary >= 600;
            }
        };
    }

    public static void testFilter() {
        List<Employee> listForTest = new ArrayList<>(Arrays.asList(utilArray));
        assertEquals("CollectionUtilsTest.testFilter", true, filter(listForTest, salaryMoreThan()));
    }

    private static MyTransformer<Employee, String> getField() {
        return new MyTransformer<Employee, String>() {
            @Override
            public String transformElement(Employee e) {
                return e.name;
            }

            @SuppressWarnings("unchecked")
            @Override
            public Collection transformCollection(Collection employees) {
                Iterator<Employee> empItr = employees.iterator();
                Deque<Employee> empDeq = new ArrayDeque<>();
                while (empItr.hasNext()) {
                    empDeq.add(empItr.next());
                    empItr.remove();
                }
                while (!empDeq.isEmpty()) {
                    employees.add(empDeq.remove().salary);
                }
                return employees;
            }
        };
    }

    public static void testTransformer() {
        String[] expected = {"Hank", "Bill", "Boomhauer", "Dale"};
        List<Employee> listForTest = new ArrayList<>(Arrays.asList(utilArray));
        assertEquals("CollectionUtilsTest.testTransformer", expected, transform(listForTest, getField()).toArray());
    }

    public static void testTransformer2() {
        Integer[] expected = {1000, 600, 700, 300};
        List<Employee> listForTest = new ArrayList<>(Arrays.asList(utilArray));
        assertEquals("CollectionUtilsTest.testTransformer2", expected, transform2(listForTest, getField()).toArray());
    }

    public static MyClosure<Employee> increaseSalary() {
        return new MyClosure<Employee>() {
            @Override
            public Employee apply(Employee e) {
                e.salary += 100;
                return e;
            }
        };
    }

    public static void testForAllDo() {
        Integer[] salary = {1100, 700, 800, 400};
        List<Employee> listForTest = new ArrayList<>(Arrays.asList(utilArray));
        assertEquals("CollectionUtilsTest.testForAllDo", salary,
                transform2(forAllDo(listForTest, increaseSalary()), getField()).toArray());
    }

    public static void testUnmodifiableCollection() {
        List<Employee> listForTest = new ArrayList<>(Arrays.asList(utilArray));
        Collection<Employee> fixedList = unmodifiableCollection(listForTest);

        assertEquals("CollectionUtilsTest.testUnmodifiableCollection.size", 4, fixedList.size());
        assertEquals("CollectionUtilsTest.testUnmodifiableCollection.isEmpty", false, fixedList.isEmpty());
        assertEquals("CollectionUtilsTest.testUnmodifiableCollection.contains", true, fixedList.contains(listForTest.get(0)));
        try {
            fixedList.add(new Employee("Jhon Redcorn", 500));
        } catch (UnsupportedOperationException e) {
            System.out.println("CollectionUtilsTest.testUnmodifiableCollection.add passed");
        }
        try {
            fixedList.remove(listForTest.get(0));
        } catch (UnsupportedOperationException e) {
            System.out.println("CollectionUtilsTest.testUnmodifiableCollection.remove passed");
        }
        try {
            fixedList.addAll(listForTest);
        } catch (UnsupportedOperationException e) {
            System.out.println("CollectionUtilsTest.testUnmodifiableCollection.addAll passed");
        }
        try {
            fixedList.clear();
        } catch (UnsupportedOperationException e) {
            System.out.println("CollectionUtilsTest.testUnmodifiableCollection.clear passed");
        }
        try {
            fixedList.retainAll(listForTest);
        } catch (UnsupportedOperationException e) {
            System.out.println("CollectionUtilsTest.testUnmodifiableCollection.retainAll passed");
        }
        try {
            fixedList.removeAll(listForTest);
        } catch (UnsupportedOperationException e) {
            System.out.println("CollectionUtilsTest.testUnmodifiableCollection.removeAll passed");
        }
        assertEquals("CollectionUtilsTest.testUnmodifiableCollection.containsAll", true, fixedList.containsAll(listForTest));
        assertEquals("CollectionUtilsTest.testUnmodifiableCollection.toArray", utilArray, fixedList.toArray());
        assertEquals("CollectionUtilsTest.testUnmodifiableCollection.toArray", utilArray, fixedList.toArray(new Employee[4]));

        Iterator<Employee> itr = fixedList.iterator();

        assertEquals("CollectionUtilsTest.testUnmodifiableCollection.iterator.hasNext", true, itr.hasNext());
        assertEquals("CollectionUtilsTest.testUnmodifiableCollection.iterator.hasNext", listForTest.get(0), itr.next());
        try {
            itr.remove();
        } catch (UnsupportedOperationException e) {
            System.out.println("CollectionUtilsTest.testUnmodifiableCollection.iterator.remove passed");
        }
    }
}