package com.getjavajob.training.algo08.nekrasovs.lesson06;

import java.util.*;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;

public class LinkedHashSetTest {
    public static void main(String[] args) {
        testAdd();
    }

    public static void testAdd() {
        Set<String> s = new LinkedHashSet<>();
        s.add("first");
        s.add("second");
        s.add("third");
        List<String> l = new ArrayList<>();
        Iterator<String> it = s.iterator();
        while (it.hasNext()) {
            l.add(it.next());
        }
        assertEquals("LinkedHashSetTest.testAdd", "first", l.get(0));
        assertEquals("LinkedHashSetTest.testAdd", "second", l.get(1));
        assertEquals("LinkedHashSetTest.testAdd", "third", l.get(2));
        assertEquals("LinkedHashSetTest.testAdd", false, s.add("third"));
    }
}