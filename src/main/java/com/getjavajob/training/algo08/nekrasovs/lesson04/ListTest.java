package com.getjavajob.training.algo08.nekrasovs.lesson04;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;

public class ListTest {
    public static void main(String[] args) {
        testAddAll();
        testGet();
        testSet();
        testRemove();
        testIndexOf();
        testLastIndexOf();
        testListIterator();
        testListIteratorInd();
        testSublist();
    }

    public static void testAddAll() {
        List<String> testCol = new ArrayList<>();
        testCol.add("t");
        testCol.add("t");
        List<String> colToAdd = new ArrayList<>();
        colToAdd.add("e");
        colToAdd.add("s");

        testCol.addAll(1, colToAdd);

        List<String> colToCompare = new ArrayList<>();
        colToCompare.add("t");
        colToCompare.add("e");
        colToCompare.add("s");
        colToCompare.add("t");
        assertEquals("SinglyLinkedListTest.testAddAll", colToCompare, testCol);
    }

    public static void testGet() {
        List<String> testCol = new ArrayList<>();
        testCol.add("t");
        testCol.add("e");
        testCol.add("s");
        testCol.add("t");
        assertEquals("SinglyLinkedListTest.testGet", "e", testCol.get(1));
    }

    public static void testSet() {
        List<String> testCol = new ArrayList<>();
        testCol.add("t");
        testCol.add("s");
        testCol.add("t");

        testCol.set(1, "e");

        assertEquals("SinglyLinkedListTest.testSet", "e", testCol.get(1));
    }

    public static void testRemove() {
        List<String> testCol = new ArrayList<>();
        testCol.add("t");
        testCol.add("e");
        testCol.add("s");
        testCol.add("t");

        testCol.remove(3);

        List<String> colToCompare = new ArrayList<>();
        colToCompare.add("t");
        colToCompare.add("e");
        colToCompare.add("s");
        assertEquals("SinglyLinkedListTest.testRemove", colToCompare, testCol);
    }

    public static void testIndexOf() {
        List<String> testCol = new ArrayList<>();
        testCol.add("t");
        testCol.add("e");
        testCol.add("s");
        testCol.add("t");
        assertEquals("SinglyLinkedListTest.IndexOf", 0, testCol.indexOf("t"));
    }

    public static void testLastIndexOf() {
        List<String> testCol = new ArrayList<>();
        testCol.add("t");
        testCol.add("e");
        testCol.add("s");
        testCol.add("t");
        assertEquals("SinglyLinkedListTest.LastIndexOf", 3, testCol.lastIndexOf("t"));
    }

    public static void testListIterator() {
        List<String> testCol = new ArrayList<>();
        testCol.add("t");
        testCol.add("e");
        testCol.add("s");
        testCol.add("t");
        List<String> testColItr = new ArrayList<>();
        ListIterator<String> listItr = testCol.listIterator();
        while (listItr.hasNext()) {
            testColItr.add(listItr.next());
        }
        assertEquals("SinglyLinkedListTest.testListIterator", testColItr, testCol);
    }

    public static void testListIteratorInd() {
        List<String> testCol = new ArrayList<>();
        testCol.add("t");
        testCol.add("e");
        testCol.add("s");
        testCol.add("t");
        List<String> testColToCompare = new ArrayList<>();
        testColToCompare.add("e");
        testColToCompare.add("s");
        testColToCompare.add("t");
        List<String> testColItr = new ArrayList<>();
        ListIterator<String> listItr = testCol.listIterator(1);
        while (listItr.hasNext()) {
            testColItr.add(listItr.next());
        }
        assertEquals("SinglyLinkedListTest.testListIteratorInd", testColItr, testColToCompare);
    }

    public static void testSublist() {
        List<String> testCol = new ArrayList<>();
        testCol.add("t");
        testCol.add("e");
        testCol.add("s");
        testCol.add("t");
        List<String> testColToCompare = new ArrayList<>();
        testColToCompare.add("e");
        testColToCompare.add("s");

        assertEquals("SinglyLinkedListTest.testSublist", testColToCompare, testCol.subList(1, 3));
    }
}