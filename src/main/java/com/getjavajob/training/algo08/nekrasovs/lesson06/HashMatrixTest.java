package com.getjavajob.training.algo08.nekrasovs.lesson06;

import java.util.Arrays;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;

public class HashMatrixTest {
    private static final int COL = 999_999;

    public static void main(String[] args) {
        testGet();
        testEquals();
        testEqualsKey();
        testHashcode();
        testHashcodeKey();
    }

    private static HashMatrix<String> getHm(String value) {
        HashMatrix<String> hm = new HashMatrix<>();

        for (int i = 0; i < 999_999; i++) {
            hm.set(i, COL, value);
        }
        return hm;
    }

    public static void testGet() {
        HashMatrix<String> hm = getHm("value");
        String[] strings = new String[COL];
        for (int i = 0; i < COL; i++) {
            strings[i] = hm.get(i, COL);
        }
        String[] stringsToCompare = new String[COL];
        Arrays.fill(stringsToCompare, "value");

        assertEquals("HashMatrixTest.testGet", stringsToCompare, strings);
    }

    public static void testEquals() {
        HashMatrix<String> hmX = getHm("value");
        assertEquals("HashMatrixTest.testEquals", true, hmX.equals(hmX));
        HashMatrix<String> hmY = getHm("value");
        assertEquals("HashMatrixTest.testEquals", true, hmX.equals(hmY));
        assertEquals("HashMatrixTest.testEquals", true, hmY.equals(hmX));
        HashMatrix<String> hmZ = getHm("value");
        assertEquals("HashMatrixTest.testEquals", true, hmY.equals(hmZ));
        assertEquals("HashMatrixTest.testEquals", true, hmX.equals(hmZ));
        hmX.set(0, 1, "value2");
        assertEquals("HashMatrixTest.testEquals", false, hmX.equals(hmY));
    }

    public static void testEqualsKey() {
        HashMatrix.Key x = new HashMatrix.Key(0, 0);
        assertEquals("HashMatrixTest.testEqualsKey", true, x.equals(x));
        HashMatrix.Key y = new HashMatrix.Key(0, 0);
        assertEquals("HashMatrixTest.testEqualsKey", true, x.equals(y));
        assertEquals("HashMatrixTest.testEqualsKey", true, y.equals(x));
        HashMatrix.Key z = new HashMatrix.Key(0, 0);
        assertEquals("HashMatrixTest.testEqualsKey", true, y.equals(z));
        assertEquals("HashMatrixTest.testEqualsKey", true, x.equals(z));
        HashMatrix.Key k = new HashMatrix.Key(1, 0);
        assertEquals("HashMatrixTest.testEqualsKey", false, k.equals(x));
    }

    public static void testHashcode() {
        HashMatrix<String> hmX = getHm("value");
        HashMatrix<String> hmY = getHm("value");
        assertEquals("HashMatrixTest.testHashcode", hmX.hashCode(), hmY.hashCode());
    }

    public static void testHashcodeKey() {
        HashMatrix.Key x = new HashMatrix.Key(0, 0);
        HashMatrix.Key y = new HashMatrix.Key(0, 0);
        assertEquals("HashMatrixTest.testHashcodeKey", x.hashCode(), y.hashCode());
    }
}