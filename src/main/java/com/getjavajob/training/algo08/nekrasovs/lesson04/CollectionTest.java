package com.getjavajob.training.algo08.nekrasovs.lesson04;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;

public class CollectionTest {
    public static void main(String[] args) {
        testSize();
        testIsEmptyFalse();
        testIsEmptyTrue();
        testContains();
        testIterator();
        testToArray();
        testToArrayT();
        testAdd();
        testRemoveO();
        testContainsAll();
        testAddAll();
        testRemoveAll();
        testRetainAll();
        testClear();
        testEquals();
        testHashCode();
    }

    public static void testSize() {
        Collection<String> testCol = new ArrayList<>();
        testCol.add("t");
        testCol.add("e");
        testCol.add("s");
        testCol.add("t");
        assertEquals("CollectionsTest.testSize", 4, testCol.size());
    }

    public static void testIsEmptyFalse() {
        Collection<String> testCol = new ArrayList<>();
        testCol.add("t");
        testCol.add("e");
        testCol.add("s");
        testCol.add("t");
        assertEquals("CollectionsTest.testIsEmptyFalse", false, testCol.isEmpty());
    }

    public static void testIsEmptyTrue() {
        Collection<String> testCol = new ArrayList<>();
        assertEquals("CollectionsTest.testIsEmptyTrue", true, testCol.isEmpty());
    }

    public static void testContains() {
        Collection<String> testCol = new ArrayList<>();
        testCol.add("t");
        testCol.add("e");
        testCol.add("s");
        testCol.add("t");
        assertEquals("CollectionsTest.testContains", true, testCol.contains("t"));
    }

    public static void testIterator() {
        Collection<String> testCol = new ArrayList<>();
        testCol.add("t");
        testCol.add("e");
        testCol.add("s");
        testCol.add("t");
        Iterator<String> itr = testCol.iterator();
        Collection<String> iteratedTestCol = new ArrayList<>();
        while (itr.hasNext()) {
            iteratedTestCol.add(itr.next());
        }
        assertEquals("CollectionsTest.testIterator", testCol, iteratedTestCol);
    }

    public static void testToArray() {
        String[] testArray = {"t", "e", "s", "t"};
        Collection<String> testCol = new ArrayList<>();
        testCol.add("t");
        testCol.add("e");
        testCol.add("s");
        testCol.add("t");
        assertEquals("SinglyLinkedListTest.testToArray", testArray, testCol.toArray());
    }

    public static void testToArrayT() {
        String[] testArray = {"t", "e", "s", "t"};
        String[] emptyArray = {};
        Collection<String> testCol = new ArrayList<>();
        testCol.add("t");
        testCol.add("e");
        testCol.add("s");
        testCol.add("t");
        assertEquals("SinglyLinkedListTest.testToArrayT", testArray, testCol.toArray(emptyArray));
    }

    public static void testAdd() {
        Collection<String> testCol = new ArrayList<>();
        testCol.add("t");
        testCol.add("e");
        testCol.add("s");
        testCol.add("t");
        String[] testArray = {"t", "e", "s", "t"};
        assertEquals("SinglyLinkedListTest.testAdd", testArray, testCol.toArray());
    }

    public static void testRemoveO() {
        Collection<String> testCol = new ArrayList<>();
        testCol.add("t");
        testCol.add("e");
        testCol.add("s");
        testCol.add("t");
        testCol.remove("e");
        String[] testArray = {"t", "s", "t"};
        assertEquals("SinglyLinkedListTest.testRemoveO", testArray, testCol.toArray());
    }

    public static void testContainsAll() {
        Collection<String> testCol = new ArrayList<>();
        testCol.add("t");
        testCol.add("e");
        testCol.add("s");
        testCol.add("t");
        Collection<String> colToCompare = new ArrayList<>();
        colToCompare.add("t");
        colToCompare.add("e");
        colToCompare.add("s");
        assertEquals("SinglyLinkedListTest.testContainsAll", true, testCol.containsAll(colToCompare));
    }

    public static void testAddAll() {
        Collection<String> testCol = new ArrayList<>();
        testCol.add("t");
        testCol.add("e");
        Collection<String> colToAdd = new ArrayList<>();
        colToAdd.add("s");
        colToAdd.add("t");

        testCol.addAll(colToAdd);

        Collection<String> colToCompare = new ArrayList<>();
        colToCompare.add("t");
        colToCompare.add("e");
        colToCompare.add("s");
        colToCompare.add("t");
        assertEquals("SinglyLinkedListTest.testAddAll", colToCompare, testCol);
    }

    public static void testRemoveAll() {
        Collection<String> testCol = new ArrayList<>();
        testCol.add("t");
        testCol.add("e");
        testCol.add("s");
        testCol.add("t");
        Collection<String> colToRemove = new ArrayList<>();
        colToRemove.add("t");
        colToRemove.add("s");

        testCol.removeAll(colToRemove);

        Collection<String> colToCompare = new ArrayList<>();
        colToCompare.add("e");
        assertEquals("SinglyLinkedListTest.testRemoveAll", colToCompare, testCol);
    }

    public static void testRetainAll() {
        Collection<String> testCol = new ArrayList<>();
        testCol.add("t");
        testCol.add("e");
        testCol.add("s");
        testCol.add("t");
        Collection<String> colToRetain = new ArrayList<>();
        colToRetain.add("t");

        testCol.retainAll(colToRetain);

        Collection<String> colToCompare = new ArrayList<>();
        colToCompare.add("t");
        colToCompare.add("t");
        assertEquals("SinglyLinkedListTest.testRetainAll", colToCompare, testCol);
    }

    public static void testClear() {
        Collection<String> testCol = new ArrayList<>();
        testCol.add("t");
        testCol.add("e");
        testCol.add("s");
        testCol.add("t");
        testCol.clear();
        Collection<String> colToCompare = new ArrayList<>();
        assertEquals("SinglyLinkedListTest.testClear", colToCompare, testCol);
    }

    public static void testEquals() {
        Collection<String> testCol1 = new ArrayList<>();
        testCol1.add("t");
        testCol1.add("e");
        testCol1.add("s");
        testCol1.add("t");
        Collection<String> testCol2 = new ArrayList<>();
        testCol2.add("t");
        testCol2.add("e");
        testCol2.add("s");
        testCol2.add("t");
        assertEquals("SinglyLinkedListTest.testEquals", true, testCol1.equals(testCol2));
    }

    public static void testHashCode() {
        Collection<String> testCol1 = new ArrayList<>();
        testCol1.add("t");
        testCol1.add("e");
        testCol1.add("s");
        testCol1.add("t");
        Collection<String> testCol2 = new ArrayList<>();
        testCol2.add("t");
        testCol2.add("e");
        testCol2.add("s");
        testCol2.add("t");
        assertEquals("SinglyLinkedListTest.testHashCode", true, testCol1.hashCode() == testCol2.hashCode());
    }
}