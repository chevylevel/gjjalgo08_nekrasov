package com.getjavajob.training.algo08.nekrasovs.lesson05;

public class LinkedListQueue<E> extends AbstractClass<E> {
    private SinglyLinkedList<E> sll = new SinglyLinkedList<>();

    public static void main(String[] args) {
        LinkedListQueue<String> llq = new LinkedListQueue<>();
        llq.add("t");
        llq.add("e");
        llq.add("s");
        llq.add("t");

        System.out.println(llq.remove());
        System.out.println(llq.remove());
        System.out.println(llq.remove());
        System.out.println(llq.remove());
    }

    public boolean add(E e) {
        sll.add(e);
        return true;
    }

    public E remove() {
        SinglyLinkedList.Node<E> node = sll.head;
        sll.head = sll.head.next;
        return node.val;
    }
}