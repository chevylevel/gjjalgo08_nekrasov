package com.getjavajob.training.algo08.nekrasovs.lesson06;

import java.util.*;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;

public class MapTest {
    public static void main(String[] args) {
        testPut();
        testSize();
        testIsEmpty();
        testContainsKey();
        testContainsValue();
        testGet();
        testRemove();
        testPutAll();
        testClear();
        testKeySet();
        testValues();
        testEntrySet();
    }

    public static void testPut() {
        Map<String, String> map = new HashMap<>();
        assertEquals("MapTest.testPut", null, map.put("key", "val"));
    }

    public static void testSize() {
        Map<String, String> map = new HashMap<>();
        map.put("key", "val");
        assertEquals("MapTest.testSize", 1, map.size());
    }

    public static void testIsEmpty() {
        Map<String, String> map = new HashMap<>();
        assertEquals("MapTest.testIsEmpty", true, map.isEmpty());
        map.put("key", "val");
        assertEquals("MapTest.testIsEmpty", false, map.isEmpty());
    }

    public static void testContainsKey() {
        Map<String, String> map = new HashMap<>();
        map.put("key", "val");
        assertEquals("MapTest.testContainsKey", false, map.containsKey("KEY"));
        assertEquals("MapTest.testContainsKey", true, map.containsKey("key"));
    }

    public static void testContainsValue() {
        Map<String, String> map = new HashMap<>();
        map.put("key", "val");
        assertEquals("MapTest.testContainsValue", false, map.containsValue("VAL"));
        assertEquals("MapTest.testContainsValue", true, map.containsValue("val"));
    }

    public static void testGet() {
        Map<String, String> map = new HashMap<>();
        map.put("key", "val");
        assertEquals("MapTest.testGet", "val", map.get("key"));
    }

    public static void testRemove() {
        Map<String, String> map = new HashMap<>();
        map.put("key", "val");
        assertEquals("MapTest.testRemove", "val", map.remove("key"));
        assertEquals("MapTest.testRemove", null, map.get("key"));
    }

    public static void testPutAll() {
        Map<String, String> map2 = new HashMap<>();
        map2.put("key", "val");
        Map<String, String> map = new HashMap<>();
        map.putAll(map2);
        assertEquals("MapTest.testPutAll", "val", map.get("key"));
    }


    public static void testClear() {
        Map<String, String> map = new HashMap<>();
        map.put("key", "val");
        map.clear();
        assertEquals("MapTest.testClear", null, map.get("key"));
    }

    public static void testKeySet() {
        Map<String, String> map = new HashMap<>();
        map.put("key", "val");
        Set<String> set = map.keySet();
        assertEquals("MapTest.testKeySet", true, set.contains("key"));
    }

    public static void testValues() {
        Map<String, String> map = new HashMap<>();
        map.put("key", "val");
        Collection<String> values = map.values();
        assertEquals("MapTest.testValues", true, values.contains("val"));
    }

    public static void testEntrySet() {
        Map<String, String> map = new HashMap<>();
        map.put("key", "val");
        Set<Map.Entry<String, String>> set = map.entrySet();
        Iterator<Map.Entry<String, String>> itr = set.iterator();
        Map.Entry<String, String> entry = itr.next();
        assertEquals("MapTest.testEntrySet", "key", entry.getKey());
        assertEquals("MapTest.testEntrySet", "val", entry.getValue());
    }
}