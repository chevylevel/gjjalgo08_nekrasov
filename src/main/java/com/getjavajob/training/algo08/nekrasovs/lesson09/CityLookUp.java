package com.getjavajob.training.algo08.nekrasovs.lesson09;

import java.util.Collection;
import java.util.Iterator;
import java.util.NavigableMap;
import java.util.TreeMap;

public class CityLookUp {
    private static NavigableMap<String, String> cities = new TreeMap<>();

    /**
     * Search Entries in Storage by request.
     * Returns empty string if request is empty string.
     *
     * @param request String search request.
     * @return String elements of Collection which is found entries.
     */
    public static String searchCity(String request) {
        if (request.equals("")) {
            return "";
        }
        request = request.toLowerCase();
        int reqLen = request.length();
        String upperBound;
        char lastChar = request.charAt(reqLen - 1);
        Collection<String> res;

        if (lastChar == Character.MAX_VALUE) {
            res = cities.tailMap(String.valueOf(lastChar)).values();
        } else {
            lastChar++;
            upperBound = request.substring(0, reqLen - 1) + lastChar;
            res = cities.subMap(request, true, upperBound, false).values();
        }

        StringBuilder searchOut = new StringBuilder();
        Iterator<String> itr = res.iterator();
        while (itr.hasNext()) {
            searchOut.append(itr.next());
            if (itr.hasNext()) {
                searchOut.append(", ");
            }
        }
        return searchOut.toString();
    }

    public static void addCity(String city) {
        cities.put(city.toLowerCase(), city);
    }
}