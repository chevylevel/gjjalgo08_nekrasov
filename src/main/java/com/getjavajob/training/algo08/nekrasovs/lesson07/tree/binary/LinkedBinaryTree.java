package com.getjavajob.training.algo08.nekrasovs.lesson07.tree.binary;

import com.getjavajob.training.algo08.nekrasovs.lesson07.tree.Node;

import java.util.Collection;
import java.util.Iterator;

/**
 * Concrete implementation of a binary tree using a node-based, linked structure
 *
 * @param <E> element
 */
public class LinkedBinaryTree<E> extends AbstractBinaryTree<E> {
    protected Node<E> root;
    protected int size;

    // nonpublic utility

    /**
     * Validates the node is an instance of supported {@link NodeImpl} type and casts to it
     *
     * @param n node
     * @return casted {@link NodeImpl} node
     * @throws IllegalArgumentException
     */
    protected NodeImpl<E> validate(Node<E> n) throws IllegalArgumentException {
        if (n instanceof NodeImpl) {
            return (NodeImpl<E>) n;
        } else {
            throw new IllegalArgumentException();
        }
    }

    // update methods supported by this class

    @Override
    public Node<E> addRoot(E e) throws IllegalStateException {
        if (root == null && e != null) {
            root = new NodeImpl<>(e, null, null, null);
            size++;
            return root;
        } else {
            throw new IllegalStateException();
        }
    }

    @Override
    public Node<E> add(Node<E> n, E e) throws IllegalArgumentException {
        return left(n) == null ? addLeft(n, e) : addRight(n, e);
    }

    @Override
    public Node<E> addLeft(Node<E> n, E e) throws IllegalArgumentException {
        NodeImpl<E> validN = validate(n);
        if (validN.left == null) {
            size++;
            return validN.left = new NodeImpl<>(e, n, null, null);
        } else {
            throw new IllegalArgumentException("Node already has left child");
        }
    }

    @Override
    public Node<E> addRight(Node<E> n, E e) throws IllegalArgumentException {
        NodeImpl<E> validN = validate(n);
        if (validN.right == null) {
            size++;
            return validN.right = new NodeImpl<>(e, n, null, null);
        } else {
            throw new IllegalArgumentException("Node already has right child");
        }
    }

    /**
     * Replaces the element at {@link Node} <i>n</i> with <i>e</i>
     *
     * @param n node
     * @param e element
     * @return replace element
     * @throws IllegalArgumentException
     */
    @Override
    public E set(Node<E> n, E e) throws IllegalArgumentException {
        NodeImpl<E> validN = validate(n);
        E value = validN.value;
        validN.value = e;
        return value;
    }

    /**
     * Replaces the element at {@link Node} <i>n</i> with <i>e</i>
     *
     * @param n node
     * @return replace element
     * @throws IllegalArgumentException
     */

    @Override
    public E remove(Node<E> n) throws IllegalArgumentException {
        E value = n.getElement();
        Node<E> toDelete = validate(n);
        NodeImpl<E> parent = (NodeImpl<E>) parent(n);

        if (childrenNumber(toDelete) == 2) {
            Node<E> nextInOrder = right(toDelete);
            while (left(nextInOrder) != null) {
                nextInOrder = left(nextInOrder);
            }
            ((NodeImpl<E>) n).setValue(nextInOrder.getElement());
            toDelete = nextInOrder;
        }

        if (childrenNumber(toDelete) == 1) {
            NodeImpl<E> replacement = (left(toDelete) == null) ?
                    (NodeImpl<E>) right(toDelete) :
                    (NodeImpl<E>) left(toDelete);
            if (parent == null) {
                root = replacement;
            } else {
                if (left(parent) == toDelete) {
                    parent.setLeft(replacement);
                    replacement.setParent(parent);
                } else {
                    parent.setRight(replacement);
                    replacement.setParent(parent);
                }
            }
        }

        if (childrenNumber(toDelete) == 0) {
            if (parent == null) {
                root = null;
            } else {
                if (left(parent) == n) {
                    parent.setLeft(null);
                } else {
                    parent.setRight(null);
                }
            }
        }
        size--;
        return value;
    }

    // {@link Tree} and {@link BinaryTree} implementations

    @Override
    public Node<E> left(Node<E> p) throws IllegalArgumentException {
        NodeImpl<E> validP = validate(p);
        return validP.left;
    }

    @Override
    public Node<E> right(Node<E> p) throws IllegalArgumentException {
        NodeImpl<E> validP = validate(p);
        return validP.right;
    }

    @Override
    public Node<E> root() {
        return root;
    }

    @Override
    public Node<E> parent(Node<E> n) throws IllegalArgumentException {
        NodeImpl<E> validN = validate(n);
        return validN.parent;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public Iterator<E> iterator() {
        return new LbtIterator();
    }

    @Override
    public Collection<Node<E>> nodes() {
        return inOrder();
    }

    private class LbtIterator implements Iterator<E> {
        private Iterator<Node<E>> it = inOrder().iterator();
        Node<E> lastReturned;

        @Override
        public boolean hasNext() {
            return it.hasNext();
        }

        @Override
        public E next() {
            lastReturned = it.next();
            return lastReturned.getElement();
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

    protected static class NodeImpl<E> implements Node<E> {

        protected E value;
        protected NodeImpl<E> left;
        protected NodeImpl<E> right;
        protected NodeImpl<E> parent;

        public NodeImpl(E value, Node<E> parent, Node<E> left, Node<E> right) {
            this.value = value;
            this.parent = (NodeImpl<E>) parent;
            this.left = (NodeImpl<E>) left;
            this.right = (NodeImpl<E>) right;
        }

        public void setLeft(Node<E> left) {
            this.left = (NodeImpl<E>) left;
        }

        public void setRight(Node<E> right) {
            this.right = (NodeImpl<E>) right;
        }

        public void setValue(E value) {
            this.value = value;
        }

        public void setParent(Node<E> parent) {
            this.parent = (NodeImpl<E>) parent;
        }

        public NodeImpl<E> getLeft() {
            return left;
        }

        public NodeImpl<E> getRight() {
            return right;
        }

        @Override
        public E getElement() {
            return value;
        }
    }
}