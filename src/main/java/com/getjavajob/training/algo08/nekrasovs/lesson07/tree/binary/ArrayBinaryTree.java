package com.getjavajob.training.algo08.nekrasovs.lesson07.tree.binary;

import com.getjavajob.training.algo08.nekrasovs.lesson07.tree.Node;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;

/**
 * Concrete implementation of a binary tree using a array-based, linked structure
 *
 * @param <E> element
 */
public class ArrayBinaryTree<E> extends AbstractBinaryTree<E> {
    private final int DEFAULT_CAPACITY = 32;
    private NodeImpl<E>[] storage;
    private int size;

    public ArrayBinaryTree() {
        this.storage = (NodeImpl<E>[]) new NodeImpl[DEFAULT_CAPACITY];
    }

    protected NodeImpl<E> validate(Node<E> n) throws IllegalArgumentException {
        if (n instanceof NodeImpl) {
            return (NodeImpl<E>) n;
        } else {
            throw new IllegalArgumentException("Node is invalid");
        }
    }

    private void inflateStorage() {
        storage = Arrays.copyOf(storage, storage.length * 2);
    }

    // side == 1 for addLeft method, side == 2 for addRight method.
    private Node<E> addChild(Node<E> n, E e, int side) throws IllegalArgumentException {
        NodeImpl<E> validN = validate(n);
        int index = 2 * validN.index + side;
        if (index * 2 > storage.length) {
            inflateStorage();
        }
        if (storage[index] == null) {
            size++;
            return storage[index] = new NodeImpl<>(e, index);
        } else {
            String s = (side == 1) ? "left" : "right";
            throw new IllegalArgumentException("Node already has " + s + " child");
        }
    }

    @Override
    public Node<E> left(Node<E> p) throws IllegalArgumentException {
        NodeImpl<E> validP = validate(p);
        int index = 2 * validP.index + 1;
        return storage[index];
    }

    @Override
    public Node<E> right(Node<E> p) throws IllegalArgumentException {
        NodeImpl<E> validP = validate(p);
        int index = 2 * validP.index + 2;
        return storage[index];
    }

    @Override
    public Node<E> addLeft(Node<E> n, E e) throws IllegalArgumentException {
        return addChild(n, e, 1);
    }

    @Override
    public Node<E> addRight(Node<E> n, E e) throws IllegalArgumentException {
        return addChild(n, e, 2);
    }

    @Override
    public Node<E> root() {
        return storage[0];
    }

    @Override
    public Node<E> parent(Node<E> n) throws IllegalArgumentException {
        NodeImpl<E> validN = validate(n);
        if (validN.index == 0) {
            return null;
        }
        int index = (validN.index - 1) / 2;
        return storage[index];
    }

    @Override
    public Node<E> addRoot(E e) throws IllegalStateException {
        if (storage[0] == null) {
            storage[0] = new NodeImpl<>(e, 0);
            size++;
            return storage[0];
        } else {
            throw new IllegalStateException("Tree is not empty");
        }
    }

    @Override
    public Node<E> add(Node<E> n, E e) throws IllegalArgumentException {
        return left(n) == null ? addLeft(n, e) : addRight(n, e);
    }

    @Override
    public E set(Node<E> n, E e) throws IllegalArgumentException {
        NodeImpl<E> validN = validate(n);
        E value = validN.value;
        validN.value = e;
        return value;
    }

    @Override
    public E remove(Node<E> n) throws IllegalArgumentException {
        NodeImpl<E> validN = validate(n);
        if (isEmpty() || validN.index > storage.length - 1) {
            return null;
        } else {
            if (childrenNumber(n) == 0) {
                storage[validN.index] = null;
                size--;
            }
            if (childrenNumber(n) == 1) {
                NodeImpl<E> child = (NodeImpl<E>) ((left(n) == null) ? right(n) : left(n));
                storage[validN.index] = storage[child.index];
                storage[child.index] = null;
                size--;
            }
            if (childrenNumber(n) == 2) {
                NodeImpl<E> next = (NodeImpl<E>) right(validN);
                while (left(next) != null) {
                    next = (NodeImpl<E>) left(next);
                }
                storage[validN.index].value = storage[next.index].value;
                remove(next);
            }
            return validN.getElement();
        }
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public Iterator<E> iterator() {
        return new ArrayBtIterator();
    }

    @Override
    public Collection<Node<E>> nodes() {
        return inOrder();
    }

    private static class NodeImpl<E> implements Node<E> {
        E value;
        int index;

        public NodeImpl(E value, int index) {
            this.value = value;
            this.index = index;
        }

        @Override
        public E getElement() {
            return value;
        }
    }

    private class ArrayBtIterator implements Iterator<E> {
        private Collection<Node<E>> elements = inOrder();
        private Iterator<Node<E>> it = elements.iterator();

        @Override
        public boolean hasNext() {
            return it.hasNext();
        }

        @Override
        public E next() {
            return it.next().getElement();
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
}