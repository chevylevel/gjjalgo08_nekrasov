package com.getjavajob.training.algo08.nekrasovs.lesson01;

import static com.getjavajob.training.algo08.nekrasovs.lesson01.Task06.*;
import static com.getjavajob.training.algo08.util.Assert.assertEquals;

public class Task06Test {
    public static void main(String[] args) {
        testA();
        testB();
        testC();
        testD();
        testE();
        testF();
        testG();
        testH();
        testI();
    }

    public static void testA() {
        assertEquals("Task06Test.testA", 8, a(3));
    }

    public static void testB() {
        assertEquals("Task06Test.testB", 12, b(2, 3));
    }

    public static void testC() {
        assertEquals("Task06Test.testC", 0b11110000, c(0b11111111, 4));
    }

    public static void testD() {
        assertEquals("Task06Test.testD", 0b101, d(0b1, 2));
    }

    public static void testE() {
        assertEquals("Task06Test.testE", 0b111, d(0b101, 1));
    }

    public static void testF() {
        assertEquals("Task06Test.testF", 0b11011, f(0b11111, 2));
    }

    public static void testG() {
        assertEquals("Task06Test.testG", 0b101, g(0b11101, 3));
    }

    public static void testH() {
        assertEquals("Task06Test.testH", 0b1, h(0b1000, 3));
    }

    public static void testI() {
        assertEquals("Task06Test.testI", "0b00000010", i((byte) 2));
    }
}