package com.getjavajob.training.algo08.nekrasovs.lesson09.tree.binary.search.balanced;

import com.getjavajob.training.algo08.nekrasovs.lesson07.tree.Node;
import com.getjavajob.training.algo08.nekrasovs.lesson08.tree.binary.search.BinarySearchTree;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;

public class RedBlackTreeTest {
    private static BinarySearchTree<Integer> testTree = new RedBlackTree<>();

    public static void main(String[] args) {
        testAdd();
        testRemove();
    }

    private static void testAdd() {
        testTree.add(5);  //case 1: parent == null
        assertEquals("RedBlackTreeTest.testAdd", "(5b)", testTree.toString());

        testTree.add(3); //case 2: parent is BLACK
        assertEquals("RedBlackTreeTest.testAdd", "(5b(3r))", testTree.toString());
        testTree.add(7); //(5b(3r, 7r))
        testTree.add(9); //(5b(3b, 7b(9r)))

        testTree.add(13);//case 5:
        assertEquals("RedBlackTreeTest.testAdd", "(5b(3b, 9b(7r, 13r)))", testTree.toString());
        testTree.add(0); //(5b(3b(0r), 9b(7r, 13r)))

        testTree.add(2); // case 4: P is RED, U is BLACK
        assertEquals("RedBlackTreeTest.testAdd", "(5b(2b(0r, 3r), 9b(7r, 13r)))", testTree.toString());

        testTree.add(8); // case 3: P is RED, U is RED
        assertEquals("RedBlackTreeTest.testAdd", "(5b(2b(0r, 3r), 9r(7b(8r), 13b)))", testTree.toString());
    }

    private static void testRemove() {
        // case 2: two children, successor to delete is RED
        Node<Integer> n2 = testTree.treeSearch(testTree.root(), 2);
        testTree.remove(n2);
        assertEquals("RedBlackTreeTest.testRemove", "(5b(3b(0r), 9r(7b(8r), 13b)))", testTree.toString());
        testTree.add(2);

        // Double black situations:

        // case 5: parent is RED, sibling is BLACK, right of sibling is RED, while node itself is right.
        Node<Integer> n13 = testTree.treeSearch(testTree.root(), 13);
        testTree.remove(n13);
        assertEquals("RedBlackTreeTest.testRemove", "(5b(2b(0r, 3r), 8r(7b, 9b)))", testTree.toString());

        // case 3 - 4: sibling is BLACK, children of sibling are BLACK, parent is RED
        Node<Integer> n7 = testTree.treeSearch(testTree.root(), 7);
        testTree.remove(n7);
        assertEquals("RedBlackTreeTest.testRemove", "(5b(2b(0r, 3r), 8b(9r)))", testTree.toString());
        testTree.add(-1);

        // case 6: sibling is BLACK,  left of sibling is RED, while node itself is right.
        Node<Integer> n3 = testTree.treeSearch(testTree.root(), 3);
        //System.out.println(testTree);
        testTree.remove(n3);
        assertEquals("RedBlackTreeTest.testRemove", "(5b(0r(-1b, 2b), 8b(9r)))", testTree.toString());

        n2 = testTree.treeSearch(testTree.root(), 2);
        testTree.remove(n2);

        Node<Integer> nMin1 = testTree.treeSearch(testTree.root(), -1);
        testTree.remove(nMin1);

        Node<Integer> n0 = testTree.treeSearch(testTree.root(), 0);
        testTree.remove(n0);

        // element to fix is root
        Node<Integer> n5 = testTree.treeSearch(testTree.root(), 5);
        testTree.remove(n5);
        assertEquals("RedBlackTreeTest.testRemove", "(8b(9r))", testTree.toString());

        Node<Integer> n8 = testTree.treeSearch(testTree.root(), 8);
        testTree.remove(n8);

        // remove root
        Node<Integer> n9 = testTree.treeSearch(testTree.root(), 9);
        testTree.remove(n9);
        assertEquals("RedBlackTreeTest.testRemove", "( )", testTree.toString());
    }
}
