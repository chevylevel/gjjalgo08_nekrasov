package com.getjavajob.training.algo08.nekrasovs.lesson06;

import java.util.HashMap;
import java.util.Map;

import static com.getjavajob.training.algo08.util.StopWatch.getElapsedTime;
import static com.getjavajob.training.algo08.util.StopWatch.start;

public class AssociativeArrayPerfomanceTest {

    private static final int SIZE = 5_000_000;
    private static final Integer[] KEYS = new Integer[SIZE];

    public static void main(String[] args) {
        for (Integer i = 0; i < SIZE; i++) {
            KEYS[i] = i;
        }
        //testAddAa();
        //testPutJdk();
        //testGetAa();
        //testGetJdkTable();
        //testRemoveAa();
        //testRemoveJdkTable();
    }

    public static void testPutJdk() {
        Map<Integer, String> jdkTable = new HashMap<>();
        start();
        for (Integer k : KEYS) {
            jdkTable.put(k, "value");
        }
        long time = getElapsedTime();
        System.out.println("jdkTable.put(k, v): " + time);
    }

    public static void testAddAa() {
        AssociativeArray<Integer, String> myTable = new AssociativeArray<>();
        start();
        for (Integer k : KEYS) {
            myTable.add(k, "value");
        }
        long time = getElapsedTime();
        System.out.println("myTable.add(k, v): " + time);
    }

    public static void testGetJdkTable() {
        Map<Integer, String> jdkTable = new HashMap<>();
        for (Integer k : KEYS) {
            jdkTable.put(k, "value");
        }
        start();
        for (Integer k : KEYS) {
            jdkTable.get(k);
        }
        long time = getElapsedTime();
        System.out.println("jdkTable.get(k, v): " + time);
    }

    public static void testGetAa() {
        AssociativeArray<Integer, String> myTable = new AssociativeArray<>();
        for (Integer k : KEYS) {
            myTable.add(k, "value");
        }
        start();
        for (Integer k : KEYS) {
            myTable.get(k);
        }
        long time = getElapsedTime();
        System.out.println("myTable.get(k, v): " + time);
    }

    public static void testRemoveJdkTable() {
        Map<Integer, String> jdkTable = new HashMap<>();
        for (Integer k : KEYS) {
            jdkTable.put(k, "value");
        }
        start();
        for (Integer k : KEYS) {
            jdkTable.remove(k);
        }
        long time = getElapsedTime();
        System.out.println("jdkTable.remove(k, v): " + time);
    }

    public static void testRemoveAa() {
        AssociativeArray<Integer, String> myTable = new AssociativeArray<>();
        for (Integer k : KEYS) {
            myTable.add(k, "value");
        }
        start();
        for (Integer k : KEYS) {
            myTable.remove(k);
        }
        long time = getElapsedTime();
        System.out.println("myTable.remove(k, v): " + time);
    }
}