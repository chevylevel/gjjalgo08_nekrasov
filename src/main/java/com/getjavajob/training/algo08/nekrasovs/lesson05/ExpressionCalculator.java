package com.getjavajob.training.algo08.nekrasovs.lesson05;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ExpressionCalculator {

    private final static String ops = "-+/*";
    private static Deque<String> outputQueue = new ArrayDeque<>();
    private static Deque<Character> operators = new ArrayDeque<>();
    private static StringBuilder operands = new StringBuilder();

    public static boolean isExpCorrect(String exp) {
        char[] expChars = exp.toCharArray();
        int parenthesis = 0;
        Pattern p = Pattern.compile("(^[^0-9(])|" +
                "([^0-9)]$)|" +
                "([+\\-*/]{2})|" +
                "(\\([^0-9(])|" +
                "([^0-9)]\\))|" +
                "(\\)\\()|" +
                "([0-9]\\()|" +
                "(\\)[0-9])|" +
                "(\\([0-9+]\\))|" +
                "([^0-9()+\\-*/])");
        Matcher m = p.matcher(exp);

        for (char i : expChars) {
            if (i == '(') {
                parenthesis++;
            } else if (i == ')') {
                parenthesis--;
                if (parenthesis < 0) {
                    return false;
                }
            }
        }
        return parenthesis == 0 && !m.find();
    }

    public static String convertInfixToPostfix(String infixExp) throws IllegalArgumentException {
        if (infixExp.length() != 0 && isExpCorrect(infixExp)) {
            for (int i = 0; i < infixExp.length(); i++) {
                char charAtI = infixExp.charAt(i);
                if (Character.isDigit(charAtI)) {
                    operands.append(charAtI);
                    if (i == infixExp.length() - 1) {
                        operands.append(' ');
                        outputQueue.add(operands.toString());
                        operands.setLength(0);
                    }
                } else if (charAtI == '+' ||
                        charAtI == '-' ||
                        charAtI == '*' ||
                        charAtI == '/') {
                    if (operands.length() != 0) {
                        operands.append(' ');
                    }
                    outputQueue.add(operands.toString());
                    operands.setLength(0);
                    while (operators.peekLast() != null &&
                            ops.indexOf(operators.peekLast()) >> 1 >= ops.indexOf(charAtI) >> 1) {
                        outputQueue.add(operators.pollLast().toString());
                    }
                    operators.add(charAtI);
                } else if (charAtI == '(') {
                    operators.add(charAtI);
                } else if (charAtI == ')') {
                    if (operands.length() != 0) {
                        operands.append(' ');
                        outputQueue.add(operands.toString());
                        operands.setLength(0);
                    }
                    while (operators.peekLast() != '(') {
                        outputQueue.add(operators.pollLast().toString());
                    }
                    operators.pollLast();
                }
            }
            while (operators.size() != 0) {
                outputQueue.add(operators.pollLast().toString());
            }
            StringBuilder outputRpn = new StringBuilder();
            while (outputQueue.size() != 0) {
                outputRpn.append(outputQueue.remove());
            }
            return outputRpn.toString();
        } else {
            throw new IllegalArgumentException("Wrong expression");
        }
    }

    public static double calcPostfixExp(String postfixExp) {
        char[] postfixChars = postfixExp.toCharArray();
        double temp = 0;
        double left;
        double right;
        for (char i : postfixChars) {
            if (Character.isDigit(i)) {
                operands.append(i);
            } else if (i == ' ') {
                outputQueue.add(operands.toString());
                operands.setLength(0);
            } else if (ops.indexOf(i) != -1) {
                right = Double.valueOf(outputQueue.pollLast());
                left = Double.valueOf(outputQueue.pollLast());
                switch (i) {
                    case '+':
                        temp = left + right;
                        break;
                    case '-':
                        temp = left - right;
                        break;
                    case '*':
                        temp = left * right;
                        break;
                    case '/':
                        temp = left / right;
                }
                outputQueue.add(String.valueOf(temp));
            }
        }
        return Double.valueOf(outputQueue.pollLast());
    }
}