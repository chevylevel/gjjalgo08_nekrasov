package com.getjavajob.training.algo08.nekrasovs.lesson05;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;

public class LinkedListStackTest {
    public static void main(String[] args) {
        testPushAndPop();
    }

    public static void testPushAndPop() {
        LinkedListStack<String> lls = new LinkedListStack<>();
        lls.push("t");
        lls.push("e");
        lls.push("s");
        lls.push("t");

        String testString = "";
        testString += lls.pop();
        testString += lls.pop();
        testString += lls.pop();
        testString += lls.pop();

        assertEquals("LinkedListStackTest.testPush", "tset", testString);
    }
}