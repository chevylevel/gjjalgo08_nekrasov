package com.getjavajob.training.algo08.nekrasovs.lesson04;

import java.util.LinkedList;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;

public class DoublyLinkedListTest {
    public static void main(String[] args) {
        testAdd();
        testAddToBegin();
        testAddToMiddle();
        testAddToEnd();
        testRemove();
        testRemoveBegin();
        testRemoveMiddle();
        testRemoveEnd();
        testGet();
    }

    public static void testAdd() {
        DoublyLinkedList<String> dll = new DoublyLinkedList<>();
        dll.add("s");
        assertEquals("DoublyLinkedListTest.testAdd", "s", dll.get(0));
    }

    public static void testAddToBegin() {
        DoublyLinkedList<String> dll = new DoublyLinkedList<>();
        dll.add("s");
        dll.add(0, "t");
        assertEquals("DoublyLinkedListTest.testAddToBegin", "t", dll.get(0));
    }

    public static void testAddToMiddle() {
        DoublyLinkedList<String> dll = new DoublyLinkedList<>();
        dll.add("s");
        dll.add("r");
        dll.add(1, "t");
        assertEquals("DoublyLinkedListTest.testAddToMiddle", "t", dll.get(1));
    }

    public static void testAddToEnd() {
        DoublyLinkedList<String> dll = new DoublyLinkedList<>();
        dll.add("s");
        dll.add("t");
        dll.add(2, "r");
        assertEquals("DoublyLinkedListTest.testAddToEnd", "r", dll.get(2));
    }

    public static void testRemove() {
        DoublyLinkedList<Object> dll = new DoublyLinkedList<>();
        dll.add("s");
        dll.add("t");
        dll.remove("s");
        assertEquals("DoublyLinkedListTest.testRemove", "t", dll.get(0));
    }

    public static void testRemoveBegin() {
        DoublyLinkedList<Object> dll = new DoublyLinkedList<>();
        dll.add("s");
        dll.add("t");
        dll.add("r");
        assertEquals("DoublyLinkedListTest.testRemoveBegin", "s", dll.remove(0));
    }

    public static void testRemoveMiddle() {
        DoublyLinkedList<Object> dll = new DoublyLinkedList<>();
        dll.add("s");
        dll.add("t");
        dll.add("r");
        assertEquals("DoublyLinkedListTest.testRemoveMiddle", "t", dll.remove(1));
    }

    public static void testRemoveEnd() {
        LinkedList<Object> dll = new LinkedList<>();
        dll.add("s");
        dll.add("t");
        dll.add("r");
        assertEquals("DoublyLinkedListTest.testRemoveEnd", "r", dll.remove(2));
    }

    public static void testGet() {
        LinkedList<Object> dll = new LinkedList<>();
        dll.add("s");
        assertEquals("DoublyLinkedListTest.testGet", "s", dll.get(0));
    }
}