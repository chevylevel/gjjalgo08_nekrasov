package com.getjavajob.training.algo08.nekrasovs.lesson05;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class CollectionUtils {
    public static <T> boolean filter(Collection<T> collection, MyPredicate<T> predicate) {
        Iterator<T> colIterator = collection.iterator();
        boolean isModified = false;
        while (colIterator.hasNext()) {
            if (!predicate.test(colIterator.next())) {
                colIterator.remove();
                isModified = true;
            }
        }
        return isModified;
    }

    public static <I, O> Collection<O> transform(Collection<I> inputCol, MyTransformer<I, O> transformer) {
        Collection<O> outputCol = new ArrayList<>();
        for (I anInputCol : inputCol) {
            outputCol.add(transformer.transformElement(anInputCol));
        }
        return outputCol;
    }

    public static <I, O> Collection<O> transform2(Collection<I> inputCol, MyTransformer<I, O> transformer) {
        return transformer.transformCollection(inputCol);
    }

    public static <E> Collection<E> forAllDo(Collection<E> collection, MyClosure<E> closure) {
        Iterator<E> colIterator = collection.iterator();
        while (colIterator.hasNext()) {
            closure.apply(colIterator.next());
        }
        return collection;
    }

    public static <E> Collection<E> unmodifiableCollection(Collection<E> passedCol) {
        return new UnmodifiableCollection<>(passedCol);
    }
}