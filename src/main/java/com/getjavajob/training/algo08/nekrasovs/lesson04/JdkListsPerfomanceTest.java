package com.getjavajob.training.algo08.nekrasovs.lesson04;

import java.util.*;

import static com.getjavajob.training.algo08.util.StopWatch.getElapsedTime;
import static com.getjavajob.training.algo08.util.StopWatch.start;

public class JdkListsPerfomanceTest {

    public static void main(String[] args) {
        //testAddLl();
        //testAddAl();

        //testAddToBeginLl();
        //testAddToBeginAl();

        //testAddToMiddleLl();
        //testAddToMiddleAl();

        //testAddToEndLl();
        //testAddToEndAl();

        //testRemoveLl();
        //testRemoveAl();

        //testRemoveBeginLl();
        //testRemoveBeginAl();

        //testRemoveMiddleLl();
        //testRemoveMiddleAl();

        //testRemoveEndLl();
        //testRemoveEndAl();
    }

    public static void testAddLl() {
        String[] testArray = new String[9_000_000];
        Arrays.fill(testArray, "0");
        LinkedList<String> ll = new LinkedList<>();
        start();
        for (String i : testArray) {
            ll.add(i);
        }
        long time = getElapsedTime();
        System.out.println("LinkedList.add(e): " + time);
    }

    public static void testAddAl() {
        String[] testArray = new String[9_000_000];
        Arrays.fill(testArray, "0");
        List<String> al = new ArrayList<>();
        start();
        for (String i : testArray) {
            al.add(i);
        }
        long time = getElapsedTime();
        System.out.println("ArrayList.add(e): " + time);
    }

    public static void testAddToBeginLl() {
        int[] testArray = new int[200_000];
        Arrays.fill(testArray, 1);
        LinkedList<Object> ll = new LinkedList<>();
        start();
        for (int i : testArray) {
            ll.add(0, i);
        }
        long time = getElapsedTime();
        System.out.println("LinkedList.add(0, e): " + time);
    }

    public static void testAddToBeginAl() {
        int[] testArray = new int[200_000];
        Arrays.fill(testArray, 1);
        List<Object> al = new ArrayList<>();
        start();
        for (int i : testArray) {
            al.add(0, i);
        }
        long time = getElapsedTime();
        System.out.println("ArrayList.add(0, e): " + time);
    }

    public static void testAddToMiddleLl() {
        int[] testArray = new int[100_000];
        Arrays.fill(testArray, 1);
        LinkedList<Integer> ll = new LinkedList<>();
        for (int i : testArray) {
            ll.add(i);
        }
        start();
        for (int i = 0; i < 30_000; i++) {
            ll.add(ll.size() >> 1, 1);
        }
        long time = getElapsedTime();
        System.out.println("LinkedList.add(ll.size() / 2, e): " + time);
    }

    public static void testAddToMiddleAl() {
        int[] testArray = new int[100_000];
        Arrays.fill(testArray, 1);
        List<Object> al = new ArrayList<>();
        for (int i : testArray) {
            al.add(i);
        }
        start();
        for (int i : testArray) {
            al.add(al.size() >> 1, i);
        }
        long time = getElapsedTime();
        System.out.println("ArrayList.add(al.size() / 2, e): " + time);
    }

    public static void testAddToEndLl() {
        int[] testArray = new int[5_000_000];
        Arrays.fill(testArray, 1);
        LinkedList<Object> ll = new LinkedList<>();
        for (int i : testArray) {
            ll.add(i);
        }
        start();
        for (int i = 5_000_000; i < 10_000_000; i++) {
            ll.add(i, 1);
        }
        long time = getElapsedTime();
        System.out.println("LinkedList.add(i, e): " + time);
    }

    public static void testAddToEndAl() {
        int[] testArray = new int[5_000_000];
        Arrays.fill(testArray, 1);
        List<Object> testAl = new ArrayList<>();
        for (int i : testArray) {
            testAl.add(i);
        }
        start();
        for (int i = 5_000_000; i < 10_000_000; i++) {
            testAl.add(i, 1);
        }
        long time = getElapsedTime();
        System.out.println("ArrayList.add(i, e): " + time);
    }

    public static void testRemoveLl() {
        String[] testArray = new String[300_000];
        Arrays.fill(testArray, "0");
        LinkedList<String> ll = new LinkedList<>();
        Collections.addAll(ll, testArray);
        start();
        for (String i : testArray) {
            ll.remove(i);
        }
        long time = getElapsedTime();
        System.out.println("LinkedList.remove(e): " + time);
    }

    public static void testRemoveAl() {
        String[] testArray = new String[300_000];
        Arrays.fill(testArray, "0");
        List<Object> al = new ArrayList<>();
        Collections.addAll(al, testArray);
        start();
        for (String i : testArray) {
            al.remove(i);
        }
        long time = getElapsedTime();
        System.out.println("ArrayList.remove(e): " + time);
    }

    public static void testRemoveBeginLl() {
        int[] testArray = new int[300_000];
        Arrays.fill(testArray, 1);
        LinkedList<Object> ll = new LinkedList<>();
        for (int i : testArray) {
            ll.add(i);
        }
        start();
        for (int i = 0; i < 300_000; i++) {
            ll.remove(0);
        }
        long time = getElapsedTime();
        System.out.println("LinkedList.remove(0): " + time);
    }

    public static void testRemoveBeginAl() {
        int[] testArray = new int[300_000];
        Arrays.fill(testArray, 1);
        List<Object> al = new ArrayList<>();
        for (int i : testArray) {
            al.add(i);
        }
        start();
        for (int i = 0; i < 300_000; i++) {
            al.remove(0);
        }
        long time = getElapsedTime();
        System.out.println("ArrayList.remove(0): " + time);
    }

    public static void testRemoveMiddleLl() {
        int[] testArray = new int[200_000];
        Arrays.fill(testArray, 1);
        LinkedList<Object> ll = new LinkedList<>();
        for (int i : testArray) {
            ll.add(i);
        }
        start();
        for (int i = 0; i < 50_000; i++) {
            ll.remove(ll.size() >> 1);
        }
        long time = getElapsedTime();
        System.out.println("LinkedList.remove(ll.size() / 2): " + time);
    }

    public static void testRemoveMiddleAl() {
        int[] testArray = new int[200_000];
        Arrays.fill(testArray, 1);
        List<Object> al = new ArrayList<>();
        for (int i : testArray) {
            al.add(i);
        }
        start();
        for (int i = 0; i < 50_000; i++) {
            al.remove(al.size() >> 1);
        }
        long time = getElapsedTime();
        System.out.println("ArrayList.remove(al.size() / 2): " + time);
    }

    public static void testRemoveEndLl() {
        int[] testArray = new int[9_000_000];
        Arrays.fill(testArray, 1);
        LinkedList<Object> ll = new LinkedList<>();
        for (int i : testArray) {
            ll.add(i);
        }
        start();
        for (int i = 8_999_999; i >= 0; i--) {
            ll.remove(i);
        }
        long time = getElapsedTime();
        System.out.println("LinkedList.remove(i): " + time);
    }

    public static void testRemoveEndAl() {
        int[] testArray = new int[9_000_000];
        Arrays.fill(testArray, 1);
        List<Object> al = new ArrayList<>();
        for (int i : testArray) {
            al.add(i);
        }
        start();
        for (int i = 8_999_999; i >= 0; i--) {
            al.remove(i);
        }
        long time = getElapsedTime();
        System.out.println("ArrayList.remove(i): " + time);
    }
}