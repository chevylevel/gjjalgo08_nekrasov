package com.getjavajob.training.algo08.nekrasovs.lesson07.tree.binary;

import com.getjavajob.training.algo08.nekrasovs.lesson07.tree.AbstractTree;
import com.getjavajob.training.algo08.nekrasovs.lesson07.tree.Node;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;

public abstract class AbstractBinaryTree<E> extends AbstractTree<E> implements BinaryTree<E> {
    @Override
    public Node<E> sibling(Node<E> n) throws IllegalArgumentException {
        Node<E> p = parent(n);
        return n == left(p) ? right(p) : left(p);
    }

    @Override
    public Collection<Node<E>> children(Node<E> n) throws IllegalArgumentException {
        Collection<Node<E>> children = new ArrayList<>();
        if (left(n) != null) {
            children.add(left(n));
        }
        if (right(n) != null) {
            children.add(right(n));
        }
        return children;
    }

    @Override
    public int childrenNumber(Node<E> n) throws IllegalArgumentException {
        if (left(n) != null && right(n) != null) {
            return 2;
        }
        return left(n) == null && right(n) == null ? 0 : 1;
    }

    private Collection<Node<E>> in(Node<E> n) throws IllegalArgumentException {
        if (n == null) {
            throw new IllegalArgumentException();
        }
        Collection<Node<E>> buf = new ArrayDeque<>();
        Node<E> left = left(n);
        Node<E> right = right(n);
        Node<E> node = (left == null ? right : left);
        if (node == null) {
            buf.add(n);
            return buf;
        }
        buf.addAll(in(node));
        buf.add(n);
        if (node == right) {
            buf.add(n);
            return buf;
        } else {
            if (right != null) {
                buf.addAll(in(right));
            }
        }
        return buf;
    }

    @Override
    public String toString() {
        if (isEmpty()) {
            return "( )";
        }
        StringBuilder treeSb = new StringBuilder();
        Collection<Node<E>> elements = preOrder();
        Node<E> prev = null;
        boolean preStateIsInternal = true;
        int brackets = 0;
        for (Node<E> e : elements) {
            if (preStateIsInternal) {
                treeSb.append("(");
                brackets++;
            }
            if (!preStateIsInternal) {
                if (parent(e) == parent(prev)) {
                    treeSb.append(", ");
                } else {
                    treeSb.append("), ");
                    brackets--;
                }
            }

            treeSb.append(String.valueOf(e.getElement()));
            preStateIsInternal = isInternal(e);
            prev = e;
        }
        for (int b = brackets; b != 0; b--) {
            treeSb.append(")");
        }
        return treeSb.toString();
    }

    /**
     * @return an iterable collection of nodes of the tree in inorder
     */
    public Collection<Node<E>> inOrder() {
        return in(root());
    }
}