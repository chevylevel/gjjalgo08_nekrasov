package com.getjavajob.training.algo08.nekrasovs.lesson05;

import java.util.Collection;
import java.util.Iterator;

public class UnmodifiableCollection<E> implements Collection<E> {
    private final Collection<E> unmodifiableCollection;

    public UnmodifiableCollection(Collection<E> c) {
        this.unmodifiableCollection = c;
    }

    @Override
    public int size() {
        return unmodifiableCollection.size();
    }

    @Override
    public boolean isEmpty() {
        return unmodifiableCollection.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return unmodifiableCollection.contains(o);
    }

    @Override
    public Object[] toArray() {
        return unmodifiableCollection.toArray();
    }

    @Override
    public boolean add(Object o) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean remove(Object o) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void clear() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean retainAll(Collection c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean removeAll(Collection c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean containsAll(Collection c) {
        return unmodifiableCollection.containsAll(c);
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return (T[]) unmodifiableCollection.toArray(a);
    }

    @Override
    public Iterator<E> iterator() {

        return new Iterator<E>() {
            @Override
            public boolean hasNext() {
                return unmodifiableCollection.iterator().hasNext();
            }

            @Override
            public E next() {
                return unmodifiableCollection.iterator().next();
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException();
            }
        };
    }
}