package com.getjavajob.training.algo08.nekrasovs.lesson06;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;

public class SetTest {
    public static void main(String[] args) {
        testAdd();
        testAddAll();
    }

    public static void testAdd() {
        Set<String> s = new HashSet<>();
        assertEquals("SetTest.testAdd", true, s.add("value"));
        assertEquals("SetTest.testAdd", false, s.add("value"));
    }

    public static void testAddAll() {
        Set<String> s = new HashSet<>();
        Collection<String> c = new ArrayList<>();
        c.add("1");
        c.add("2");
        c.add("3");
        assertEquals("SetTest.testAddAll", true, s.addAll(c));
        assertEquals("SetTest.testAddAll", false, s.addAll(c));
    }
}