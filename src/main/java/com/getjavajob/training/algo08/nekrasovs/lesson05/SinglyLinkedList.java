package com.getjavajob.training.algo08.nekrasovs.lesson05;

import java.util.ArrayList;
import java.util.List;

public class SinglyLinkedList<E> {
    Node<E> head;

    public E get(int index) {
        Node<E> e = head;
        for (int i = 0; i != index; i++)
            e = e.next;
        return e.val;
    }

    public int size() {
        int i = 0;
        for (Node<E> e = head; e != null; e = e.next) {
            i++;
        }
        return i;
    }

    public void add(E e) {
        Node<E> newElement = new Node<>();
        newElement.val = e;
        if (head == null) {
            head = newElement;
        } else {
            Node<E> element = head;
            while (element.next != null) {
                element = element.next;
            }
            element.next = newElement;
        }
    }

    public void reverse() {
        for (Node<E> e = head, temp = head.next; temp != null; temp = e.next) {
            e.next = temp.next;
            temp.next = head;
            head = temp;
        }
    }

    public void swap(Node<E> a, Node<E> b) {
        for (Node<E> e = head, aNext = a.next, bNext = b.next, next; e != null; e = next) {
            if (e.next == a) {
                if (e != b) {
                    e.next = b;
                    if (a.next == b) {
                        a.next = b.next;
                        b.next = a;
                        break;
                    } else {
                        if (b.next == aNext) {
                            next = null;
                        } else {
                            next = a.next;
                        }
                        a.next = bNext;
                    }
                } else {
                    e.next = a.next;
                    a.next = b;
                    head = a;
                    break;
                }
            } else if (e.next == b) {
                if (e != a) {
                    e.next = a;
                    if (b.next == a) {
                        b.next = a.next;
                        a.next = b;
                        break;
                    } else {
                        if (a.next == bNext) {
                            next = null;
                        } else {
                            next = b.next;
                        }
                        b.next = aNext;
                    }
                } else {
                    e.next = b.next;
                    b.next = a;
                    head = b;
                    break;
                }
            } else if (e == a) {
                next = e.next;
                e.next = b.next;
                head = b;
            } else if (e == b) {
                next = e.next;
                e.next = a.next;
                head = a;
            } else {
                next = e.next;
            }
        }
    }

    public List<E> asList() {
        List<E> list = new ArrayList<>();
        for (Node<E> t = head; t != null; t = t.next) {
            list.add(t.val);
        }
        return list;
    }

    static class Node<E> {
        Node<E> next;
        E val;
    }
}