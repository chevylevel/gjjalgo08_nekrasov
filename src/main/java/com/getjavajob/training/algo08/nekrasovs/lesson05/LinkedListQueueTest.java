package com.getjavajob.training.algo08.nekrasovs.lesson05;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;

public class LinkedListQueueTest {
    public static void main(String[] args) {
        testAddAndRemove();
    }

    public static void testAddAndRemove() {
        LinkedListQueue<String> llq = new LinkedListQueue<>();
        llq.add("t");
        llq.add("e");
        llq.add("s");
        llq.add("t");

        String testString = "";

        testString += llq.remove();
        testString += llq.remove();
        testString += llq.remove();
        testString += llq.remove();

        assertEquals("LinkedListQueueTest.testAddAndRemove", "test", testString);
    }
}