package com.getjavajob.training.algo08.nekrasovs.lesson05;


public interface Stack<E> {
    /**
     * add element to the top
     *
     * @param e element add to
     */
    void push(E e);

    /**
     * removes element from the top
     *
     * @return removed element e
     */
    E pop();
}
