package com.getjavajob.training.algo08.nekrasovs.lesson09;

import java.util.Iterator;
import java.util.NavigableSet;
import java.util.TreeSet;

import static com.getjavajob.training.algo08.nekrasovs.lesson09.SortedSetTest.*;
import static com.getjavajob.training.algo08.util.Assert.assertEquals;

public class NavigableSetTest {

    private static NavigableSet<String> emptyTs = new TreeSet<>();

    public static void main(String[] args) {
        testLower();
        testFloor();
        testCeiling();
        testHigher();
        testPollFirst();
        testPollLast();
        testIterator();
        testDescendingSet();
        testDescendingIterator();
        testSubSet();
        testHeadSet();
        testTailSet();
    }

    public static void testLower() {
        NavigableSet<String> nS = new TreeSet<>();
        nS.add("s1");
        nS.add("s2");
        nS.add("s3");

        assertEquals("NavigableSetTest.testLower", "s1", nS.lower("s2"));
        assertEquals("NavigableSetTest.testLower", null, nS.lower("s1"));

        NavigableSet<CustomClass> tSForCce = new TreeSet<>();
        tSForCce.add(new SubClass2("k1", 1, "1"));

        try {
            tSForCce.lower(new SubClass1("k1", 1, "1"));
        } catch (ClassCastException e) {
            System.out.println("CCE, when passed elements cannot be compared with" +
                    " the elements currently in the set, was thrown");
        }

        try {
            nS.lower(null);
        } catch (NullPointerException e) {
            System.out.println("NPE, when passed null element, was thrown");
        }
        System.out.println("///////////////////////////////////////////");
    }

    public static void testFloor() {
        NavigableSet<String> nS = new TreeSet<>();
        nS.add("s1");
        nS.add("s2");
        nS.add("s3");

        assertEquals("NavigableSetTest.testFloor", "s2", nS.floor("s2"));
        assertEquals("NavigableSetTest.testFloor", null, nS.floor("s0"));

        NavigableSet<CustomClass> tSForCce = new TreeSet<>();
        tSForCce.add(new SubClass2("k1", 1, "1"));

        try {
            tSForCce.floor(new SubClass1("k1", 1, "1"));
        } catch (ClassCastException e) {
            System.out.println("CCE, when passed elements cannot be compared with" +
                    " the elements currently in the set, was thrown");
        }

        try {
            nS.floor(null);
        } catch (NullPointerException e) {
            System.out.println("NPE, when passed null element, was thrown");
        }
        System.out.println("///////////////////////////////////////////");
    }

    public static void testCeiling() {
        NavigableSet<String> nS = new TreeSet<>();
        nS.add("s1");
        nS.add("s2");
        nS.add("s3");

        assertEquals("NavigableSetTest.testCeiling", "s2", nS.ceiling("s2"));
        assertEquals("NavigableSetTest.testCeiling", null, nS.ceiling("s4"));

        NavigableSet<CustomClass> tSForCce = new TreeSet<>();
        tSForCce.add(new SubClass2("k1", 1, "1"));

        try {
            tSForCce.ceiling(new SubClass1("k1", 1, "1"));
        } catch (ClassCastException e) {
            System.out.println("CCE, when passed elements cannot be compared with" +
                    " the elements currently in the set, was thrown");
        }

        try {
            nS.ceiling(null);
        } catch (NullPointerException e) {
            System.out.println("NPE, when passed null element, was thrown");
        }
        System.out.println("///////////////////////////////////////////");
    }

    public static void testHigher() {
        NavigableSet<String> nS = new TreeSet<>();
        nS.add("s1");
        nS.add("s2");
        nS.add("s3");

        assertEquals("NavigableSetTest.testHigher", "s2", nS.higher("s1"));
        assertEquals("NavigableSetTest.testHigher", null, nS.higher("s3"));

        NavigableSet<CustomClass> tSForCce = new TreeSet<>();
        tSForCce.add(new SubClass2("k1", 1, "1"));

        try {
            tSForCce.higher(new SubClass1("k1", 1, "1"));
        } catch (ClassCastException e) {
            System.out.println("CCE, when passed elements cannot be compared with" +
                    " the elements currently in the set, was thrown");
        }

        try {
            nS.higher(null);
        } catch (NullPointerException e) {
            System.out.println("NPE, when passed null element, was thrown");
        }
        System.out.println("///////////////////////////////////////////");
    }

    public static void testPollFirst() {
        NavigableSet<String> tS = new TreeSet<>();
        tS.add("k1");
        tS.add("k2");
        tS.add("k3");

        assertEquals("NavigableSetTest.testPollFirst", "k1", tS.pollFirst());
        assertEquals("NavigableSetTest.testPollFirst", false, tS.contains("k1"));
        assertEquals("NavigableSetTest.testPollFirst", null, emptyTs.pollFirst());
        System.out.println("///////////////////////////////////////////");
    }

    public static void testPollLast() {
        NavigableSet<String> tS = new TreeSet<>();
        tS.add("k1");
        tS.add("k2");
        tS.add("k3");

        assertEquals("NavigableSetTest.testPollLast", "k3", tS.pollLast());
        assertEquals("NavigableSetTest.testPollLast", false, tS.contains("k3"));
        assertEquals("NavigableSetTest.testPollLast", null, emptyTs.pollLast());
        System.out.println("///////////////////////////////////////////");
    }

    public static void testIterator() {
        NavigableSet<String> tS = new TreeSet<>();
        tS.add("k1");
        tS.add("k2");
        tS.add("k3");

        StringBuilder actual = new StringBuilder();
        Iterator<String> itr = tS.iterator();
        while (itr.hasNext()) {
            actual.append(itr.next());
        }
        String assume = "k1k2k3";
        assertEquals("NavigableSetTest.testIterator", assume, actual.toString());
        System.out.println("///////////////////////////////////////////");
    }

    public static void testDescendingSet() {
        NavigableSet<String> tS = new TreeSet<>();
        tS.add("k1");
        tS.add("k2");
        tS.add("k3");

        String assume = "[k3, k2, k1]";
        NavigableSet<String> actual = tS.descendingSet();

        assertEquals("NavigableSetTest.testDescendingSet", assume, actual.toString());
        assertEquals("NavigableSetTest.testDescendingSet", tS.toString(), actual.descendingSet().toString());
        actual.remove("k1");
        assertEquals("NavigableSetTest.testDescendingSet", false, tS.contains("k1"));
        tS.remove("k2");
        assertEquals("NavigableSetTest.testDescendingSet", false, actual.contains("k2"));
        System.out.println("///////////////////////////////////////////");

    }

    public static void testDescendingIterator() {
        NavigableSet<String> tS = new TreeSet<>();
        tS.add("k1");
        tS.add("k2");
        tS.add("k3");

        StringBuilder actual = new StringBuilder();
        Iterator<String> itr = tS.descendingIterator();
        while (itr.hasNext()) {
            actual.append(itr.next());
        }
        String assume = "k3k2k1";
        assertEquals("NavigableSetTest.testDescendingIterator", assume, actual.toString());
        System.out.println("///////////////////////////////////////////");
    }

    public static void testSubSet() {
        NavigableSet<String> tS = new TreeSet<>();
        tS.add("k1");
        tS.add("k2");
        tS.add("k3");
        tS.add("k4");
        tS.add("k5");

        NavigableSet<String> actual = tS.subSet("k2", true, "k4", true);

        assertEquals("NavigableSetTest.testSubSet", "[k2, k3, k4]", actual.toString());
        assertEquals("NavigableSetTest.testSubSet", "[k3]", tS.subSet("k2", false, "k4", false).toString());

        actual.remove("kn3");
        assertEquals("NavigableSetTest.testSubSet", false, tS.contains("kn3"));
        tS.remove("kn2");
        assertEquals("NavigableSetTest.testSubSet", false, actual.contains("kn2"));

        try {
            actual.add("kn6");
        } catch (IllegalArgumentException e) {
            System.out.println("IAE, when insert element is out of range of subset, was thrown");
        }

        NavigableSet<CustomClass> tSForCce = new TreeSet<>();
        try {
            tSForCce.subSet(new SubClass1("k1", 1, "1"), new SubClass2("k3", 3, "3"));
        } catch (ClassCastException e) {
            System.out.println("CCE, when passed elements cannot be compared to one another, was thrown");
        }

        try {
            actual.subSet(null, "6");
        } catch (NullPointerException e) {
            System.out.println("NPE, when passed null element, was thrown");
        }

        try {
            actual.subSet("kn7", "kn6");
        } catch (IllegalArgumentException e) {
            System.out.println("IAE, when fromElement > toElement, was thrown");
        }
        System.out.println("///////////////////////////////////////////");
    }

    public static void testHeadSet() {
        NavigableSet<String> tS = new TreeSet<>();
        tS.add("k1");
        tS.add("k2");
        tS.add("k3");
        tS.add("k4");
        tS.add("k5");

        NavigableSet<String> actual = tS.headSet("k2", true);

        assertEquals("NavigableSetTest.testHeadSet", "[k1, k2]", actual.toString());

        actual.remove("kn1");
        assertEquals("NavigableSetTest.testHeadSet", false, tS.contains("kn3"));
        tS.remove("kn2");
        assertEquals("NavigableSetTest.testHeadSet", false, actual.contains("kn2"));

        try {
            actual.add("kn6");
        } catch (IllegalArgumentException e) {
            System.out.println("IAE, when insert element is out of range of subset, was thrown");
        }

        NavigableSet<CustomClass> tSForCce = new TreeSet<>();
        try {
            tSForCce.headSet(new CustomClass("k1", 1));
        } catch (ClassCastException e) {
            System.out.println("CCE, when passed elements doesn't implement Comparable, was thrown");
        }

        try {
            actual.headSet(null);
        } catch (NullPointerException e) {
            System.out.println("NPE, when passed null element, was thrown");
        }
        System.out.println("///////////////////////////////////////////");
    }

    public static void testTailSet() {
        NavigableSet<String> tS = new TreeSet<>();
        tS.add("k1");
        tS.add("k2");
        tS.add("k3");
        tS.add("k4");

        NavigableSet<String> actual = tS.tailSet("k2", false);

        assertEquals("NavigableSetTest.testTailSet", "[k3, k4]", actual.toString());

        actual.remove("kn4");
        assertEquals("NavigableSetTest.testTailSet", false, tS.contains("kn4"));
        tS.remove("kn3");
        assertEquals("NavigableSetTest.testTailSet", false, actual.contains("kn3"));

        try {
            actual.add("k0");
        } catch (IllegalArgumentException e) {
            System.out.println("IAE, when insert element is out of range of subset, was thrown");
        }

        NavigableSet<CustomClass> tSForCce = new TreeSet<>();
        try {
            tSForCce.tailSet(new CustomClass("k1", 1));
        } catch (ClassCastException e) {
            System.out.println("CCE, when passed elements doesn't implement Comparable, was thrown");
        }

        try {
            actual.tailSet(null);
        } catch (NullPointerException e) {
            System.out.println("NPE, when passed null element, was thrown");
        }
        System.out.println("///////////////////////////////////////////");
    }
}