package com.getjavajob.training.algo08.nekrasovs.lesson05;

import java.util.Collection;

public interface MyTransformer<I, O> {

    O transformElement(I element);

    Collection<O> transformCollection(Collection<I> inputCol);
}