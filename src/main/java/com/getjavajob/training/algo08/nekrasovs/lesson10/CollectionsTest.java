package com.getjavajob.training.algo08.nekrasovs.lesson10;

import java.util.*;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;

public class CollectionsTest {
    public static void main(String[] args) {
        testAddAll();
        testAsLifoQueue();
        testSort();
        testBinarySearch();
        testReverse();
        testSwap();
        testFill();
        testCopy();
        testMin();
        testMax();
        testRotate();
        testReplaceAll();
        testIndexOfSubList();
        testLastIndexOfSubList();
        testUnmodifiableCollection();
        testEmptyIterator();
        testSingleton();
        testNcopies();
        testReverseOrder();
        testEnumeration();
        testList();
        testFrequency();
        testDisJoint();
    }

    public static void testAddAll() {
        Collection<Integer> testCol = new ArrayList<>();

        Collection<Integer> testCol2 = new ArrayList<>();
        testCol2.add(1);
        testCol2.add(2);
        testCol2.add(3);

        testCol.addAll(testCol2);
        assertEquals("CollectionsTest.testAddAll", testCol2, testCol);
    }

    public static void testAsLifoQueue() {
        Deque<Integer> testDeque = new ArrayDeque<>();
        Queue<Integer> asLifo = Collections.asLifoQueue(testDeque);
        asLifo.add(1);
        asLifo.add(2);

        assertEquals("CollectionsTest.testAsLifoQueue", (Integer) 2, asLifo.remove());
    }

    public static void testSort() {
        List<Integer> testCol = new ArrayList<>();
        testCol.add(3);
        testCol.add(1);
        testCol.add(2);
        List<Integer> sorted = new ArrayList<>();
        sorted.add(1);
        sorted.add(2);
        sorted.add(3);
        Collections.sort(testCol);
        assertEquals("CollectionsTest.testSort", sorted, testCol);
    }

    public static void testBinarySearch() {
        List<Integer> testCol = new ArrayList<>();
        testCol.add(1);
        testCol.add(2);
        testCol.add(3);
        assertEquals("CollectionsTest.testBinarySearch", 1, Collections.binarySearch(testCol, 2));
    }

    public static void testReverse() {
        List<Integer> testCol = new ArrayList<>();
        testCol.add(1);
        testCol.add(2);
        testCol.add(3);
        List<Integer> reversed = new ArrayList<>();
        reversed.add(3);
        reversed.add(2);
        reversed.add(1);
        Collections.reverse(testCol);
        assertEquals("CollectionsTest.testReverse", reversed, testCol);
    }

    public static void testSwap() {
        List<Integer> testCol = new ArrayList<>();
        testCol.add(1);
        testCol.add(2);
        testCol.add(3);
        List<Integer> afterSwap = new ArrayList<>();
        afterSwap.add(2);
        afterSwap.add(1);
        afterSwap.add(3);
        Collections.swap(testCol, 0, 1);
        assertEquals("CollectionsTest.testSwap", afterSwap, testCol);
    }

    public static void testFill() {
        List<Integer> testCol = new ArrayList<>();
        testCol.add(1);
        testCol.add(2);
        testCol.add(3);
        List<Integer> filled = new ArrayList<>();
        filled.add(4);
        filled.add(4);
        filled.add(4);
        Collections.fill(testCol, 4);
        assertEquals("CollectionsTest.testFill", filled, testCol);
    }

    public static void testCopy() {
        List<Integer> testCol = new ArrayList<>();
        testCol.add(1);
        testCol.add(2);
        testCol.add(3);
        testCol.add(4);
        List<Integer> toCopy = new ArrayList<>();
        toCopy.add(4);
        toCopy.add(5);
        toCopy.add(6);
        List<Integer> assume = new ArrayList<>();
        assume.add(4);
        assume.add(5);
        assume.add(6);
        assume.add(4);
        Collections.copy(testCol, toCopy);
        assertEquals("CollectionsTest.testCopy", assume, testCol);
    }

    public static void testMin() {
        List<Integer> testCol = new ArrayList<>();
        testCol.add(1);
        testCol.add(2);
        testCol.add(3);
        assertEquals("CollectionsTest.testMin", (Integer) 1, Collections.min(testCol));
    }

    public static void testMax() {
        List<Integer> testCol = new ArrayList<>();
        testCol.add(1);
        testCol.add(2);
        testCol.add(3);
        assertEquals("CollectionsTest.testMax", (Integer) 3, Collections.max(testCol));
    }

    public static void testRotate() {
        List<Integer> testCol = new ArrayList<>();
        testCol.add(1);
        testCol.add(2);
        testCol.add(3);
        testCol.add(4);
        List<Integer> rotated = new ArrayList<>();
        rotated.add(4);
        rotated.add(1);
        rotated.add(2);
        rotated.add(3);
        Collections.rotate(testCol, 1);
        assertEquals("CollectionsTest.testRotate", rotated, testCol);
    }

    public static void testReplaceAll() {
        List<Integer> testCol = new ArrayList<>();
        testCol.add(1);
        testCol.add(2);
        testCol.add(1);
        testCol.add(4);
        List<Integer> afterReplace = new ArrayList<>();
        afterReplace.add(3);
        afterReplace.add(2);
        afterReplace.add(3);
        afterReplace.add(4);
        Collections.replaceAll(testCol, 1, 3);
        assertEquals("CollectionsTest.testReplaceAll", afterReplace, testCol);
    }

    public static void testIndexOfSubList() {
        List<Integer> testCol = new ArrayList<>();
        testCol.add(1);
        testCol.add(2);
        testCol.add(3);
        testCol.add(4);
        List<Integer> subList = new ArrayList<>();
        subList.add(3);
        subList.add(4);
        assertEquals("CollectionsTest.testIndexOfSubList", 2, Collections.indexOfSubList(testCol, subList));
    }

    public static void testLastIndexOfSubList() {
        List<Integer> testCol = new ArrayList<>();
        testCol.add(1);
        testCol.add(2);
        testCol.add(3);
        testCol.add(4);
        testCol.add(3);
        testCol.add(4);
        List<Integer> subList = new ArrayList<>();
        subList.add(3);
        subList.add(4);
        assertEquals("CollectionsTest.testLastIndexOfSubList", 4, Collections.lastIndexOfSubList(testCol, subList));
    }

    public static void testUnmodifiableCollection() {
        List<Integer> testCol = new ArrayList<>();
        testCol.add(1);
        testCol.add(2);
        testCol.add(3);
        testCol.add(4);
        testCol.add(3);
        testCol.add(4);
        Collection<Integer> unmodifiable = Collections.unmodifiableCollection(testCol);
        try {
            unmodifiable.add(5);
        } catch (UnsupportedOperationException e) {
            System.out.println("UOE was thrown");
        }
    }

    public static void testEmptyIterator() {
        Iterator<Integer> itr = Collections.emptyIterator();
        assertEquals("CollectionsTest.testEmptyIterator", false, itr.hasNext());
        try {
            itr.next();
        } catch (NoSuchElementException e) {
            System.out.println("NSE was thrown");
        }
    }

    public static void testSingleton() {
        Set<Integer> stn = Collections.singleton(0);
        //immutable
        try {
            stn.add(1);
        } catch (UnsupportedOperationException e) {
            System.out.println("singleton is immutable");
        }
    }

    public static void testNcopies() {
        Integer[] d = new Integer[1];
        List<Integer[]> nCopies = Collections.nCopies(3, d);
        assertEquals("CollectionsTest.testNcopies", true, nCopies.get(0) == d);
        assertEquals("CollectionsTest.testNcopies", true, nCopies.get(1) == d);
        assertEquals("CollectionsTest.testNcopies", true, nCopies.get(2) == d);
    }

    public static void testReverseOrder() {
        List<Integer> testCol = new ArrayList<>();
        testCol.add(1);
        testCol.add(2);
        testCol.add(3);
        List<Integer> assumed = new ArrayList<>();
        assumed.add(3);
        assumed.add(2);
        assumed.add(1);
        Comparator<Integer> cmp = Collections.reverseOrder();
        Collections.sort(testCol, cmp);
        assertEquals("CollectionsTest.testReverseOrder", assumed, testCol);
    }

    public static void testEnumeration() {
        List<Integer> testCol = new ArrayList<>();
        testCol.add(1);
        testCol.add(2);
        testCol.add(3);
        Enumeration<Integer> enm = Collections.enumeration(testCol);
        StringBuilder forTest = new StringBuilder();
        while (enm.hasMoreElements()) {
            forTest.append(enm.nextElement());
        }
        assertEquals("CollectionsTest.testEnumeration", "123", forTest.toString());
    }

    public static void testList() {
        List<Integer> testCol = new ArrayList<>();
        testCol.add(1);
        testCol.add(2);
        testCol.add(3);
        Enumeration<Integer> enm = Collections.enumeration(testCol);
        assertEquals("CollectionsTest.testList", testCol, Collections.list(enm));
    }

    public static void testFrequency() {
        List<Integer> testCol = new ArrayList<>();
        testCol.add(1);
        testCol.add(2);
        testCol.add(3);
        testCol.add(4);
        testCol.add(3);
        assertEquals("CollectionsTest.testFrequency", 2, Collections.frequency(testCol, 3));
    }

    public static void testDisJoint() {
        List<Integer> testCol = new ArrayList<>();
        testCol.add(1);
        testCol.add(2);
        testCol.add(3);
        List<Integer> testCol2 = new ArrayList<>();
        testCol2.add(4);
        testCol2.add(5);
        testCol2.add(6);
        assertEquals("CollectionsTest.testDisJoint", true, Collections.disjoint(testCol, testCol2));
    }
}