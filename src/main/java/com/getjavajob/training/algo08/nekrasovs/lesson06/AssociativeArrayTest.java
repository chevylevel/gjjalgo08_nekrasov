package com.getjavajob.training.algo08.nekrasovs.lesson06;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;

public class AssociativeArrayTest {

    private static final String[] KEYS = {"one", "two", "three", "4", "5", "six", "seven", "eight", "9",
            "ten", "eleven", "=-12", "13*", "14"};
    private static AssociativeArray<String, String> phoneBook = new AssociativeArray<>();

    public static void main(String[] args) {
        testAdd();
        testRemove();
        testGet();
    }

    public static void testAdd() {
        // addition to empty bucket
        // 0 *
        // 7 * | "skankHunt42" | "Gerald" |
        // 16*
        assertEquals("AssociativeArrayTest.testAdd", null, phoneBook.add("skankHunt42", "Gerald"));
        assertEquals("AssociativeArrayTest.testAdd check whether value added",
                "Gerald", phoneBook.get("skankHunt42"));

        // rewrite existing entry by new value
        // 0 *
        // 7 * | "skankHunt42" | "Gerald Broflovski" |
        // 16*
        assertEquals("AssociativeArrayTest.testAdd rewrite", "Gerald", phoneBook.add("skankHunt42", "Gerald Broflovski"));
        assertEquals("AssociativeArrayTest.testAdd check whether value rewritten",
                "Gerald Broflovski", phoneBook.get("skankHunt42"));

        // adding with null key
        // 0 * | null          | "valueForNullKey"   |
        // 7 * | "skankHunt42" | "Gerald Broflovski" |
        // 16*
        assertEquals("AssociativeArrayTest.testAdd add null key value",
                null, phoneBook.add(null, "valueForNullKey"));
        assertEquals("AssociativeArrayTest.testAdd check whether null key value added",
                "valueForNullKey", phoneBook.get(null));

        // rewrite entry with null key
        // 0 * | null          | "valueForNullKey2"  |
        // 7 * | "skankHunt42" | "Gerald Broflovski" |
        // 16*
        assertEquals("AssociativeArrayTest.testAdd rewrite null key",
                "valueForNullKey", phoneBook.add(null, "valueForNullKey2"));
        assertEquals("AssociativeArrayTest.testAdd check rewritten value (null key)",
                "valueForNullKey2", phoneBook.get(null));

        // Adding "random" key
        // 0 * | null          | "valueForNullKey2"  |
        // 7 * | "skankHunt42" | "Gerald Broflovski" |
        // 13* | "random"      | "value1"            |
        // 16*
        assertEquals("AssociativeArrayTest.testAdd \"random\" key", null, phoneBook.add("random", "value1"));
        assertEquals("AssociativeArrayTest.testAdd check added \"random\" key",
                "value1", phoneBook.get("random"));

        // Check collision handling for bucket 0.
        // 0 * | null          | "valueForNullKey2"  | -> | "polygenelubricants" | "value2" |
        // 7 * | "skankHunt42" | "Gerald Broflovski" |
        // 13* | "random"      | "value1"            |
        // 16*
        assertEquals("AssociativeArrayTest.testAdd polygenelubricants key",
                null, phoneBook.add("polygenelubricants", "value2"));
        assertEquals("AssociativeArrayTest.testAdd check added \"polygenelubricants\" key",
                "value2", phoneBook.get("polygenelubricants"));
        assertEquals("AssociativeArrayTest.testAdd check access to null key",
                "valueForNullKey2", phoneBook.get(null));

        // rewriting check for non first position in bucket 0.
        // 0 * | null          | "valueForNullKey2"  | -> | "polygenelubricants" | "anotherValue" |
        // 7 * | "skankHunt42" | "Gerald Broflovski" |
        // 13* | "random"      | "value1"            |
        // 16*
        assertEquals("AssociativeArrayTest.testAdd rewrite non first element in bucket 0",
                "value2", phoneBook.add("polygenelubricants", "anotherValue"));
        assertEquals("AssociativeArrayTest.testAdd check rewritten value",
                "anotherValue", phoneBook.get("polygenelubricants"));

        // Check collision handling for not null key element in non 0 bucket.
        // 0 * | null          | "valueForNullKey2"  | -> | "polygenelubricants" | "anotherValue"      |
        // 7 * | "skankHunt42" | "Gerald Broflovski" |
        // 13* | "random"      | "value1"            | -> | "ten"                | "value for ten key" |
        // 16*
        assertEquals("AssociativeArrayTest.testAdd check collision handling",
                null, phoneBook.add("ten", "value for ten key"));
        assertEquals("AssociativeArrayTest.testAdd check whether element added",
                "value for ten key", phoneBook.get("ten"));
        assertEquals("AssociativeArrayTest.testAdd check access to \"random\" element",
                "value1", phoneBook.get("random"));

        // Rewrite non first entry value in non 0 bucket.
        // 0 * | null          | "valueForNullKey2"  | -> | "polygenelubricants" | "anotherValue"      |
        // 7 * | "skankHunt42" | "Gerald Broflovski" |
        // 13* | "random"      | "value1"            | -> | "ten"                | "value2 for ten key" |
        // 16*
        assertEquals("AssociativeArrayTest.testAdd rewrite non first element",
                "value for ten key", phoneBook.add("ten", "value2 for ten key"));
        assertEquals("AssociativeArrayTest.testAdd check rewritten non first element",
                "value2 for ten key", phoneBook.get("ten"));
    }

    public static void testRemove() {

        // Remove element.
        // 0 * | null     | "valueForNullKey2" | -> | "polygenelubricants" | "anotherValue"        |
        // 7 *
        // 13* | "random" | "value1"           | -> | "ten"                | "value2 for ten key " |
        // 16*
        assertEquals("AssociativeArrayTest.testRemove", "Gerald Broflovski", phoneBook.remove("skankHunt42"));
        assertEquals("AssociativeArrayTest.testRemove check whether element removed",
                null, phoneBook.get("skankHunt42"));

        // Remove null key element.
        // 0 *| "polygenelubricants" | "anotherValue" |
        // 7 *
        // 13* | "random"            | "value1"       | -> | "ten" | value2 for ten key |
        // 16*
        assertEquals("AssociativeArrayTest.testRemove null key entry",
                "valueForNullKey2", phoneBook.remove(null));
        assertEquals("AssociativeArrayTest.testRemove check whether null key entry removed",
                null, phoneBook.get(null));
        // Adding sufficient elements to invoke rehash method.
        for (String key : KEYS) {
            phoneBook.add(key, "value");
        }
    }

    // check access for elements after rehashing.
    public static void testGet() {
        assertEquals("AssociativeArrayTest.testGet", "value1", phoneBook.get("random"));
        assertEquals("AssociativeArrayTest.testGet", "anotherValue", phoneBook.get("polygenelubricants"));
        assertEquals("AssociativeArrayTest.testGet", "value2 for ten key", phoneBook.get("ten"));
    }
}