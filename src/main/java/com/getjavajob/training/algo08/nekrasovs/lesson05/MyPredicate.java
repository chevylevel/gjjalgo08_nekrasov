package com.getjavajob.training.algo08.nekrasovs.lesson05;


public interface MyPredicate<E> {
    /**
     * Method applies implemented condition to given object "element".
     *
     * @param element of any type
     * @return boolean value whether element satisfy condition.
     */
    boolean test(E element);
}