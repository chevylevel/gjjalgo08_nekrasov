package com.getjavajob.training.algo08.nekrasovs.lesson10;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;
import static java.util.Arrays.*;

public class ArraysTest {
    public static void main(String[] args) {
        testSort();
        testBinarySearch();
        testEquals();
        testFill();
        testCopyOf();
        testCopyOfRange();
        testAsList();
        testHashCode();
        testToString();
        testDeepToString();
    }

    public static void testSort() {
        int[] sample = {3, 2, 1};
        int[] sorted = {1, 2, 3};
        sort(sample);
        assertEquals("ArraysTest.testSort", sorted, sample);

        int[] sample2 = {3, 2, 1};
        int[] sorted2 = {3, 1, 2};
        sort(sample2, 1, 3);
        assertEquals("ArraysTest.testSort", sorted2, sample2);

        //check stability
        MyComparableClass[] objArray = {
                new MyComparableClass("2", 2),
                new MyComparableClass("1", 1),
                new MyComparableClass("3", 4),
                new MyComparableClass("3", 3),
        };
        MyComparableClass[] sortedObjArray = {
                new MyComparableClass("1", 1),
                new MyComparableClass("2", 2),
                new MyComparableClass("3", 4),
                new MyComparableClass("3", 3),
        };
        sort(objArray);
        assertEquals("ArraysTest.testSort", sortedObjArray, objArray);

        // sort with comparator
        MyClass[] objArray2 = {
                new MyClass("2", 2),
                new MyClass("1", 1),
                new MyClass("3", 4),
                new MyClass("3", 3),
        };
        MyClass[] sortedObjArray2 = {
                new MyClass("1", 1),
                new MyClass("2", 2),
                new MyClass("3", 4),
                new MyClass("3", 3),
        };
        sort(objArray2, new Comparator<MyClass>() {
            @Override
            public int compare(MyClass o1, MyClass o2) {
                return o1.name.compareTo(o2.name);
            }
        });
        assertEquals("ArraysTest.testSort", sortedObjArray2, objArray2);
    }

    public static void testBinarySearch() {
        int[] sample = {1, 2, 4}; //should be sorted
        assertEquals("ArraysTest.testBinarySearch", 1, binarySearch(sample, 2));
        assertEquals("ArraysTest.testBinarySearch", -4, binarySearch(sample, 5));
        assertEquals("ArraysTest.testBinarySearch", -3, binarySearch(sample, 0, 2, 4));
    }

    public static void testEquals() {
        int[] sample = {1, 2, 4};
        int[] equalSample = {1, 2, 4};
        assertEquals("ArraysTest.testEquals", true, Arrays.equals(sample, equalSample));

        assertEquals("ArraysTest.testEquals", false, Arrays.equals(sample, null));

        int[] nullSample = null;
        assertEquals("ArraysTest.testEquals", true, Arrays.equals(nullSample, null));

    }

    public static void testFill() {
        int[] sample = {1, 1, 1};
        int[] toFill = new int[3];
        fill(toFill, 1);
        assertEquals("ArraysTest.testFill", sample, toFill);

        fill(toFill, 0, 2, 2);
        int[] sample2 = {2, 2, 1};
        assertEquals("ArraysTest.testFill", sample2, toFill);
    }

    public static void testCopyOf() {
        int[] sample = {1, 1, 1};
        int[] copy = {1, 1, 1};
        assertEquals("ArraysTest.testCopyOf", copy, copyOf(sample, 3));

        int[] copy2 = {1, 1, 1, 0};
        assertEquals("ArraysTest.testCopyOf", copy2, copyOf(sample, 4));

        int[] copy3 = {1, 1};
        assertEquals("ArraysTest.testCopyOf", copy3, copyOf(sample, 2));


        Number[] numA = {1, 2, 3, null};
        Integer[] integerA = {1, 2, 3};
        assertEquals("ArraysTest.testCopyOf", numA, copyOf(integerA, 4, Number[].class));

        Character[] chA = {'1', '2', '3'};
        try {
            copyOf(chA, 3, Number[].class);
        } catch (ArrayStoreException e) {
            System.out.println("ASE test passed");
        }
    }

    public static void testCopyOfRange() {
        Number[] numA = {1, 2, 3, null};
        Integer[] integerA = {1, 2, 3};

        assertEquals("ArraysTest.testCopyOfRange", numA, copyOfRange(integerA, 0, 4, Number[].class));
    }

    public static void testAsList() {
        List<Integer> testList = new ArrayList<>();
        testList.add(1);
        testList.add(2);
        testList.add(3);

        Integer[] testArray = {1, 2, 3};

        List<Integer> asList = asList(testArray);
        assertEquals("ArraysTest.testAsList", testList, asList);

        asList.set(2, 4);
        assertEquals("ArraysTest.testAsList", 4, (int) testArray[2]);

        testArray[0] = 0;
        assertEquals("ArraysTest.testAsList", 0, (int) asList.get(0));
    }

    public static void testHashCode() {
        List<Integer> testList = new ArrayList<>();
        testList.add(1);
        testList.add(2);
        testList.add(3);
        Integer[] testArray = {1, 2, 3};
        assertEquals("ArraysTest.testHashCode", testList.hashCode(), Arrays.hashCode(testArray));
    }

    public static void testToString() {
        Integer[] testArray = {1, 2, 3};
        assertEquals("ArraysTest.testToString", "[1, 2, 3]", Arrays.toString(testArray));
    }

    public static void testDeepToString() {
        Integer[] innerArray = {1, 2, 3};
        Integer[][] testArray = {innerArray, innerArray};
        assertEquals("ArraysTest.testDeepToString", "[[1, 2, 3], [1, 2, 3]]", Arrays.deepToString(testArray));
    }

    private static class MyClass {
        String name;
        int value;

        MyClass(String name, int value) {
            this.name = name;
            this.value = value;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            MyClass myClass = (MyClass) o;
            return name != null ? name.equals(myClass.name) : myClass.name == null;
        }

        @Override
        public int hashCode() {
            return name != null ? name.hashCode() : 0;
        }
    }


    private static class MyComparableClass implements Comparable {
        String name;
        int value;

        MyComparableClass(String name, int value) {
            this.name = name;
            this.value = value;
        }

        @Override
        public int compareTo(Object o) {
            return name.compareTo(((MyComparableClass) o).name);
        }

        @Override
        public String toString() {
            return name + ':' + value;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            MyComparableClass myComparableClass = (MyComparableClass) o;
            return name.equals(myComparableClass.name);
        }

        @Override
        public int hashCode() {
            return name.hashCode();
        }
    }
}