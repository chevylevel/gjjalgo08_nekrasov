package com.getjavajob.training.algo08.nekrasovs.lesson04;

public abstract class AbstractLinkedList<E> extends AbstractList<E> {

    public Object[] toArray() {
        throw new UnsupportedOperationException();
    }

    public E set(int i, E e) {
        throw new UnsupportedOperationException();
    }

    public boolean contains(Object o) {
        throw new UnsupportedOperationException();
    }

    public int indexOf(Object o) {
        throw new UnsupportedOperationException();
    }
}