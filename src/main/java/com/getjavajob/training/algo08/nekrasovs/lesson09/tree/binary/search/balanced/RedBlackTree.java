package com.getjavajob.training.algo08.nekrasovs.lesson09.tree.binary.search.balanced;

import com.getjavajob.training.algo08.nekrasovs.lesson07.tree.Node;
import com.getjavajob.training.algo08.nekrasovs.lesson07.tree.binary.LinkedBinaryTree;
import com.getjavajob.training.algo08.nekrasovs.lesson08.tree.binary.search.balanced.BalanceableTree;

import java.util.Collection;

public class RedBlackTree<E> extends BalanceableTree<E> {
    private static final boolean RED = false;
    private static final boolean BLACK = true;


    private boolean isBlack(Node<E> n) {
        return (n == null) ? BLACK : ((NodeImpl<E>) n).color;
    }

    private boolean isRed(Node<E> n) {
        return (n == null) ? BLACK : !((NodeImpl<E>) n).color;
    }

    private void makeBlack(Node<E> n) {
        ((NodeImpl<E>) n).color = BLACK;
    }

    private void makeRed(Node<E> n) {
        ((NodeImpl<E>) n).color = RED;
    }

    @Override
    protected NodeImpl<E> validate(Node<E> n) throws IllegalArgumentException {
        if (n instanceof NodeImpl) {
            return (NodeImpl<E>) n;
        } else {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public Node<E> addRoot(E e) throws IllegalStateException {
        if (root != null) {
            throw new IllegalStateException("Tree already has root");
        }
        if (e == null) {
            throw new IllegalArgumentException("null element is not allowed");
        }
        root = new NodeImpl<>(e, null, null, null);
        afterElementAdded(root);
        size++;
        return root;
    }

    @Override
    public Node<E> add(E e) throws IllegalArgumentException {
        Node<E> t = root;
        if (t == null) {
            return addRoot(e);
        } else {
            NodeImpl<E> parent;
            int cmp;
            do {
                parent = (RedBlackTree.NodeImpl<E>) t;
                cmp = compare(t.getElement(), e);
                if (cmp > 0) {
                    t = left(t);
                } else if (cmp < 0) {
                    t = right(t);
                } else {
                    return parent;
                }
            } while (t != null);

            Node<E> newNode = new NodeImpl<>(e, parent, null, null);

            if (cmp > 0) {
                parent.setLeft(newNode);
            } else {
                parent.setRight(newNode);
            }
            afterElementAdded(newNode);
            size++;
            return newNode;
        }
    }

    @Override
    public E remove(Node<E> n) throws IllegalArgumentException {
        E value = n.getElement();
        Node<E> toDelete = validate(n);
        NodeImpl<E> parent = (NodeImpl<E>) parent(n);

        if (childrenNumber(toDelete) == 2) {
            Node<E> nextInOrder = right(toDelete);
            while (left(nextInOrder) != null) {
                nextInOrder = left(nextInOrder);
            }
            ((NodeImpl<E>) n).setValue(nextInOrder.getElement());
            toDelete = nextInOrder;
            parent = (NodeImpl<E>) parent(nextInOrder);
        }

        if (childrenNumber(toDelete) == 1) {
            NodeImpl<E> replacement = (left(toDelete) == null) ?
                    (NodeImpl<E>) right(toDelete) :
                    (NodeImpl<E>) left(toDelete);
            if (parent == null) {
                root = replacement;
                replacement.setParent(null);
            } else {
                if (left(parent) == toDelete) {
                    parent.setLeft(replacement);
                } else {
                    parent.setRight(replacement);
                }
                replacement.setParent(parent);
            }
            ((NodeImpl<E>) n).setParent(null);
            ((NodeImpl<E>) n).setLeft(null);
            ((NodeImpl<E>) n).setRight(null);
            afterElementRemoved(replacement);

        } else {
            if (parent == null) {
                root = null;
            } else {
                if (left(parent) == toDelete) {
                    parent.setLeft(null);
                } else {
                    parent.setRight(null);
                }
                ((NodeImpl<E>) toDelete).setParent(parent(n));
                afterElementRemoved(toDelete);
                ((NodeImpl<E>) toDelete).setParent(null);
            }
        }
        size--;
        return value;
    }

    private Node<E> getG(Node<E> n) {
        return parent(n) == null ? null : parent(parent(n));
    }

    private Node<E> getU(Node<E> n) {
        if (getG(n) == null) {
            return null;
        } else {
            return left(getG(n)) == parent(n) ? right(getG(n)) : left(getG(n));
        }
    }

    private Node<E> siblingOf(Node<E> n) {
        return left(parent(n)) == null ? right(parent(n)) : left(parent(n));
    }

    @Override
    protected void afterElementAdded(Node<E> n) {
        makeRed(n);

        Node<E> current = n;
        while (!isBlack(parent(current))) {
            if (current == root) {
                makeBlack(current);
                root = current;
                return;
            }
            Node<E> p = parent(current);
            if (getU(current) != null && isRed(getU(current))) {
                makeBlack(p);
                makeBlack(getU(current));
                makeRed(getG(current));
                current = getG(current);
            } else {
                Node<E> g = getG(current);
                reduceSubtreeHeight(current);
                if (parent(current) != p) {
                    makeBlack(current);
                    current = p;
                } else {
                    makeBlack(p);
                }
                makeRed(g);
            }
        }
        makeBlack(root);
    }

    @Override
    protected void afterElementRemoved(Node<E> toFix) {
        if (isRed(toFix) || toFix == root) {
            makeBlack(toFix);
        } else {
            if (isRed(siblingOf(toFix))) {
                makeRed(parent(toFix));
                rotate(siblingOf(toFix));
            }
            if (isBlack(right(siblingOf(toFix))) &&
                    isBlack(left(siblingOf(toFix)))) {
                makeRed(siblingOf(toFix));
                toFix = parent(toFix);
                afterElementRemoved(toFix);
            } else {
                Node<E> redSibChild;
                Node<E> blackSibChild;
                if (right(parent(toFix)) == siblingOf(toFix)) {
                    redSibChild = left(siblingOf(toFix));
                    blackSibChild = right(siblingOf(toFix));
                } else {

                    redSibChild = right(siblingOf(toFix));
                    blackSibChild = left(siblingOf(toFix));
                }
                if (isBlack(blackSibChild)) {
                    makeBlack(redSibChild);
                    makeRed(siblingOf(toFix));
                    rotate(redSibChild);
                }
                ((NodeImpl<E>) siblingOf(toFix)).color = ((NodeImpl<E>) parent(toFix)).color;
                makeBlack(parent(toFix));
                redSibChild = (siblingOf(toFix) == right(parent(toFix))) ?
                        right(siblingOf(toFix)) :
                        left(siblingOf(toFix));
                makeBlack(redSibChild);
                rotate(siblingOf(toFix));
            }
        }
    }

    @Override
    public String toString() {
        if (isEmpty()) {
            return "( )";
        }
        StringBuilder treeSb = new StringBuilder();
        Collection<Node<E>> elements = preOrder();
        Node<E> prev = null;
        boolean preStateIsInternal = true;
        int brackets = 0;
        for (Node<E> e : elements) {
            if (preStateIsInternal) {
                treeSb.append("(");
                brackets++;
            }
            if (!preStateIsInternal) {
                if (parent(e) == parent(prev)) {
                    treeSb.append(", ");
                } else {
                    treeSb.append("), ");
                    brackets--;
                }
            }

            String color = (isBlack(e)) ? "b" : "r";
            treeSb.append(String.valueOf(e.getElement())).
                    append(color);
            preStateIsInternal = isInternal(e);
            prev = e;
        }
        for (int b = brackets; b != 0; b--) {
            treeSb.append(")");
        }
        return treeSb.toString();
    }

    private static class NodeImpl<E> extends LinkedBinaryTree.NodeImpl<E> {
        boolean color;

        NodeImpl(E value, Node<E> parent, Node<E> left, Node<E> right) {
            super(value, parent, left, right);
        }

        @Override
        public String toString() {
            return "value=" + " " + value + "color=" + color;
        }
    }
}