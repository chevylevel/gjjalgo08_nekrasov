package com.getjavajob.training.algo08.nekrasovs.lesson08.tree.binary.search;

import com.getjavajob.training.algo08.nekrasovs.lesson07.tree.Node;
import com.getjavajob.training.algo08.nekrasovs.lesson07.tree.binary.LinkedBinaryTree;

import java.util.Comparator;

public class BinarySearchTree<E> extends LinkedBinaryTree<E> {
    private Comparator<E> comparator;

    public BinarySearchTree() {
    }

    public BinarySearchTree(Comparator<E> comparator) {
        this.comparator = comparator;
    }

    /**
     * Method for comparing two values
     *
     * @param val1
     * @param val2
     * @return
     */
    protected int compare(E val1, E val2) {
        return (comparator == null) ? ((Comparable<? super E>) val1).compareTo(val2) :
                comparator.compare(val1, val2);
    }

    /**
     * Returns the node in n's subtree by val
     *
     * @param n   is root of subtree where search is accomplished
     * @param val is value for searching
     * @return Node with value val or null if element is absent.
     */
    public Node<E> treeSearch(Node<E> n, E val) {
        Node<E> t = n;
        while (t != null) {
            int cmp = compare(t.getElement(), val);
            if (cmp > 0) {
                t = left(t);
            } else if (cmp < 0) {
                t = right(t);
            } else {
                return t;
            }
        }
        return null;
    }

    public Node<E> add(E e) throws IllegalArgumentException {
        Node<E> t = root;
        if (t == null) {
            return addRoot(e);
        } else {
            NodeImpl<E> parent;
            int cmp;
            do {
                parent = (NodeImpl<E>) t;
                cmp = compare(t.getElement(), e);
                if (cmp > 0) {
                    t = left(t);
                } else if (cmp < 0) {
                    t = right(t);
                } else {
                    return parent;
                }
            } while (t != null);

            Node<E> newNode = new NodeImpl<>(e, parent, null, null);

            if (cmp > 0) {
                parent.setLeft(newNode);
            } else {
                parent.setRight(newNode);
            }
            afterElementAdded(newNode);
            size++;
            return newNode;
        }
    }

    @Override
    public E set(Node<E> n, E e) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Node<E> addLeft(Node<E> n, E e) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Node<E> addRight(Node<E> n, E e) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Node<E> add(Node<E> n, E e) {
        throw new UnsupportedOperationException();
    }

    protected void afterElementRemoved(Node<E> n) {
    }

    protected void afterElementAdded(Node<E> n) {
    }
}