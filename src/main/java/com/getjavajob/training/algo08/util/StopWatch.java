package com.getjavajob.training.algo08.util;

public class StopWatch {
    private static long time;

    public static long start() {
        time = System.currentTimeMillis();
        return time;
    }

    public static long getElapsedTime() {
        long dTime = System.currentTimeMillis() - time;
        time = 0;
        return dTime;
    }
}
