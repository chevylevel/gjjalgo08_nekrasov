package com.getjavajob.training.algo08.util;

import java.util.Arrays;

public class Assert {
    public static void assertEquals(String testName, boolean expected, boolean actual) {
        if (expected == actual) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String testName, String expected, String actual) {
        if (expected != null) {
            if (expected.equals(actual)) {
                System.out.println(testName + " passed");
            } else {
                System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
            }
        } else {
            if (actual == null) {
                System.out.println(testName + " passed");
            } else {
                System.out.println(testName + " failed: expected " + "null" + ", actual " + actual);
            }
        }
    }

    public static void assertEquals(String testName, double expected, double actual) {
        if (expected == actual) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String testName, int[] expected, int[] actual) {
        if (Arrays.equals(expected, actual)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + Arrays.toString(expected) +
                    ", actual " + Arrays.toString(actual));
        }
    }

    public static void assertEquals(String testName, char expected, char actual) {
        if (expected == actual) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String testName, int[][] expected, int[][] actual) {
        if (Arrays.deepEquals(expected, actual)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + Arrays.toString(expected) +
                    ", actual " + Arrays.toString(actual));
        }
    }

    public static void assertEquals(String testName, java.util.List expected, java.util.List actual) {
        int indicator = 0;
        for (int i = 0; i < expected.size(); i++) {
            if (!expected.get(i).equals(actual.get(i))) {
                System.out.println(testName + " failed: expected " + expected.get(i) +
                        ", actual " + actual.get(i));
                indicator = -1;
            }
        }
        if (indicator != -1) {
        System.out.println(testName + " passed");
        }
    }

    public static void assertEquals(String testName, String[] expected, String[] actual) {
        if (Arrays.equals(expected, actual)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + Arrays.toString(expected) +
                    ", actual " + Arrays.toString(actual));
        }
    }

    public static void assertEquals(String testName, Object expected, Object actual) {
        if (expected != null) {
            if (actual.equals(expected)) {
                System.out.println(testName + " passed");
            } else {
                System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
            }
        } else {
            if (actual == null){
                System.out.println(testName + " passed");
            } else {
                System.out.println(testName + " failed: expected " + "null" + ", actual " + actual);
            }
        }
    }

    public static void assertEquals(String testName, Object[] expected, Object[] actual) {
        if (Arrays.equals(expected, actual)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " +
                    Arrays.toString(expected) +
                    ", actual " +
                    Arrays.toString(actual));
        }
    }

    public static void fail(String msg) {
        throw new AssertionError(msg);
    }

    public static void equals(String msg, String expectedMessage) {
        if (msg.equals(expectedMessage)) {
            System.out.println(expectedMessage + "Exception test passed");
        } else {
            System.out.println("Exception test failed: " + msg + " thrown, " + expectedMessage + " expected");
        }
    }
}